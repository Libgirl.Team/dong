Feature: kill training instance
    Scenario: a user kills a training model(job)

    Scenario Outline: a user kills a already trained model(job)
        Given there is model already trained
        When a user enters dong train kill -j <job-name>
        Then output "<alert> - <cause>"

        Examples:
            | alert | cause                                                |
            | fail  | there's no training model can kill / already trained |
