 @train
Feature: Train from a pre-trained model

    Training from a pre-trained model should be provided or designed in the template
    # We can give a training script for pre-train/retrain
    # In the function, load model first, then do training

    Scenario Outline:  Train from a pre-trained model at <model-loc>
        #CLI: dong train exec --pre-trained <model-name>

        #API
        # method: POST
        # passing [model-name, model-loc, func-loc]
        # e.g. cloud-component: Ben/mnist modelname: vgg16_20190807.hdf5 (You can have multi-models in a modelstore component)
        # Client check the env file (local /cloud) 
        # for local component(s) 
        #   Client send request to server from local with parameters
        #   Server check user permission
        #   upload to the cloud

        # for cloud component(s)
        #   Client send request to server from local with parameters
        #   Server check user permission
        #   Server check model exist
        #   Server ready traning model 
        #   Server response result


        Given a user can access a pre-trained model at <model-loc>
        And the user can access the train-from-pretrain function at <func-loc>
        And the user has appropriate right for training a new model
        And the settings of components are <modelstore-component> and <model-component>
        When the user enters CLI command to do training from pre-trained model
        Then a training job will <result>

        Examples:
            | model-loc | func-loc | modelstore-component | model-component | result |
            | local     | local    | local                | local           | start  |
            | local     | cloud    | local                | cloud           | start  |
            | cloud     | local    | cloud                | local           | start  |
            | cloud     | cloud    | cloud                | cloud           | start  |

    Scenario: Training from a pre-trained model fails to start since dong couldn't match model or function location
        Given a user can access a pre-trained model at <model-loc>
        And the user can access the train-from-pretrain function at <func-loc>
        And the settings of components are <modelstore-component> and <model-component>
        And <model-loc> and <modelstore-component> are not the same or <func-loc> and <model-component> are not the same
        When the user enters CLI command to do training from pre-trained model
        Then the training will fail to start
        And output message to indicate model or function not found


    Scenario: Training from a pre-trained model fails to start since dong couldn't found the model
        Given a user doesn't have an accessible pre-trained model
        When the user enters CLI command to do training from pre-trained model
        Then the training will fail to start
        And output message to indicate model not found