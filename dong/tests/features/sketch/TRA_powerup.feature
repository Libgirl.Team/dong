# ckeck power-up spec -> console

Feature: Training power-up

    Power-up option for user training job. 
    Check current power-up info.
    Permission power-up limit notification by webhook.

    @power-up
    Scenario: User power-up model training
        # CLI:
        # dong train exec -m "my first dong train exec" \
        # --power-up-fuc [arg]\
        # --power-up-cpu 8 --ram-multiples 6 \ 
        # --power-up-hdd 16 \
        # --power-up-ssd 16 \
        # --power-up-storage 16 \
        # --power-up-gpuk80 8 \
        # --power-up-network-traffic physical \
        Given user has a trainable model
        And setting option of power-up is valid
        When user enter command to power-up
        Then model training with power-up

    @power-up
    Scenario: User power-up model training fail
        Given user has a trainable model
        And setting option of power-up is invalid
        When user enter command to power-up
        Then return
        And CLI output error message

    @status
    Scenario: User checking current power-up info
        # CLI: dong train status -j <JOB-ID>
        Given user has a trainable model
        And user already setting power-up
        When user enter command to check status
        Then CLI output: 
            """
            | JOB-ID    | STATUS  | power-up-ID  | TIMESTAMP         |
            | jb-EDEDED | success | set1         | 18:00:02 2019/4/3 |
            ---
            power-up-ID : set1
            JOB-ID: jb-EDEDED
            vCPU: 8 
            RAM: 8
            HDD: 16 (TB)
            SSD: 16 (TB)
            Cloud storage: 16 (TB) 
            GPU - Nvidia Tesla K80: 8
            Network Traffic:  physical limit
            """

    @notification
    Scenario: User permissions are not sufficient to power-up
        Given user role is <role>
        And user train model with power-up over limit
        When user enter command to power-up
        Then CLI output warning message
        And show <confirm prompts> 
        And user need to confirm
        And notify by webhook

    @notification
     Scenario: User permissions are sufficient to power-up
        Given user role is <role>
        And user permissions sufficient to power-up
        When user enter command to power-up
        Then model training with power-up
     



