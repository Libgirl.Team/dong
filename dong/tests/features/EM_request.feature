@endpoint
Feature: Request an endpoint

  As a user,
  he or she wants to use my client to send request to the model endpoint

  # should make concrete of the scenario later
  Scenario: http request
    Given a trained and deployed model on the endpoint with URL
    When a user uses a http client to send request to the model endpoint
    Then the user's http client should get response to the request based on the deployed model
    
