@login
Feature: Login feature

    Scenario: A user enters correct password
        Given a user's account with the email johndoe@libgirl.com and the password 42703538
        When a user logins with the email johndoe@libgirl.com and the password 42703538
        Then login should be successful
        And output: "Login success!"

    Scenario: A user enters wrong password
        Given a user's account with the email johndoe@libgirl.com and the password 42703538
        When a user logins with the email johndoe@libgirl.com and the password 12341234
        Then login should be failed
        And output: "Login failed!"

    Scenario: A user forgets password and wants to reset with dong auth resetpw <email>
        Given a user's <email>
        When a user enters "dong auth resetpw <email>"
        Then the reset password email should be sent to <email>
        And output: "Password reset mail is sent to <email>, please check."
    
    Scenario: A user forgets password and wants to reset with dong auth resetpw
        Given a user's <email>
        When a user enters "dong auth resetpw"
        Then output: "Please enter your account email: "
        And dong get the input of user as <email>
        And output: "Password reset mail is sent to <email>, please check."

Feature: Logout feature

    Scenario: A user logout
        Given a user has logged-in
        When the user enters "dong logout"
        Then output: "Logout success!"
        And remove the login credential at the user's system

Feature: Inquire user acconunt information

    Scenario: A logged-in user inquire account information 
        Given a user has logged-in
        When the user enters "dong auth inquire"
        Then output the account information of the user

    Scenario: A user who hasn't logged-in inquire account information
        Given a user hasn't logged-in
        When the user enters "dong auth inquire"
        Then output: "You haven't logged-in yet!"

