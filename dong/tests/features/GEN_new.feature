@new
Feature: Generate a new project following the template

    Scenario Outline: Generate new project follow the template successfully
        Given <project-name> is not occupied at the working directory
        When a user enters "dong new <project-name>"
        Then dong generates a new project following the template
        And <project-name> as user custom name
        And output: Generate <project-name> in <filepaths>

        Examples:
            | project-name   | filepaths      |
            | user_project   | ./user_project |
            | MyFirstproject | ./Myproject    |

