@endpoint
Feature: Delete an endpoint

  As a user,
  he or she wants to delete one of his/her endpoint on dong's cloud

  Scenario: Delete success
    Given a logged-in user has a endpoint with <name> 
    When the user enters "dong endpoint kill <name>"
    Then output "endpoint <name> ready to be killed"
    And the cloud should delete the running endpoint
