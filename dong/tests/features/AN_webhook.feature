@webhook
Feature: Webhook feature for Success/Fail
	As a user execute an operation,
	it will generate a result and he or she will be notified via webhook.

	Scenario: A user trains a model without subscribe any notification gets no result
		Given a user has a model for training
		And the model is training
		And none notifications are subscribed
		When the training is completed
		Then nothing happens

	Scenario: A user trains a model and gets a success result
		Given a user has a model for training
		And the model is training
		And the notification of success is subscribed
		When the training is successful completed
		Then a notification that informs the model is successfully trained will send to the user via webhook
	
	Scenario: A user trains a model and gets a fail result
		Given a user has a model for training
		And the model is training
		And the notification of fail is subscribed
		When the training fails to complete
		Then a notification that informs the model is failed to be trained will send to the user via webhook
