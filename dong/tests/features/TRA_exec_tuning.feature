@exec @tuning
Feature: Tuning model
    As a user,
    he or she wants submits a parameter submission module to the model for cloud training.

    Scenario: Tuning model for training success
        Given a user specifies has tuning module
        And the user specifies the tuning module
        When the user enters:
            """
            dong train exec
            --tune-module <tune-module-name>
            --package-path <package-path>
            --module-name <moudle-name>
            """
        Then dong check parameter configuration
        And dong package project
        And uploads to cloud for training
        And output: <message>


