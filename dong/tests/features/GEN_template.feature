@template
Feature: Generate templated project files

    Scenario Outline: A user runs generator with name
        When a user specifies the module type <type>
        And the user specifies a name <name> for the module
        Then dong generates template files in the filepath
        And output: Generate [<type>] <name> in <filepaths>

        Examples:
            | type      | name     | filepaths                            |
            | model     | evelin   | /models/evelin.py                    |
            | save_load | sl       | /save_load/sl.py                     |
            | train     | dalian   | /trains/dalian.py,/trains/dalian.ini |
            | endpoint  | 101      | /endpoints/101.py                    |
            | data      | cat      | /data/cat.py                         |
            | build     | 20190417 | /builds/20190417.cfg                 |

    Scenario Outline: A user runs generator without name
        When a user specifies the module type <type>
        Then dong generates template files in the filepath
        And dong gives <name> as default
        And output: 
        user not specifies name, dong will give <name> name...
        And output: Generate [<type>] <name> in <filepaths>

        Examples:
            | type      | name    | filepaths                              |
            | model     | default | /models/default.py                     |
            | save_load | default | /save_load/default.py                  |
            | train     | default | /trains/default.py,/trains/default.ini |
            | endpoint  | default | /endpoints/default.py                  |
            | data      | default | /data/default.py                       |
            | build     | default | /builds/default.cfg                    |

    Scenario Outline: A user runs generator without module type
        When a user doesn't specifies the module type
        Then output: Usage: dong template [OPTIONS] COMMAND [ARGS]...        

# let's do after alpha
# Scenario Outline: 新增專案資料範本內容 by dong giving random name

# Scenario: 使用者查看 template 架構
#     # 需支援不同的help形式
#     When 使用者輸入 "dong template" or "dong template --help"
#     Then help Usage顯示  "想查看template架構 請輸入 {查看文件指令} 指令 或 至 {說明頁面 URL} 查看"



