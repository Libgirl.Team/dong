@status @log @jobname
Feature: inquire training info

    Scenario Outline: inquire job status
        Given a user has job-list
            | job-name | status  | created-time    |
            | job_003  | Success | 2019-5-21 19:35 |
            | job_002  | Error   | 2019-5-20 19:35 |
            | job_001  | Fail    | 2019-5-19 19:35 |

        When the user enters "dong train status --job-name job_003"
        Then output
            """
            | job-name | status  | created-time    |
            | job_003  | Success | 2019-5-21 19:35 |
            """

    Scenario: Inquire training log
        Given a user has training log (at lest one)
        When the user enters "dong train log --job-name <job-name>"
        Then output training log

    Scenario: inquire job-list
        Given a user has job-list (at lest one)
            | jobname | status  | created time    |
            | job_003 | Success | 2019-5-21 19:35 |
            | job_002 | Error   | 2019-5-20 19:35 |
            | job_001 | Success | 2019-5-19 19:35 |
        When the user enters "dong train list"
        Then output job-list
            """
            | jobname | status  | created time    |
            | job_003 | Success | 2019-5-21 19:35 |
            | job_002 | Error   | 2019-5-20 19:35 |
            | job_001 | Success | 2019-5-19 19:35 |
            """

    Scenario Outline: No training to inquire
        Given a user has no training job 
        And the user has no training training log
        And the user uses commad to inquire
            | inquire-commad                       |
            | dong train status --job-name job_001 |
            | dong train log --job-name job_003    |
            | dong train list                      |
        When the user enters <inquire-commad>
        Then output:
            """
            There is no item to inquire
            """



