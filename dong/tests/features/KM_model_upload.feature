Feature: Model uploading for Knowledge Management

    For connecting to model management system,
    the ability for uploading model and/or model settings to let dong cloud host/manage is necessary.
    For this feature, "eval file" indicates the evaluation report for the model that the user wants to upload.

    Scenario: A user without correct permission couldn't upload a model with an eval file
        Given a user has no correct permission for uploading model with settings
        When the user enters "dong upload [model] [model-eval-file]"
        Then the CLI will output "Uploading the model with an eval file is not allowed under your permission."

    Scenario: A user with correct permission uploads model with a eval file
        Given a user has correct permission for uploading model with a eval file
        When the user enters "dong upload [model] [model-eval-file]"
        Then the CLI will upload the model to the dong cloud
        And the CLI will upload the model-eval-file
        And the dong cloud will parse the model-eval-file
        And the evaluation results of the model will be attached to the model's information

    Scenario: A user without correct permission couldn't upload remote model settings with an eval file
        Given a user has no correct permission for uploading model with settings
        When the user enters "dong upload [model-setting-file] [model-eval-file]"
        Then the CLI will output "Uploading the remote model settings with an eval file is not allowed under your permission."

    Scenario: A user with correct permission uploads remote model settings with an eval file
        Given a user has correct permission for uploading model settings with an eval file
        When the user enters "dong set-eval [model-setting-file] [model-eval-file]"
        Then the CLI will upload the model-setting-file to the dong cloud
        And the CLI will upload the model-eval-file
        And the dong cloud will parse the model-setting-file
        And the dong cloud will parse the model-eval-file
        And the evaluation results of the model will be attached to the model's information