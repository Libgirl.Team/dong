@train @info
Feature: Train job information

  As a user,
  he or she wants to get constantly updated and sorted information about train jobs on dong's cloud

  Scenario: Top train job
    When a user enters "dong train top"
    Then the console should display constantly updated and sorted information about my train jobs on dong's cloud
    And the user should see each job has a job ID, a train package hash, and its status

  Scenario: Train log
    When a user requests for the train log
    Then the console should display the train log in the chronicle manner
    And the user should see each job has a job ID, a train package hash, and its status

  Scenario: Detail train info
    Given there is a train job with ID ff123
    When a user requests for detail train info of that train job
    Then the user should get an VIM browser UI to see train job ID ff123's detail info including job ID, train package hash, initial model, final model name, and source code folder
    And the user should be able to browse the train source code folder using VIM browser UI

  # add dong train ps later
