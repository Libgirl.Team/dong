@endpoint @info
Feature: Endpoint info

  As a user,
  he or she wants to get endpoint info

  Scenario: Endpoint list
    When a user enters "dong endpoint list"
    Then the user should see the created endpoints
    And each endpoint should has an ID, a name, and a status
