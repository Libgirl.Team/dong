@help @version
Feature: Basic function    
    Basic utils that help users understand dong and get to know how to use
    
    Scenario: A user check the group of help documents
        When a user enters "dong help"
        Then output the help document on terminal

    Scenario: A user check a command's usage/options and help
        When a user enters "dong <group> <command> --help"
        Then output the corresponding usage for the <group> and <command>
        And output options info
    

        