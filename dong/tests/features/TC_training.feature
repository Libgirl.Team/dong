Feature: Training job

    prmission management across Training Job execution.

    Rule: Matrix of behavior and excution result for each role: https://docs.google.com/spreadsheets/d/14bcV8RbrLXYd8eKWi2BZhJD012aRaDWgKzZo6V10U3Q/edit?usp=sharing
    dong shall executes the <result> and <output> according to the Matrix

    Background:
        Given the user has already sign in
        And be a member of a team (A team represent that it has attached a paid plan)
        And the default status setting for a trained model for a team is private

    Scenario: Start a training job with permission, and not encounter any limits, dong sahll lanuch a training instance and record the user as "Starter" of said job
        Given the user is <role> whom the the <result> is Success
        When the user enters "dong train exec"
        Then dong lanuch a job instance
        And database record "Starter" to the user's account

        Examples:
            | role      | result  | dong's action                                                             |
            | Developer | Success | lanuch a job instance and database record "Starter" to the user's account |

    Scenario: Start a training job with permission, but encounter concurrency limit, dong sahll reject and output further information
        Given the team attached <plan>
        And the user is <role> whom the <result> of start a training job is Success
        But enconter concurrency limit
        When the user enters "dong train exec"
        Then dong reject the request
        And <output> further information

        Examples:
            | role  | plan    | dong's action      | dong output                                                                       |
            | Owner | Trainer | reject the request | "reach the training concurrency limit" + "please upgrade to purchase the powerup" |
            | Owner | Teamup  | reject the request | "reach the training concurrency limit" + "please purchase powerup"                |

    Scenario: Start a training job with permission, but encounter private model q'ty limit, dong sahll wait for further confirm and output confirm massage
        Given the team attached Trainer Plan
        And the user is <role> whom the <result> of start a training job is Success
        But enconter private model q'ty limit
        When the user enters "dong train exec"
        Then dong waiting for further confirm [y/n]
        And <output> confirm massage

        Examples:
            | role  | plan    | dong's action               | dong output                                                                                                                                                                      |
            | Owner | Trainer | waiting for further confirm | "reach the private model q'ty limit" + "the model will be set public" + "please change the excisting model status setting or upgrade to Teamup Plan for unlimited private model" |

    Scenario: Start a training job without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters "dong train exec"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role            | result        | dong's action      | dong output        |
            | Billing Manager | No Permission | reject the request | "permission error" |

    Scenario: If the user is the "Starter" of a job, when the user want stop that running job, dong sahll stop the training instance
        # A user being a Starter represent that the user being the role which have permission to start a training job
        Given the user is <Starter> of that job
        When the user enters CLI command
        # Pre-design CLI command: "dong train stop"
        Then dong stop the training instance

        Examples:
            | role      | startership | result  | dong's action              |
            | Developer | Starter     | Success | stop the training instance |

    Scenario: If the user is not the "Starter" of a job, only the user with permission can stop that running job
        Given the user is not <Starter> of the job
        And the user is <role> whom the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command: "dong train stop"
        Then dong stop the training instance

        Examples:
            | role          | Startership | result  | dong's action              |
            | Administrator | Non-Starter | Success | stop the training instance |

    Scenario: If the user is not the "Starter" of a job, and without permission to stop the running job, dong sahll reject and output permission error
        Given the user is not <Starter> of the job
        And the user is <role> whom the excution <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong train stop"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | Startership | result        | dong's action      | dong output        |
            | Developer | Non-Starter | No Permission | reject the request | "permission error" |

    Scenario: stop a running job without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong train stop"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role            | result        | dong's action      | dong output        |
            | Billing Manager | No Permission | reject the request | "permission error" |

    Scenario: For the *Access* to Job's name, description, if a user with lower access stratum want to execute higher access behavior, dong sahll deny and output permission error
        Given the user is <role> with the access stratum <result>
        When the user execute a higher access stratum <behavior>
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role            | access stratum result | behavior        | dong's action | dong output        |
            | Billing Manager | read                  | edit a job name | deny access   | "permission error" |