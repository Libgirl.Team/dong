@Cloud @DataIntegration
Feature: A user does some listing components and CRUD oprations
    # Need to handle permissions/confirm messages/duplicated names

    @Listing
    Scenario: A user lists all the components accessible from the cloud
        # CLI: dong component ls
        # List all the component that are in the user's permission
        Given a user has permission to access some components on the cloud
        When the user enters "dong component ls"
        Then the CLI will output all the components that are accessible from the cloud

    @Listing
    Scenario: A user lists one category of the components accessible from the cloud
        # CLI: dong component ls --category <component>
        # List all the component that are in the user's permission
        Given a user has permission to access some components of <component> category on the cloud
        When the user enters "dong component ls <component>"
        Then the CLI will output all the components of the <component> category on the cloud

    @Listing
    Scenario: A user doeesn't find any components accessible from the cloud
        Given a user doesn't have any components accessible from the cloud
        When the user enters "dong component ls"
        Then the CLI will output "No component found."

    @Listing
    Scenario: A user doeesn't find any components of category <component> accessible from the cloud
        Given a user doesn't have any components of category <component> accessible from the cloud
        When the user enters "dong component ls <component>"
        Then the CLI will output  "No component found."

    @Create
    Scenario Outline: A user tries to create a copy of a component and put it on the cloud
        # CLI: dong component create [component] [cloud-name]
        Given a user has the permission to create a component on the cloud
        And the <cloud-name> at <component> is <status>
        When the user enters "dong component create <component> <cloud-name>"
        Then the CLI will <action>
        And the <cloud-name> at <component> on the cloud will <result>

        Examples:
            | status    | action                                           | result                     |
            | available | create a copy, upload and show upload message    | be created after uploading |
            | exists    | show message to indicate component name occupied | remain the same            |

    @Read
    Scenario Outline: A user accesses a component on the cloud and download to local
        # CLI: dong component read [component] [cloud-name]
        Given a user has the permission to access <cloud-name> at <component> on the cloud
        And the user enters "dong component read <component> <cloud-name>"
        And the CLI asks the user to confirm to overwrite the local <component>
        When the user answers <ans>
        Then the CLI will <action>

        Examples:
            | ans                 | action                                                                   |
            | "YES" or "Y" or "y" | download <cloud-name> at <component> and overwrite the local <component> |
            | others              | output "Read cancelled."                                                 |


    @Update
    Scenario Outline: A user uploads a component to the cloud and replaces an existing component
        # CLI: dong component update [component] [cloud-name]
        Given a user has the permission to overwrite <cloud-name> at <component> on the cloud
        And the user enters "dong component update <component> <cloud-name>"
        And the CLI asks the user to confirm to upload and overwrite the cloud <component> <cloud-name>
        When the user answers <ans>
        Then the CLI will <action>

        Examples:
            | ans                 | action                                                                   |
            | "YES" or "Y" or "y" | upload <component> and overwrite the cloud's <cloud-name> at <component> |
            | others              | output "Update cancelled."                                               |

    @Delete
    Scenario: A user deletes a component on the cloud
        # CLI: dong component delete [component] [cloud-name]
        Given a user has permission to delete a component named <cloud-name> at <component> on the cloud
        When the user enters "dong component delete <component> <cloud-name>"
        And the user confirms to delete as the CLI ask user to confirm
        Then the <cloud-name> on the cloud is scheduled to deleted in <number> days and is moved to <recycle>
        And the CLI will output "<cloud-name> is deleted successfully!"

    @Delete
    Scenario: A user wants to delete a component on the cloud but choose No while confirming
        # CLI: dong component delete [component] [cloud-name]
        Given a user has permission to delete a component named <cloud-name> at <component> on the cloud
        When the user enters "dong component delete <component> <cloud-name>"
        And the user choose No as the CLI ask user to confirm
        Then the CLI will output "Delete action is cancelled!"

    @Delete
    Scenario: A user tries to delete a non-existing component on the cloud
        Given there is no <cloud-name> at <component> on the cloud
        When a user enters "dong component delete <component> <cloud-name>"
        Then the CLI will output "<component> <cloud-name> is not found on the cloud!"

    @Delete
    Scenario: A user fails to delete a component on the cloud
        Given a user has no permission to delete the <cloud-name> at <component> on the cloud
        When the user enters "dong component delete <component> <cloud-name>"
        Then the CLI will output "You have no permission to delete the <component> <cloud-name>!"

    @Rename
    Scenario Outline: A user tries to rename a component on the cloud
        Given the <cloud-name> at <component> status is <status> on the cloud
        And the <cloud-rename> at <component> status is <re-status> on the cloud
        When the user enters "dong component rename <component> <cloud-name> <cloud-rename>"
        Then the CLI will <action>

        Examples:
            | status       | re-status    | action                                                                                                                       |
            | not existing | occupied     | output "The <cloud-name> at <component> doesn't exist!"                                                                      |
            | not existing | not existing | output "The <cloud-name> at <component> doesn't exist!"                                                                      |
            | existing     | not existing | output "Renaming <cloud-name> at <component> to <cloud-rename>" and rename the <cloud-anme> at <component> to <cloud-rename> |
            | existing     | occupied     | output "The <cloud-rename> at <component> already exists! Please use another name instead."                                  |

    @Recover
    Scenario Outline: A user tries to recover a component on the cloud
        Given the <cloud-name> at <component> at <recycle> status is <recycle-status> on the cloud
        And the <cloud-name> at <component> status is <status> on the cloud
        When the user enters "dong component recover <component> <cloud-name>"
        Then the CLI will <action>

        Examples:
            | recycle-status | status       | action                                                                                                                              |
            | existing       | not existing | output "Recover the <cloud-name> <component>." and move the <cloud-name> at <component> at <recycle> to <cloud-name> at <component> |
            | existing       | existing     | output "The <cloud-name> at <component> already exists, do you want to overwrite?(YES/NO)"                                                    |
            | not existing   | not existing | output "The <cloud-name> at <component> at recycle doesn't exist, do nothing."                                                      |
            | not existing   | existing     | output "The <cloud-name> at <component> at recycle doesn't exist, do nothing."                                                      |

    @Recover
    Scenario Outline: A user tries to recover a component on the cloud and answers the question of overwriting
        Given the <cloud-name> at <component> at <recycle> status is existing on the cloud
        And the <cloud-name> at <component> status is existing on the cloud
        And the user enters "dong component recover <component> <cloud-name>"
        And the user answers <ans> for the question "The <cloud-name> at <component> already exists, do you want to overwrite?" the CLI has asked
        Then the CLI will <action>

        Examples:
            # <other> means strings that are not "YES"
            | ans | action |
            | YES | output "Recover and overwrite the <cloud-name> <component>." and overwrite the <cloud-name> at <component>|
            | <other> | output "Recovering is cancelled" |