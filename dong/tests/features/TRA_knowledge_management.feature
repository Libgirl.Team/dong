@record
Feature: Cloud record for reproducibility and knowledge management

    - When to record? At training and serving
    - What to record for each item? training parameters/load or new model/training logs
    - How to query the record? using CLI commands
    - How to present the record? output as file or print to shell

    # Logs, commands and nouns definitions
    - dmesg: logs from command "dmesg"
    - system log: logs from file "/var/log/syslog"
    - datetime: date and time from command "date" (export LC_TIME=en_US.utf8 first)
    - dataset name: the dataset folder or file name
    - dataset size: the size outputted from command "du -sh" of the dataset directory
    - cpu usage: the average util rate percentage during training, should be gotten and averaged by getting info from commnad "iostat"
    - gpu usage: the avergae util rate percentage during training, should be gotten and averaged be getting info from command "gpustat" (Need to "pip install gpustat" first)
    # Example (In this case, the util rate is 11%)
    # $ gpustat
    # desolve-G531GV       Wed Aug 14 10:51:15 2019  430.26
    # [0] GeForce RTX 2060 | 46'C,  11 % |   639 /  5934 MB | desolve(228M) desolve(126M) desolve(240M) desolve(43M)
    - training time: the total length of time from the time training starts till the time training ends
    - framework log: logs from framework outputted on the screen
    - version-hash: Get a directory total hash by command: "find [project-dir] -type f -exec md5sum {} \; | md5sum"
    # Example (At a dong project directory on the cloud)
    # $ find . -type f -exec md5sum {} \; | md5sum
    # b2d5d3a5e102aae48eb6ff36c602ac75  -
    - last-commit: The last git commit's hash that the user committed. (We might need remind the user to commit those not staged changes before training, or the version will lose its meaning.)
    - components: listed all components(3rd level of a dong-project's folders), and show their statuses (local or cloud-component)

    @logtiming
    Scenario: A training log will be recorded when a user start training
        Given a user is ready to train a model
        When the user starts training
        Then the dong cloud will start to record <system log> and <logging log>
        And the start <datetime> will be recorded

    @logtiming
    Scenario Outline:  A system log will be recorded when an endpoint is bringup
        # serving to record input/output/error
        # For logging library please see: https://docs.python.org/3/library/logging.html
        # In this example, d = {'user': 'CYL'}

        Given a user has a model trained
        And the user has implemented serving functions
        And the user has added some logs of logging for serving functions
        When the user bringups an endpoint
        Then the dong cloud will start to record <system log> and <logging log> at serving
        And the start <datetime> will be recorded

        Examples:
            | system log                                                                                          | logging log                                                  | datetime                     |
            | [    6.729145] NVRM: loading NVIDIA UNIX x86_64 Kernel Module  430.26  Tue Jun  4 17:40:52 CDT 2019 | logger.info('Ready for inferencing: %s', 'mnist', extra = d) | Wed Aug 14 09:51:54 CST 2019 |

    @parameters
    Scenario Outline: Parameters for a training are recorded
        # define what parameters need to be recorded
        Given  a user is ready to train a model
        And the "get_config" functions is well implemented in the default.py
        When the user starts training
        Then the dong cloud will record parameters of the training, including the <optimizer>, <loss function>, <metrics>, <epoch num>

        ### In this context, get_config is supposed to be as the following:
        # def get_config():
        #     return OrderedDict([("self.compile", (":optimizer", "adam",
        #                                           ":loss", "sparse_categorical_crossentropy",
        #                                           ":metrics",  [ "accuracy" ])),
        #                         ("self.fit", (":epochs", 5))])
        ###


        Examples:
            | optimizer | loss function                     | metrics        | epoch num |
            | "adam"    | "sparse_categorical_crossentropy" | [ "accuracy" ] | 5         |

    @dataset
    Scenario Outline: Dataset name and size for a training are recorded
        # define what data needs to be recroded
        Given a user is ready to train a model
        When the user starts training
        Then the dong cloud will record the data of the training, including the dataset <name> and <size>.

            | name           | size  |
            | mnist_20190807 | 10.9M |


    @traininglog
    Scenario: Training log for a training is recorded
        # define what log needs to be recorded
        Given a user is ready to train a model
        And the user doesn't have any changes that are not staged to a commit yet
        When the user starts training
        Then the dong cloud will start to record the framework log (<logging log> and <framework log>)
        And the environment settings will also be recorded (<env>)
        And the component status will also be recorded (local or using cloud-component)
        And the code version by hash of the dong working directory on the cloud will also be recorded (<version-hash>)
        And the code version by last git commit will also be recorded (<last-commit>)
        And the final evaluation result will also be recorded (<evaluation>)
        And the system log at the training duration will also be recorded (<systesm log>)
        And the cpu and/or gpu usage will also be recorded (<cpu usage>, <gpu usage>)
        And the datetimes will also be recorded (<start time>, <end time>)

        Examples:
            | env                                      | version-hash                     | last-commit                              | evaluation                     | cpu usage | gpu usage | start time      | end time        | framework log                                                                                                                                                                 |
            | Python 3.6.7, Ubuntu 18.04,  [$pip list] | 59a53cf2da77b757a94ed25747b39dfb | fdcf2fcc457e717eeaddb631f071790eb1f39607 | accuracy: 0.851, loss: 5.01E-5 | 40%       | 0%        | 20190807_091535 | 20190807_110802 | 2019-08-14 11:30:13.953580: I tensorflow/core/platform/cpu_feature_guard.cc:142] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA |

    @query
    Scenario Outline: Query all records for a training
        # CLI: dong logs ls (all logs summary)
        # CLI: dong component ls --dataset
        # CLI: dong logs --train <job-name>
        # CLI: dong logs --train <job-name> <category>
        # CLI: dong logs --train <job-name> --parameters
        # CLI: dong logs --endpoint-model <endpoint-name>

        # When a training is finished, records should be accessible using <job-name>
        Given a user has a finished training
        When the user enters "dong train logs <job-name>"
        Then the CLI will download all the records to a file named <recfile> at local
        And the CLI will output "Log of <job-name> is provided at <recfile>"
        And the CLI will output some basic info of this training (<result>, <version-hash>, <last-commit>, <metrics>, <training time>, <cpu usage>, <gpu usage>, ... etc)

        Examples:
            | recfile                          | version-hash                     | last-commit                              | result                                         | metrics                        | training time | cpu usage | gpu usage |
            | train_result_20190807_052044.log | 59a53cf2da77b757a94ed25747b39dfb | fdcf2fcc457e717eeaddb631f071790eb1f39607 | Success, output CYL_mnist_20190807_052044.hdf5 | accuracy: 0.851, loss: 5.01E-5 | 0d 0h 20m 7s  | 50%       | 80%       |

    @query
    Scenario: Query seperate records for a training
        # When a training is finished, records should be accessible using <job-name>
        # Should provides some options for the user to choose what to see
        Given a user has a finished training
        When the user enters command line
        Then the CLI will output the log of <category>

    @openfile
    Scenario Outline: Open a record file for a training
        Given a user has a <recfile> at local
        When the user open the <recfile>
        Then the user will see something like
"
[job-name]
jobenp3

[version-hash]
59a53cf2da77b757a94ed25747b39dfb

[last-commit]
commit fdcf2fcc457e717eeaddb631f071790eb1f39607 (HEAD -> master, origin/master, origin/HEAD)
Author: Shaka Chen <scchen@libgirl.com>
Date:   Tue Jul 2 10:42:17 2019 +0800
Title: url added

[components]
config                  local
model                  local
data                      CYL/mnist_20190801
service                 local
etl                          local
tune                      local
rawdata               CYL/mnist_20190801
modelstore        CYL/mnist_20190725
proceed               local

[output-model]
CYL_mnist_20190807_052044.hdf5

[environment]
env: Python 3.6.7, Ubuntu 18.04

[framework]
tensorflow-gpu 1.14.0

[training result]
Result: Success
Start Time:  20190807_091535
End Time: 20190807_111535
Training Time:  0d 2h 0m 0s
avg cpu usage: 50%
avg gpu usage 80%
accuracy: 0.9788, loss: 0.0695

[evaluation result]
accuracy: 0.9788, func1: 0.9822, func2: 0.8344

[dataset]
dataset name mnist_20190807
dataset size 10.9M

[parameters]
optimizer: adam
loss function:  sparse_categorical_crossentropy
metrics:
accuracy
epochs: 5

[pip list]
Package                 Version
----------------------- -------------------
absl-py                 0.7.1
......(omitted)
tensorboard             1.14.0
tensorflow-estimator    1.14.0
tensorflow-gpu          1.14.0

[logging log]
2019-08-07 10:30:13.953580: 'Ready for inferencing: mnist', 'user: CYL'
...(omitted)

[framework log]
...(omitted)
2019-08-14 11:27:35.470328: W tensorflow/compiler/jit/mark_for_compilation_pass.cc:1412] (One-time warning): Not using XLA:CPU for cluster because envvar TF_XLA_FLAGS=--tf_xla_cpu_global_jit was not set.  If you want XLA:CPU, either set that envvar, or use experimental_jit_scope to enable XLA:CPU.  To confirm that XLA is active, pass --vmodule=xla_compilation_cache=1 (as a proper command-line flag, not via TF_XLA_FLAGS) or set the envvar XLA_FLAGS=--xla_hlo_profile.
Epoch 1/5
60000/60000 [==============================] - 3s 57us/sample - loss: 0.2187 - acc: 0.9363
Epoch 2/5
60000/60000 [==============================] - 3s 47us/sample - loss: 0.0970 - acc: 0.9704
Epoch 3/5
60000/60000 [==============================] - 3s 45us/sample - loss: 0.0695 - acc: 0.9778
Epoch 4/5
60000/60000 [==============================] - 3s 46us/sample - loss: 0.0524 - acc: 0.9834
Epoch 5/5
60000/60000 [==============================] - 3s 45us/sample - loss: 0.0424 - acc: 0.9866
10000/10000 [==============================] - 0s 21us/sample - loss: 0.0695 - acc: 0.9788
Evaluation Accuracy: 0.9788

[dmesg]
...(omitted)
[   19.745124] Generic PHY r8169-300:00: attached PHY driver [Generic PHY] (mii_bus:phy_addr=r8169-300:00, irq=IGNORE)
[   19.865984] r8169 0000:03:00.0 eno2: Link is Down
[   23.580380] wlo1: authenticate with b0:b2:dc:c5:92:7b
[   23.587858] wlo1: send auth to b0:b2:dc:c5:92:7b (try 1/3)
[   23.626535] wlo1: authenticated
[   23.629896] wlo1: associate with b0:b2:dc:c5:92:7b (try 1/3)
[   23.635679] wlo1: RX AssocResp from b0:b2:dc:c5:92:7b (capab=0x411 status=0 aid=2)
[   23.638994] wlo1: associated
[   23.710469] IPv6: ADDRCONF(NETDEV_CHANGE): wlo1: link becomes ready
[   44.650801] asus_wmi: Unknown key cf pressed
[   59.307174] Bluetooth: RFCOMM TTY layer initialized
[   59.307176] Bluetooth: RFCOMM socket layer initialized
[   59.307179] Bluetooth: RFCOMM ver 1.11
[   60.280896] rfkill: input handler disabled
[  105.600424] ucsi_ccg 0-0008: failed to reset PPM!
[  105.600432] ucsi_ccg 0-0008: PPM init failed (-110)
[  110.308117] usb 1-1: new full-speed USB device number 4 using xhci_hcd
[  110.458911] usb 1-1: New USB device found, idVendor=0b05, idProduct=1847, bcdDevice= 1.03
[  110.458916] usb 1-1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[  110.458919] usb 1-1: Product: ROG STRIX IMPACT
[  110.458922] usb 1-1: Manufacturer: ASUS
[  110.463015] input: ASUS ROG STRIX IMPACT as /devices/pci0000:00/0000:00:14.0/usb1/1-1/1-1:1.0/0003:0B05:1847.0005/input/input28
[  110.463669] hid-generic 0003:0B05:1847.0005: input,hidraw4: USB HID v1.11 Pointer [ASUS ROG STRIX IMPACT] on usb-0000:00:14.0-1/input0
[  110.464776] input: ASUS ROG STRIX IMPACT Keyboard as /devices/pci0000:00/0000:00:14.0/usb1/1-1/1-1:1.1/0003:0B05:1847.0006/input/input29
[  110.524410] input: ASUS ROG STRIX IMPACT Consumer Control as /devices/pci0000:00/0000:00:14.0/usb1/1-1/1-1:1.1/0003:0B05:1847.0006/input/input30
[  110.524844] hid-generic 0003:0B05:1847.0006: input,hidraw5: USB HID v1.11 Keyboard [ASUS ROG STRIX IMPACT] on usb-0000:00:14.0-1/input1
...(omitted)

[/var/log/syslog]
...(omitted)
Aug 14 10:24:03 desolve-G531GV systemd[1661]: Starting flatpak document portal service...
Aug 14 10:24:03 desolve-G531GV dbus-daemon[1686]: [session uid=1000 pid=1686] Successfully activated service 'org.freedesktop.portal.Documents'
Aug 14 10:24:03 desolve-G531GV systemd[1661]: Started flatpak document portal service.
Aug 14 10:24:04 desolve-G531GV kernel: [ 6076.411913] kauditd_printk_skb: 33 callbacks suppressed
Aug 14 10:26:26 desolve-G531GV upowerd[1283]: energy 62.849000 bigger than full 62.833000
Aug 14 10:45:58 desolve-G531GV systemd-resolved[1049]: Server returned error NXDOMAIN, mitigating potential DNS violation DVE-2018-0001, retrying transaction with reduced feature level UDP.
Aug 14 10:46:01 desolve-G531GV systemd[1]: Reloading.
Aug 14 10:46:02 desolve-G531GV systemd[1]: message repeated 2 times: [ Reloading.]
Aug 14 10:55:01 desolve-G531GV CRON[15132]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
Aug 14 10:59:18 desolve-G531GV systemd-resolved[1049]: Using degraded feature set (UDP) for DNS server 192.168.1.1.
Aug 14 11:03:55 desolve-G531GV systemd[1]: Started Run anacron jobs.
Aug 14 11:03:55 desolve-G531GV anacron[15985]: Anacron 2.3 started on 2019-08-14
Aug 14 11:03:55 desolve-G531GV anacron[15985]: Normal exit (0 jobs run)
Aug 14 11:05:01 desolve-G531GV CRON[16034]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
Aug 14 11:15:01 desolve-G531GV CRON[16503]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
Aug 14 11:17:01 desolve-G531GV CRON[16613]: (root) CMD (   cd / && run-parts --report /etc/cron.hourly)
Aug 14 11:25:01 desolve-G531GV CRON[17126]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
...(omitted)
"