@train
Feature: Resume a stopped train

  As a user,
  he or she wants to resume my stopped train task on the dong's cloud.

  Scenario Outline: no such job
    Given there is no on training job with job number 2
    When a user enters "dong train <arg> 2"
    Then output: "no such train job"

    Examples:
    | arg    |
    | resume |
    | bg     |

  Scenario Outline: resume success
    Given a stopped train with ID k8s
    And its job number is 2
    When a user enters "dong train <arg> 2"
    Then the user can see that the train k8s is resumed on training

    Examples:
    | arg    |
    | resume |
    | bg     |

