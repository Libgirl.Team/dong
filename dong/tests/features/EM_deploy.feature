@endpoint
Feature: Deploy model to an endpoint

  As a user,
  he or she wants to deploy his/her trained model to an endpoint

  Scenario: Model <name> exists 
    Given a user has a model <name> on dong's cloud
    When the user uses dong to deploy <name>
    Then output the deployment starts
    And output a deployment ID along with the deployment <name>

  Scenario: Model <name> doesn't exists
    Given a user doesn't have a model <name> on dong's cloud
    When the user uses dong to deploy <name>
    Then the deployment should fail
    And output: "Model <name> doesn't exists!"
