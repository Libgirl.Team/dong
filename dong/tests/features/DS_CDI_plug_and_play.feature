@Cloud @DataIntegration
Feature: A user uses plug-and-play feature to train model

    Dong provides plug-and-play feature to let data that are already
    on the cloud directly used for training without uploading again.
    The phrase "component(s)" indicates any second level folder under a dong project.

    @BadCommand
    Scenario: A user enters component commands at a none dong project directory
        Given a user is not at a dong working directory
        When the user enters "dong component <cmd>"
        Then the CLI will output "You're NOT at a dong working directory!"

    @Status
    Scenario: A user lists all components' plug-and-play statuses
        # CLI: dong component status

        # API:
        # method: GET 
        # pass-in [user-permission]
        # Client send request to server from local with parameters
        # Server check user permission
        # Server response result: <components-status>

        # A plug-and-play config file should be generated if not exist
        # Should output "local" if using local component
        # Should output "<cloud-component>" if using plug-and-play feature
        Given a user is at a dong working directory
        And the env setting file is well defined
        When the user enters "dong component status"
        Then the CLI will output each component's plug-and-play status according to the local env setting file

    @Status
    Scenario: A user lists all components' plug-and-play statuses while setting file is empty or wrong, CLI shows all statuses local
        Given a user is at a dong working directory
        And the env setting file is missing or has wrong format
        When the user enters "dong component status"
        Then the CLI will output all components' plug-and-play statuses as "local"

    @LoadConfig
    Scenario: A user loads the plug-and-play config correctly
        # CLI: dong component confload <config-file-path>
        # The default config file should be update to the status of <config-file-path> indicates
        Given a user is at a dong working directory
        And the user has a <config-file-path>
        And the <config-file-path> is written in correct format
        When the user enters "dong component confload <config-file-path>"
        Then the CLI will load the <config-file-path>
        And the env setting file will be overwritten by the statuses given by the <config-file-path>
        And the CLI will output the new statuses of plug-and-play

    @LoadConfig
    Scenario Outline: A user fails to load the plug-and-play config
        Given a user is at a dong working directory
        And the <config-file-path> is at the <status>
        When the user enters "dong component confload <config-file-path>"
        Then the CLI will output <result>

        Examples:
            | config-file      | status                  | result                                 |
            | component.config | doesn't exist           | component.config not found!            |
            | mycomponent.conf | written in wrong format | can't load mycomponent.conf correctly! |

    @ExportConfig
    Scenario: A user exports config in the default file path
        # CLI: dong component confexport
        Given a user is at a dong working directory
        When the user enters "dong component confexport"
        Then the config file will be exported at the dong project root path
        And the name of config file is [default-name]
    # how to define default name?

    @ExportConfig
    Scenario: A user exports config in the specific file path
        # CLI: dong component confexport <file-path>
        Given a user is at a dong working directory
        And the <file-path> is good to be used
        When the user enters "dong component confexport <file-path>"
        Then config file will generated in specific <file-path>

    @Switch
    Scenario Outline: A user tries to turn on a component's plug-and-play
        # CLI: dong component plug --on <component> <cloud-component>

        # API:
        # method: POST
        # passing [user-permission, current-directory, component, cloud-component]
        # Client check if is at a dong working directory
        # And check component is valid
        #   Client send request to server from local with parameters
        #   Server check user user-permission
        #   Server check component exist
        #   Server turn on component's plug-and-play
        #   Server response result: <result>
        #   Client save <result> into the env file

        Given a user is at a dong working directory
        And the <cloud-component> status at <component> is <status> according to the user's permission
        When the user enters "dong component on <component> <cloud-component>"
        Then the CLI will <action>
        And the CLI will output <result>

        Examples:
            | status        | action                                                        | result                                                |
            | not found     | do nothing                                                    | <cloud-component> not found                           |
            | no permission | do nothing                                                    | no permission to access <cloud-component>             |
            | existed       | set <component> to <cloud-component> in the env settinig file | Setting <component> to <cloud-component> successfully |

    @Switch
    Scenario: A user turns off a component's plug-and-play
        # CLI: dong component plug --off <component>
        Given a user is at a dong working directory
        When the user enters "dong component off <component>"
        Then the CLI will set <component> to local in the env setting file
        And the CLI will output "Set <component> too local successfully"

    @Switch
    Scenario: A user turns off all components' plug-and-play
        # CLI: dong component plug --all-off
        Given a user is at a dong working directory
        When the user enters "dong component off all"
        Then the CLI will set all components to local in the env setting file
        And the CLI will output "Set all components to local successfully"