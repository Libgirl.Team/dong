@train
Feature: Interrupt a train
  As a user,
  he or she wants to send a signal to interrupt my train tasks on dong's cloud.

  Background:
    Given a user has a on processing train with ID k8s on dong's cloud
    
  Scenario: kill a train
    When the user enters"dong train kill k8s"
    Then the user can see that the train k8s deleted

  Scenario: stop a train
    Given there is no stopped train
    When the user enters "dong train stop k8s"
    Then the user can see that the train k8s stopped with job number 1
    
