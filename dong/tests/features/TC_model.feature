Feature: Model

    prmission management across model execution.

    Rule: Matrix of behavior and excution result for each role: https://docs.google.com/spreadsheets/d/14bcV8RbrLXYd8eKWi2BZhJD012aRaDWgKzZo6V10U3Q/edit?usp=sharing
    dong shall executes the <result> and <output> according to the Matrix

    Background:
        Given the user has already sign in
        And be a member of a team (A team represent that it has attached a paid plan)
        And the default model status setting for a created model for a team is private

    Scenario: Create a model with permission, and not encounter q'ty limit, dong sahll store the model as a private model under the team
        Given the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command:"dong model import/fork"
        Then dong store the model status as private model under the team

        Examples:
            | role      | result  | dong's action                                   |
            | Developer | Success | store the model as private model under the team |

    Scenario: Create a model with permission, but encounter the private model q'ty limit, dong sahll wait for further confirm and output confirm massage
        Given the team attached Trainer Plan
        And the user is <role> whom the <result> of Create a model is Success
        But enconter private model q'ty limit
        When the user enters CLI command
        # Pre-design CLI command:"dong model import/fork"
        Then dong waiting for further confirm [y/n]
        And <output> confirm massage

        Examples:
            | role  | plan    | dong's action               | dong output                                                                                                                                                                      |
            | Owner | Trainer | waiting for further confirm | "reach the private model q'ty limit" + "the model will be set public" + "please change the excisting model status setting or upgrade to Teamup Plan for unlimited private model" |

    Scenario: Create a model without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong model import/fork"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role            | result        | dong's action      | dong output        |
            | Billing Manager | No Permission | reject the request | "permission error" |

    Scenario: Switch the excisting model status setting with permission, and not encounter q'ty limit, dong sahll switch said model's status
        Given the excisting model's status is <model status>
        And the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command:"dong model setting -public or -private NAME"
        Then dong update that model's status

        Examples:
            | excisting model status | role       | result  | CLI command                 | dong's action                                     |
            | public                 | Administor | Success | dong model setting -private | update that model's status from public to private |
            | private                | Owner      | Success | dong model setting -public  | update that model's status from private to public |

    Scenario: Switch the excisting model from public to private, with permission, but encounter the private model q'ty limit, dong sahll reject and output further information
        Given the team attached Trainer Plan
        And the excisting model's status is public
        And the user is <role> whom the <result> of switch the excisting model status setting is Success
        But enconter private model q'ty limit
        When the user enters CLI command
        # Pre-design CLI command:"dong model setting -private NAME"
        Then dong reject the request
        And <output> further information

        Examples:
            | excisting model status | role       | result  | CLI command                 | dong's action      | dong output                                                                                        |
            | public                 | Administor | Success | dong model setting -private | reject the request | "reach the private model q'ty limit" + "please upgrade to Teamup Plan for unlimited private model" |

    Scenario: Switch the excisting model status setting without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong model setting -public or -private NAME"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | result        | dong's action      | dong output        |
            | Developer | No Permission | reject the request | "permission error" |

    Scenario: Lock the existing model with permission, dong sahll prohibit the model from being modify and delete
        Given the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command:"dong model lock NAME"
        Then dong prohibit the model from being modify and delete

        Examples:
            | role       | result  | dong's action                                   |
            | Administor | Success | prohibit the model from being modify and delete |

    Scenario: Lock the existing model without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong model lock NAME"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | result        | dong's action      | dong output        |
            | Developer | No Permission | reject the request | "permission error" |

    Scenario: delete the existing model with permission, dong sahll delete the said model
        Given the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command:"dong model delete NAME"
        Then delete the model

        Examples:
            | role       | result  | dong's action         |
            | Administor | Success | delete the said model |

    Scenario: delete the existing model without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong model delete NAME"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | result        | dong's action      | dong output        |
            | Developer | No Permission | reject the request | "permission error" |

    Scenario: For the *Access* to the model KM records & logs, if a user with lower access stratum want to execute higher access behavior, dong sahll deny and output permission error
        Given the user is <role> with the access stratum <result>
        When the user execute a higher access stratum <behavior>
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | access stratum result | behavior        | dong's action | dong output        |
            | Developer | comment               | delete a record | deny access   | "permission error" |
            | FAE       | read                  | leave a comment | deny access   | "permission error" |

    Scenario: For the *Access* to model's name, description, if a user with lower access stratum want to execute higher access behavior, dong sahll deny and output permission error
        Given the user is <role> with the access stratum <result>
        When the user execute a higher access stratum <behavior>
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role            | access stratum result | behavior          | dong's action | dong output        |
            | Billing Manager | read                  | edit a model name | deny access   | "permission error" |

