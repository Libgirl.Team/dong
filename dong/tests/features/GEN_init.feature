@init
Feature: Initialize project file
    As a user wants to initialize project.
    dong determine if the project can be initialized,
    and initialize project.

    Scenario: Initialize project success
        Given a user has a project directory that can be initialize.
            | project-path                  |
            | /home/dong/some_dong_project/ |
            | /home/company/test/big_data/  |
        When the user enters "dong init"
        Then dong check project
        And dong initialize project
        And output:
        Initialized dong project in <project-path>
 
    Scenario: Initialize stopped
        Given a user has a already initialized project.
        When the user enters "dong init"
        Then dong check project
        And dong return
        And output:
            """
            Stopped.
            The current project is already initialized.
            """

# let's do after alpha
# this scenario requires futher decomposition
# we don't have to do it for Alpha version
# Scenario: 無法被dong初始化的專案
#     Given 使用者擁有一份dong無法解析的目錄
#     When 使用者於當前目錄下 輸入 "dong init"
#     Then dong檢查專案
#     And 在終端機上 顯示 "專案不符合規範 請閱讀 dong generate 文件"


