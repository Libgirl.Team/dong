Feature: Data

    prmission management across data execution.

    Rule: Matrix of behavior and excution result for each role: https://docs.google.com/spreadsheets/d/14bcV8RbrLXYd8eKWi2BZhJD012aRaDWgKzZo6V10U3Q/edit?usp=sharing
    dong shall executes the <result> and <output> according to the Matrix

    Background:
        Given the user has already sign in
        And be a member of a team (A team represent that it has attached a paid plan)
        And the default data status setting for a created data for a team is private

    Scenario: Create a data with permission, and not encounter q'ty limit, dong sahll store the data as a private data under the team
        Given the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command:"dong data upload/import/fork"
        Then dong store the data status as private data under the team

        Examples:
            | role      | result  | dong's action                                 |
            | Developer | Success | store the data as private data under the team |

    Scenario: Create a data with permission, but encounter the private data q'ty limit, dong sahll wait for further confirm and output confirm massage
        Given the team attached Trainer Plan
        And the user is <role> whom the <result> of Create a data is Success
        But enconter private data q'ty limit
        When the user enters CLI command
        # Pre-design CLI command:"dong data upload/import/fork"
        Then dong waiting for further confirm [y/n]
        And <output> confirm massage

        Examples:
            | role  | plan    | dong's action               | dong output                                                                                                                                                                  |
            | Owner | Trainer | waiting for further confirm | "reach the private data q'ty limit" + "the data will be set public" + "please change the excisting data status setting or upgrade to Teamup Plan for unlimited private data" |

    Scenario: Create a data without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong data upload/import/fork"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role            | result        | dong's action      | dong output        |
            | Billing Manager | No Permission | reject the request | "permission error" |

    Scenario: Switch the excisting data status setting with permission, and not encounter q'ty limit, dong sahll switch said data's status
        Given the excisting data's status is <data status>
        And the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command:"dong data setting -public or -private NAME"
        Then dong update that data's status

        Examples:
            | excisting data status | role       | result  | CLI command                | dong's action                                    |
            | public                | Administor | Success | dong data setting -private | update that data's status from public to private |
            | private               | Owner      | Success | dong data setting -public  | update that data's status from private to public |

    Scenario: Switch the excisting data from public to private, with permission, but encounter the private data q'ty limit, dong sahll reject and output further information
        Given the team attached Trainer Plan
        And the excisting data's status is public
        And the user is <role> whom the <result> of switch the excisting data status setting is Success
        But enconter private data q'ty limit
        When the user enters CLI command
        # Pre-design CLI command:"dong data setting -private NAME"
        Then dong reject the request
        And <output> further information

        Examples:
            | excisting data status | role       | result  | CLI command                | dong's action      | dong output                                                                                      |
            | public                | Administor | Success | dong data setting -private | reject the request | "reach the private data q'ty limit" + "please upgrade to Teamup Plan for unlimited private data" |

    Scenario: Switch the excisting data status setting without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong data setting -public or -private NAME"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | result        | dong's action      | dong output        |
            | Developer | No Permission | reject the request | "permission error" |

    Scenario: Lock the existing data with permission, dong sahll prohibit the data from being access, write and read
        Given the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command:"dong data lock NAME"
        Then dong prohibit the data from being access, write and read

        Examples:
            | role       | result  | dong's action                                       |
            | Administor | Success | prohibit the data from being access, write and read |

    Scenario: Lock the existing data without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong data lock NAME"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | result        | dong's action      | dong output        |
            | Developer | No Permission | reject the request | "permission error" |

    Scenario: delete the existing data with permission, dong sahll delete the said data
        Given the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command:"dong data delete NAME"
        Then delete the data

        Examples:
            | role       | result  | dong's action        |
            | Administor | Success | delete the said data |

    Scenario: delete the existing data without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong data delete NAME"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | result        | dong's action      | dong output        |
            | Developer | No Permission | reject the request | "permission error" |

    Scenario: For the *Access* to the data KM records & logs, if a user with lower access stratum want to execute higher access behavior, dong sahll deny and output permission error
        Given the user is <role> with the access stratum <result>
        When the user execute a higher access stratum <behavior>
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | access stratum result | behavior        | dong's action | dong output        |
            | Developer | comment               | delete a record | deny access   | "permission error" |
            | FAE       | read                  | leave a comment | deny access   | "permission error" |

    Scenario: For the *Access* to data's name, description, if a user with lower access stratum want to execute higher access behavior, dong sahll deny and output permission error
        Given the user is <role> with the access stratum <result>
        When the user execute a higher access stratum <behavior>
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role            | access stratum result | behavior         | dong's action | dong output        |
            | Billing Manager | read                  | edit a data name | deny access   | "permission error" |