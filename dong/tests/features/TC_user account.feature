Feature: User create a Team

    Feature Description

    Scenario: Check whether having an account when a user want to create a Team
        Given the user don't have an account
        When the user enters "dong team create"
        Then output: "please sign up first"

    Scenario: A user create the 1st Team
        Given the user have an account
        And: that account attached <plan>
        When the user enters "dong team create"
        Then <output>
        And: <action>

        Examples:
            | plan        | output                                                      | action                                         |
            | no plan     | "please select Trainer or Teamup plan to create a team"     | direct to the Billing Plan Selection interface |
            | unpaid plan | "please upgrade to Trainer or Teamup plan to create a team" | direct to the payment interface                |
            | paid plan   | "Team name:" + "Description (optional)"                     | the user been default as Team Owner            |

    Scenario: A user create the 2nd- Team
        Given the user already have one or more teams
        When the user enters "dong team create"
        Then output: "please select a plan for this newly create team"
        And: direct to the Billing Plan Selection interface

