Feature: Team

    Team management permission

    Scenario: Add new member into a Team (name: "Nancy's team")
        Given the user is <role>
        When the user enters "dong team add"
        And: given an account email
        And: given a role
        Then <action>

        Examples:
            | role          | action                                                                                           |
            | Owner         | [database create] database 2 things under 1234@gmail.com account: Team: "Nancy's team", Role:___ |
            | Administrator | [database create] database 2 things under 1234@gmail.com account: Team: "Nancy's team", Role:___ |
            | Develpoer     | output: "permission Error"                                                                       |

    Scenario: Add new member but reach the Team's collaborators limit
        Given the user is <role>
        When the user enters "dong team add"
        And: given an account email
        And: given a role
        Then <action>

        Examples:
            | role          | action                                                                    |
            | Owner         | output: "reach the collaborators limit" + "Please upgrade to add members" |
            | Administrator | output: "reach the collaborators limit" + "Please upgrade to add members" |
            | Develpoer     | output: "permission Error"                                                |

    Scenario: Doesn't asign role when adding new member into a Team
        Given the user is <role>
        When the user enters "dong team add"
        And: given an account email
        And: doesn't given a role
        Then output: "Please asigned the role"

        Examples:
            | role          | action                    |
            | Owner         | "Please asigned the role" |
            | Administrator | "Please asigned the role" |

    Scenario: Change Team member's role
        Given the user is <role>
        When the user enters "dong team role"
        And: given an account
        And: given a new role
        Then <action>

        Examples:
            | role          | action                                       |
            | Owner         | [database update] new role under the account |
            | Administrator | [database update] new role under the account |
            | Develpoer     | output: "permission Error"                   |

    Scenario: Change "Owner" role without asigned another user as owner
        # substantially transfer the Team's ownership
        Given the user is the only Owner in a Team
        When the user enters "dong team role"
        And: given an NON-owner role to the user-self
        Then fail + output: "Please asign another user as Team Owner"

    Scenario: Remove existed member from a Team

        Given the user is <role>
        When the user enters "dong team remove"
        And: given an member account is 1234@gmail.com
        Then <action>

        Examples:
            | role          | action                                                                                                         |
            | Owner         | output: "confirm Y/N", And: [database delete] under 1234@gmail.com account: Team: ~~"Nancy's team"~~, Role:___ |
            | Administrator | output: "confirm Y/N", And: [database delete] under 1234@gmail.com account: Team: ~~"Nancy's team"~~, Role:___ |
            | Develpoer     | output: "permission Error"                                                                                     |

    Scenario: Remove existed member from a Team with wrong emil
        Given the user is <role>: Onwer or Administrator
        And: the being remove member's email is 1234@gmail.com
        When the user enters "dong team remove"
        And: given an member account 1423@gmail.com
        Then output: "wrong member account email"

    Scenario: Leave the Team
        Given the user is <role>
        And: the user's account email is 1234@gmail.com
        When the user enters "dong team leave"
        Then output: "confirm Y/N"
        And: [database delete] under 1234@gmail.com account: Team: ~~"Nancy's team"~~, Role:___

        Examples:
            | role  | action                                                                                                         |
            | Owner | output: "confirm Y/N", And: [database delete] under 1234@gmail.com account: Team: ~~"Nancy's team"~~, Role:___ |

    Scenario: Last member (Owner) leave the Team
        # substantially delete the Team
        Given the user is the last member, which is the last and only Owner in a Team
        When the user enters "dong team leave"
        Then output: "This will delete the Team from dong (including all unfinished training job, private model, private data...). Please confirm Y/N"
        And: delete the Team

    Scenario: delete the Team
        Given the user is <role>
        When the user enters "dong team delete"
        Then <action>

        Examples:
            | role          | action                                                                                                                                                          |
            | Owner         | output: "This will delete the Team from dong (including all unfinished training job, private model, private data...). Please confirm Y/N", And: delete the Team |
            | Administrator | output: "This will delete the Team from dong (including all unfinished training job, private model, private data...). Please confirm Y/N", And: delete the Team |
            | Develpoer     | output: "permission Error"                                                                                                                                      |