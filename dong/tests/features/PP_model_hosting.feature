Feature: Public/Private model hosting
    dong provides authority management for the model, 
    provides log for the user to judge the model's quality and also provides
    model usage documentation.

    @model_default
    Scenario: A free plan user trains a model, system default model property
        Given a user uses the free plan
        When the user trains a model
        Then the model property is set to public

    @model_default
    Scenario: A paid plan user trains a model, system default model property
        Given a user using one of the paid plans
        When the user trains a model
        And the amount of models has not reached the limit of the plan
        Then the model property is set to private

    @model_default
    Scenario: A paid plan user who is out of private model's quota trains a model
        Given a user using one of the paid plans
        When the user trains a model
        And the amount of models has reached the limit of the plan
        Then CLI asks if the user wants to train a public model

    @model_default
    Scenario: A paid plan user is asked to train a public model or not and he/she accepts
        Given a user is asked to train a public model or not
        When the user accept to train a public model
        Then the model property is set to public
        And the training starts

    @model_default
    Scenario: A paid plan user is asked to train a public model or not and declines
        Given a user is asked to train a public model or not
        When the user declines to train a public model
        Then the training will not start
        And the model will not be generated   


    @model_management
    Scenario: A paid plan user turn a trained model into public
        # CLI: dong component --public -j <job-name>
        # API: 
        # method: POST
        # passing [job-name, user-permission]
        # Client send request to server from local with parameters
        # Server check user permission and check the job-name exist 
        # Then turn into model type to public at server
        # Server response result
        Given a user using one of the paid plans
        And the user is the plan owner
        And the model property is private
        When the user enters "dong model --public <job-name>"
        Then the model property is set to public
    
    @model_management
    Scenario: A paid plan user turn a trained model into private
        #CLI: dong component --private <job-name>
        Given a user using one of the paid plans
        And the user is plan owner
        And the model property is set to public
        And the private models amount has not reach the limit
        When the user enters "dong model --private <job-name>"
        Then the model property is set to private

    # The different user priviledge should be seperated into more roles in the future
    @model_management
    Scenario: A user deletes a model
        Given a user is plan owner
        And the user has trained a model
        When the user enters to delete the model
        And confirms by typing "YES"
        Then the model is deleted
    
    @model_management
    Scenario: A not-plan-owner user tries to delete a model
        Given a user is not the plan owner
        And the user has trained a model
        When the user enters to delete the model
        Then outputs to indicate the user has no permission to do that

    @inquire_models
    Scenario: A user compares models and choose appropriate to set to public
        # CLI: dong component ls --model 
        # API:
        # method: GET 
        # passing [user-permission]
        # Client send request to server from local with parameters
        # Server check user permission
        # Server response result: public/private model(s) that the user can access 
        Given a user has a model trained
        When the user inquire the model
        Then output logs

    