Feature: Endpoint

    prmission management across deployment and service execution.

    Rule: Matrix of behavior and excution result for each role: https://docs.google.com/spreadsheets/d/14bcV8RbrLXYd8eKWi2BZhJD012aRaDWgKzZo6V10U3Q/edit?usp=sharing
    dong shall executes the <result> and <output> according to the Matrix

    Background:
        Given the user has already sign in
        And be a member of a team (A team represent that it has attached a paid plan)

    Scenario: Deploy the model with permission, dong sahll lanuch a serving instance
        Given the user is <role> whom the the <result> is Success
        When the user enters "dong endpoint up"
        Then dong lanuch a serving instance

        Examples:
            | role | result  | dong's action             |
            | FAE  | Success | lanuch a serving instance |

    Scenario: Deploy the model without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters "dong endpoint up"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role            | result        | dong's action      | dong output        |
            | Billing Manager | No Permission | reject the request | "permission error" |

    Scenario: Stop the endpoint (i.e.terminate the service) with permission, dong sahll terminate the serving instance
        Given the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command: "dong endpoint down"
        Then dong terminate the serving instance

        Examples:
            | role | result  | dong's action                  |
            | FAE  | Success | terminate the serving instance |

    Scenario: Stop the endpoint without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong endpoint down"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role            | result        | dong's action      | dong output        |
            | Billing Manager | No Permission | reject the request | "permission error" |

    Scenario: Lock the existing endpoint with permission, dong sahll prohibit the service from being roll back or delete
        Given the user is <role> whom the the <result> is Success
        When the user enters CLI command
        # Pre-design CLI command:"dong endpoint lock NAME"
        Then dong prohibit the service from being roll back or delete

        Examples:
            | role       | result  | dong's action                                       |
            | Administor | Success | prohibit the service from being roll back or delete |

    Scenario: Lock the existing endpoint without permission, dong sahll reject and output permission error
        Given the user is <role> whom the <result> is No Permission
        When the user enters CLI command
        # Pre-design CLI command: "dong endpoint lock NAME"
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role | result        | dong's action      | dong output        |
            | FAE  | No Permission | reject the request | "permission error" |

    Scenario: For the *Access* to the deployment KM records & logs, if a user with lower access stratum want to execute higher access behavior, dong sahll deny and output permission error
        Given the user is <role> with the access stratum <result>
        When the user execute a higher access stratum <behavior>
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role      | access stratum result | behavior        | dong's action | dong output        |
            | FAE       | comment               | delete a record | deny access   | "permission error" |
            | Develpoer | comment               | delete a record | deny access   | "permission error" |
            | BM        | Read                  | leave a comment | deny access   | "permission error" |

    Scenario: For the *Access* to service's name, description, if a user with lower access stratum want to execute higher access behavior, dong sahll deny and output permission error
        Given the user is <role> with the access stratum <result>
        When the user execute a higher access stratum <behavior>
        Then dong reject the request
        And <output> "permission error"

        Examples:
            | role | access stratum result | behavior          | dong's action | dong output        |
            | BM   | read                  | edit a model name | deny access   | "permission error" |
