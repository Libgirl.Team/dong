Feature: Deployment for model

    All models on the GUI interface should be deployable and can be automatic

    Background: A user has loggined correctly
        Given a user has the permission for specific models
        And the user enters the GUI interface
        And the user logins successfully

    Scenario: A user tries to deploy with no permission will fail
        Given a user with no permission to deploy the model
        When the user click the button 'deploy' of the model
        Then the GUI will show error message to indicate that the user has no permission to deploy this model

    Scenario: A user tries to deploy with correct permission
        Given a user with correct permission to deploy the model
        When the user click the button 'deploy' of the model
        Then the GUI will show interface for parameter settings of the deployment
        And the last deployment settings will be loaded as default

    Scenario: A user tries to undeploy with no permission will fail
        Given a user with no permission to undeploy the model
        And the user select an endpoint's action
        When the user choose to undeploy the model
        Then the GUI will show error message to indicate that the user has no permission to undeploy the model

    Scenario: A user tries to undeploy with correct permission
        Given a user with correct permission to undeploy the model
        And the user select an service endpoint's action 
        When the user choose to undeploy the model
        Then the model of the service endpoint would be undeployed
        And the GUI will show the the service point name with the model name/version's information

    Scenario: The deployment history should be recorded every time a model is deployed
        Given a user with correct permission to deploy the model
        When the user deploys a the model successfully
        Then the parameter settings of the deployment will be append to the deployment history of the model
