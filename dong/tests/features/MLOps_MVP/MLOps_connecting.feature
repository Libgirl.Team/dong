Feature: Connecting with knowledge management

    All training models' info should connects with its training information including data logs and evaluation results

    Background: The knowledge management info is the same as the dong training's knowledge management info
        Given all the models has knowledge management info in the format as the dong training's knowledge management info is

    Scenario: A user cannot see anything if he or she hasn't loggin
        Given a user enters the GUI interface
        When the user doesn't login successfully
        Then the user should not see any info of any models

    Scenario: A user can see the models' list he or she can access
        Given a user has the permission for specific models
        When the user enters the GUI interface
        And the user logins successfully
        Then the user should see the models' list under the permission of the models the user can access

    Scenario: A user should see the models' evaluation result list he or she can access
        Given a user has the permission for specific models
        When the user enters the GUI interface
        And the user logins successfully
        Then the user should see each model's evaluation results in the list

    Scenario: A user can see models' knowledge management info if he or she has the permission
        Given a user has enters the GUI interface
        And the user has logined
        And the user has the permission for specific models
        When the user click a model on the list
        Then the model detail info should be shown
        And the model's lastest knowledge management info should be shown

    Scenario: A user can download models' detail knowledge management info if he or she has the permission
        Given a user has enters the GUI interface
        And the user has logined
        And the user has the permission for specific models
        And the user has open a model detail info
        When the user click 'download log' button on the interface
        Then the model detail info along with the knowledge management info should be packed and downloaded as a file
