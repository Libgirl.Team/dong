Feature: Delete model(s) or service endpoints

    The delete feature of both model and service endpoint should be provided for managing models.

    Background: A user has loggined correctly
        Given a user has the permission for specific models
        And the user enters the GUI interface
        And the user logins successfully

    Scenario: A user cannot delete models with no permission
        Given a user has no permission to delete models
        And the user select some models
        When the user choose the delete action
        Then the GUI would indicate the user that he or she has no permission to do that

    Scenario: A user cannot delete endpoints with no permission
        Given a user has no permission to delete endpoints
        And the user select some endpoints
        When the user choose the delete action
        Then the GUI would indicate the user that he or she has no permission to do that

    Scenario: A user cannot delete pinned models
        Given a user has correct permission to delete models
        And the user selects one or some pinned models or models using by some locked endpoints
        When the user choose the delete action
        Then the GUI would indicate the user that the model cannot be deleted before unpin the model(s)/unlock the endpoint(s)
        
    Scenario: A user cannot delete locked endpoints
        Given a user has correct permission to delete endpoints
        And the user selects one or some locked endpoints
        When the user choose the delete action
        Then the GUI would indicate the user that the endpoint cannot be deleted
    
    Scenario: A user deletes models with correct permission
        Given a user has correct permission to delete models
        And the user selects one or some models that are neither pinned nor using by any locked endpoints
        And the user choose the delete action
        When the user confirm to delete
        Then the models would be deleted
        And the GUI would indicates the naming/version of models that are deleted

    Scenario: A user deletes service endpoints with correct permiission
        Given a user has correct permisssion to delete service endpoints
        And the user selects one or some endpoints that are not locked
        And the user choose the delete action
        When the user confirm to delete
        Then the service endpoint would be undeployed
        And the service endpoint would be deleted
        And the GUI would indicates the naming/version of service endpoint that are deleted
        