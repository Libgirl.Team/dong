Feature: Upload model settings/model

   The evaluation result and the model itself might be hosted at the user's own database or cloud rather than dong's cloud.
   Providing the GUI access to do the uploading is needed since then.

   For connecting to model management system,
   the ability for uploading model and/or model settings to let dong cloud host/manage is necessary.
   For this feature, "eval file" indicates the evaluation report for the model that the user wants to upload.

   Background: A user has loggined correctly
      Given a user has the permission for specific models
      And the user enters the GUI interface
      And the user logins successfully

   # The button name and position should be designed
   Scenario: A user without correct permission couldn't upload a model with an eval file
      Given a loggin user
      When the user has no correct permission for uploading model with settings
      Then the upload button is disabled

   Scenario: A user with correct permission uploads model with a eval file
      Given a user has correct permission for uploading model with a eval file
      And the user click the button for uploaing
      When the user chooses to upload model file with the eval file
      Then the GUI will upload the model to the dong cloud
      And the GUI will upload the model-eval-file
      And the dong cloud will parse the model-eval-file
      And the evaluation results of the model will be attached to the model's information

   Scenario: A user without correct permission couldn't upload remote model settings with an eval file
      Given a user has no correct permission for uploading model with settings
      And the user click the button for uploaing
      When the user chooses to upload remote model setting file with the eval file
      Then the GUI will output message "Uploading the remote model settings with an eval file is not allowed under your permission."

   Scenario: A user with correct permission uploads remote model settings with an eval file
      Given a user has correct permission for uploading model settings with an eval file
      And the user click the button for uploaing
      When the user chooses to upload remote model setting file with the eval file
      Then the GUI will upload the model-setting-file to the dong cloud
      And the GUI will upload the model-eval-file
      And the dong cloud will parse the model-setting-file
      And the dong cloud will parse the model-eval-file
      And the evaluation results of the model will be attached to the model's information
