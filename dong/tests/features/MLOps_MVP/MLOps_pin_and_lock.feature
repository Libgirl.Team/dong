Feature: Pin model and lock service endpoint

    Models would be considered as the same category if their structure, input, output format are the same.
    Since then, in a series of models, pin model means to make a model in a category significant and cannot be deleted.

    The action of lock service endpoint is to make a deployed model at an endpoint fixed and cannot be changed.
    That means, you can neither deploy any model to this endpoint nor delete the model.

    The unpin/unlock actions are the actions of the cancelling the things pin/lock do.


    Background: A user has loggined correctly
        Given a user has the permission for specific models
        And the user enters the GUI interface
        And the user logins successfully

    Scenario: A user cannot pin if he or she has no permission
        Given a user has no permission to pin a model
        When the user choose to pin the model
        Then the GUI would indicate the user that he or she has no permission to do that

    Scenario: A user cannot lock if he or she has no permission
        Given a user has no permission to lock a service endpoint
        When the user choose to lock the service endpoint
        Then the GUI would indicate the user that he or she has no permission to do that

    Scenario: A user pins a model with correct permission
        Given a user has correct permission to pin a model
        When the user choose to pin the model
        Then the GUI would pin the model
        And the pinned model would be significant at the list of the versions of this model
        And the pinned model would be read only (cannot be deleted)

    Scenario: A user locks an endpoint with correct permission
        Given a user has correct permission to lock an endpoint
        When the user choose to lock the endpoint
        Then the GUI would lock the endpoint
        And the endpoint cannot be deployed
        And the model of the endpoint would be read only

    Scenario: A user unpins a model with correct permisssion
        Given a user has correct permission to unpin a model
        When the user choose to unpin the model
        Then the GUI would unpin the model
        And the pinned model would be no longer significant at the list of the versions of this model
        And the pinned model would be no longer read only

    Scenario: A user unlocks an endpoint with correct permission
        Given a user has correct permission to unlock an endpoint
        When the user choose to unlock the endpoint
        Then the GUI would unlock the endpoint
        And the endpoint would be deployable
        And  the model of the endpoint would be no longer read only