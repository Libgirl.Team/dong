Feature: Package project to cloud for training

    Scenario: Execution project success
        Given the project is follow the template
        And a user has <project-path>
            | project-path              |
            | /Users/David/dong_project |
        When the user enters:
            "dong train exec --package-path <project-path>"
        Then dong check whether the project fit to the template
        And dong package project
        And uploads to dong cloud for training
        And output: <message>

    Scenario: Execution ASAP
        Given a user in the current project directory
        And Execution training project without command "--package-path"
        When the user enters "dong train exec"
        Then dong checks whether the project follow the template
        And dong find correct module
        And dong package project
        And upload to cloud for training

    Scenario: The project is "not" follow the template
        And a user has and <project-path>
            | project-path              |
            | /Users/David/dong_project |
        When the user enters:
            "dong train exec --package-path <project-path>"
        Then dong checks whether the project fit to the template
        And output: <message> please read dong-document: [train]
    
    Scenario: dong can't find any project to train
        Given a user is not in project file 
        And project flie has no "step.py"
        When the user enters "dong train exec"
        Then dong checks if there is "step.py" or not
        And output: <message> please read dong-document: [train]


# let's do after alpha
# Feature: 本地專案進行local訓練
#     進行本地端訓練
#     Scenario: 使用者專案 符合 template
#         Given 使用者的專案內容 符合 系統規範
#         And 使用者已經登入系統
#         When 使用者 輸入
#             "dong train local exec --package-path <project-path>"
#         Then 系統檢查專案是否符合tempalte
#         And 進行本地訓練
#     Scenario: 使用者專案 不符合 template
#         Given 使用者的專案內容 不符合 系統規範
#         And 使用者已經登入系統
#         When 使用者 輸入
#             "dong train local exec --package-path <project-path>"
#         Then dong 檢查專案
#         And 顯示錯誤提示 "專案不符合規範 請閱讀 dong generate 文件"






