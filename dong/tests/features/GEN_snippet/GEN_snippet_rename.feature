Feature: rename a local repo <repo_name> to <new_name>

  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name_1" exists
  
  Scenario: successfully rename the repo.
    Given there's no local repo named "repo_name_2"
    And there are no file io problems
    When a user enters "dong snippet rename repo_name_1 repo_name_2"
    Then the local repo "repo_name_1" is renamed as "repo_name_2"
        
  Scenario: another repo with identical name exists, stop.
    Given there's a local repo named "repo_name_2"
    And there are no file io problems
    When a user enters "dong snippet rename repo_name_1 repo_name_2"
    Then the user should see "Snippet repo repo_name_2 already exists."
    And the name local repo "repo_name_1" is not changed
    
  Scenario: rename stop due to file io error
    Given there's no local repo named "repo_name_2"
    And there are file io problems
    When a user enters "dong snippet rename repo_name_1 repo_name_2"
    Then the user should see the error messages
