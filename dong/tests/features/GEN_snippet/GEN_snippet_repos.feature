                
Feature: list all the repos

  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name_1" exists
    And a cloud repo of url "https://github.com/my_id/snippets" is the remote "origin" of the local repo "repo_name_1"
    And a local repo "repo_name_2" exists
    And the local repo "repo_name_2" has no remote

  Scenario Outline: successfully list the repos.
    Given there's no git or file io problem
    When a user enters "dong snippet repos"
    Then the user should see
    """
    Dong snippet repositories available:
    Repo_name url
    -------------------------------------------------
    repo_name_1     https://github.com/my_id/snippets
    repo_name_2     N/A
    """
    
  Scenario: fail to list thep repos due to system problems.
    Given there are git or file io problems
    When a user enters "dong snippet repos"
    Then the user should see error messages
