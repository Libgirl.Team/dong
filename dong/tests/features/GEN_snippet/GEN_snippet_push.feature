Feature: Push the changes of a local repo <repo_name> to the connected cloud repo

  There may be changes to either the user's local repo or the cloud repo.
  The user can add the local changes to the cloud by this feature.
  The effect is actually "git pull --rebase" and then "git push origin"
  A user may not have the authority to push to the "master" branch of a cloud repo,
  so this feature allows a user to push to branches other then the default "master".
  All the processes of git login will be handled by the git API.

  Background: 
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists
    And a cloud repo of url "https://github.com/my_id/snippets" is the remote "origin" of the local repo
    And a file "new.py" is submitted to the local of the local repo
    And the local repo is not merged to the cloud repo


  Scenario Outline: Push successful.
    Given there's no git or file io problems
    When a user enters "dong snippet push repo_name <branch>"
    Then the user should see "Push changes in repo_name to branch <result_branch> of https://github.com/my_id/snippets successfully."
    And the file "new.py" is accessible under the branch "<result_branch>" of the cloud repo

    Examples:
    | branch | result_branch |
    |        | master        |
    | master | master        |
    | new    | new           |

  Scenario: Push fail due to git problems.
    Given there are git or file io problems
    When a user enters "dong snippet push repo_name"
    Then the user should see the error messages      
