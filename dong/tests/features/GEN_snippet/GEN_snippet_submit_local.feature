Feature: Submit a snippet to only the local.

  This feature copy a file into the local repo and "git commit".
  
  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists

  Scenario Outline: No file with idential target name exists and submit succesfull

    The user may specify to override by --override, but in this case it make no difference.
    default target is the local.

    Given a file of "init/cnn.py" exists in the working diretory
    And no file of <result_path> exists in the branch "master" of the local repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py <destination> <submit_target> <is_override>"
    Then the file is copied as <result_path> into the branch "master" of the local repo

    Examples:
    | destination             | submit_target    | is_override | result_path             |
    | model/init/.            |                | --override  | model/init/cnn.py       |
    | model/init/cnn_keras.py |                |             | model/init/cnn_keras.py |
    |                         |                | --override  | cnn.py                  |
    | model/init/.            | --target=local |             | model/init/cnn.py       |
    | model/init/cnn_keras.py | --target=local | --override  | model/init/cnn_keras.py |
    |                         | --target=local |             | cnn.py                  |
    
  Scenario: No file with idential target name exists and submit fail.

    There may be to git or file io problems.

    Given no file of <result_path> exists in the branch "master" of the local repo
    And there are git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py model/init/."
    Then the user should see the error messages
    
  Scenario: A file with idential target name exists, the user specify to override and submit succesfull.

    Given a file of "init/cnn.py" exists in the working diretory
    And a file of <result_path> exists in the branch "master" of the local repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py <destination> <submit_target> --override"
    Then "init/cnn.py" is copied as <result_path> into the branch "master" of the local repo

    Examples:
    | destination             | submit_target  | result_path             |
    | model/init/.            |                | model/init/cnn.py       |
    | model/init/cnn_keras.py |                | model/init/cnn_keras.py |
    |                         |                | cnn.py                  |
    | model/init/.            | --target=local | model/init/cnn.py       |
    | model/init/cnn_keras.py | --target=local | model/init/cnn_keras.py |
    |                         | --target=local | cnn.py                  |
    
  Scenario: a file with idential target name exists, the user specify to override and submit fail due to file io problems.

    Given a file of <result_path> exists in the branch "master" of the local repo
    And there are git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py model/init/cnn_keras.py --target=local --override"
    Then the user should see the error messages
    
  Scenario: a file with idential target name exists, the user choose to override at prompt and submit succesfull.

    Given a pfile of "init/cnn.py" exists in the working diretory
    And a file of <result_path> exists in the branch "master" of the local repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py <destination> <submit_target>"
    Then the user should see "<result_path> already exists in the local. Do you want to override? [Y/N]"
    And the user enters "Y"
    And "init/cnn.py" is copied as <result_path> into the branch "master" of the local repo

    Examples:
    | destination             | submit_target  | result_path             |
    | model/init/.            |                | model/init/cnn.py       |
    | model/init/cnn_keras.py |                | model/init/cnn_keras.py |
    |                         |                | cnn.py                  |
    | model/init/.            | --target=local | model/init/cnn.py       |
    | model/init/cnn_keras.py | --target=local | model/init/cnn_keras.py |
    |                         | --target=local | cnn.py                  |

  Scenario: a file with idential target name exists, the user choose to override at prompt and submit fail due to file io problems.

    Given a file of <result_path> exists in the branch "master" of the local repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py model/init/."
    Then the user should see "model/init/cnn.py already exists in the local. Do you want to override? [Y/N]"
    And the user enters "Y"
    And the user should see the error messages
    
  Scenario: a file with idential target name exists, the user decide to cancel at prompt.

    Given a file of <result_path> exists in the branch "master" of the local repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py"
    Then the user should see "model/init/cnn.py already exists in the local. Do you want to override? [Y/N]"
    And the user enters "N"
    And Nothing
