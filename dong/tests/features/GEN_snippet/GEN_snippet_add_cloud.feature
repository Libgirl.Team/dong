Feature: Make a new local repo with connection to a cloud repo

  This feature "git clone" the cloud repo and name the local repo as specified by the user.
  
  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory at snippet_dir_path
    And a cloud repo of <remote_url> exists

  Scenario Outline: Create new repo repo_name with remote of <remote_info> successfully
    Given no folder "repo_name" exists under "$DONG_SNIPPET_DIR"
    And there are no problems in the settings of file io or git local operation
    When a user enters "dong snippet add repo_name <remote_info>"
    Then the user should see "New snippet repo repo_name is added with connection to <remote_url>."
    And a local repo "repo_name" is created
    And the user can access the local repo
    And the user can access the cloud repo
    And the data in the cloud repo will be clone to the local repo
    
    Examples:
    | remote_info                       | remote_url                        |
    | https://github.com/my_id/snippets | https://github.com/my_id/snippets |
    | my_id/snippets                    | https://github.com/my_id/snippets | 
    | my_id/snippets --host=github      | https://github.com/my_id/snippets |
    | https://gitlab.com/my_id/snippets | https://gitlab.com/my_id/snippets |
    | my_id/snippets --host=gitlab      | https://gitlab.com/my_id/snippets | 

  Scenario Outline: Create stop because a local repo with identical name already exists
    Given a folder "repo_name" exists under "$DONG_SNIPPET_DIR"
    And there are no problems in the settings of file io or git local operation
    When a user enters "dong snippet add repo_name <remote_info>"
    Then the user should see "The snippet repo repo_name already exists under snippet_dir_path"

    Examples:
    | remote_info                       | remote_url                        |
    | https://github.com/my_id/snippets | https://github.com/my_id/snippets |
    | my_id/snippets                    | https://github.com/my_id/snippets |
    | my_id/snippets --host=github      | https://github.com/my_id/snippets |
    | https://gitlab.com/my_id/snippets | https://gitlab.com/my_id/snippets |
    | my_id/snippets --host=gitlab      | https://gitlab.com/my_id/snippets |

  Scenario: Create new repo repo_name with remote of <remote_info> fail due to git or file io problems.
    Given no folder "repo_name" exists under "$DONG_SNIPPET_DIR"
    And there are problems in the settings of file io or git operation
    When a user enters "dong snippet add repo_name https://github.com/my_id/snippets"
    Then the user should see the error messages
