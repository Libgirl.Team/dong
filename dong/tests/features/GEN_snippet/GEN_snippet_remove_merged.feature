Feature: Remove a repo which is connected, and local & cloud are merged.

  This feature delete the local repo.
  
  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists under "$DONG_SNIPPET_DIR"
    And a cloud repo of url "https://github.com/my_id/snippets" is the remote "origin" of the local repo
    And in the local repo, the branch "master" was already merged to "origin/master"
    
  Scenario: A user remove the local repo successfully
    Given there are no git or file io problems
    When a user enters "dong snippet remove repo_name"
    Then the user should see "Snippet repo repo_name successfully removed."
    And the local repo "repo_name" is removed
    And
    """
    The local snippet repo repo_name is removed.
    To use the repo again, you can run the following command:
    dong snippet add repo_name https://github.com/my_id/snippets
    """

  Scenario: A user failed to remove the local repo due to git or file io problems
    Given there are git or file io problems
    When a user enters "dong snippet remove repo_name"
    Then the user should see the error messages
