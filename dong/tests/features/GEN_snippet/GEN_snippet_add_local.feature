Feature: Make a new local repo without connection to cloud repo

  This feature "git init" a local repo repo_name for storing & loading snippets locally.
  
  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    
  Scenario: Create new local repo <repo_name> success
    Given no folder "repo_name" exists under "$DONG_SNIPPET_DIR"
    And there are no problems in the settings of file io or git local operation.
    When a user enters "dong snippet add repo_name"
    Then the user should see "New snippet repo repo_name is added without connecting to a remote."
    And a local repo "repo_name" is created
    And the user can access the local repo
    
  Scenario: Creation stop because a local repo with identical name already exists
    Given a folder "repo_name" exists under "$DONG_SNIPPET_DIR"
    And there are no problems in the settings of file io and git local operation.
    When a user enters "dong snippet add repo_name"
    Then the user should see "The sniipet repo repo_name already exists under $HOME/dong_snippet_dir/repo_name"

  Scenario: Createion fail due to system problems
    Given no folder "repo_name" exists under "$DONG_SNIPPET_DIR"
    And there are problems in the settings of file io or git local operation.
    When a user enters "dong snippet add repo_name"
    Then the user should see the error messages
