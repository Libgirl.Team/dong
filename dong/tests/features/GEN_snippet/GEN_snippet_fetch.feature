Feature: Let a local repo fetch the connected cloud repo

  This feature "git fetch" for a local repo which has a remote "origin" to a cloud repo.
  
  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists under "$DONG_SNIPPET_DIR"
    And a cloud repo of url "https://github.com/my_id/snippets" is the remote "origin" of the local repo
    
  Scenario: fetch successful
    Given there are no git or file io problems
    When a user enters "dong snippet fetch repo_name"
    Then the user should see "successfully fetch snippet repo repo_name from https://github.com/my_id/snippets"
    And the branch "origin/master" of the local repo is updated to the newest version of the cloud repo
        
  Scenario: fetch fail due to git or file io problems
    Given there are no git or file io problems
    When a user enters "dong snippet fetch repo_name"
    Then the user should see the error messages
