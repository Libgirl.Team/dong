Feature: list all the snippets in a repo as tree.

  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists
    And a cloud repo of url "https://github.com/my_id/snippets" is the remote "origin" of the local repo
    And each one of the branches has different tree from others: "master" and "master/origin" of the local repo, "master" of the cloud repo.
  
  Scenario Outline: tree the local of a repo successful
    Given there's no git or file io problem.
    When a user enters "dong snippet tree repo_name <src_version>"
    Then the user should see the tree of branch "master" of the local repo.

    Examples:
    | src_version |
    |             |
    | --src=local |

  Scenario: tree the origin of a repo successful

    Show the tree of the branch "origin/master" of the local repo.
    This feature let the user get the last fetched version of the repo.
    
    Given there's no git or file io problem.
    When a user enters "dong snippet tree repo_name --src=origin"
    Then the user should see the tree of branch "origin/master" of the local repo.

  Scenario: tree the cloud version of a repo successful

    Show the tree of the branch "master" of the cloud repo.
    This feature let the user get the last fetched version of the repo.
    Dong will fetch first then tree the branch "origin/master".
    
    Given there's no git or file io problem.
    When a user enters "dong snippet tree repo_name --src=fetch"
    Then the user should see the tree of branch "master" of the cloud repo.

  Scenario: tree a single source fail
    Given there are git or file io problems.
    When a user enters "dong snippet tree repo_name <src_version>"
    Then the user should see the error messages

    Examples:
    | src_version  |
    |              |
    | --src=local  |
    | --src=origin |
    | --src=fetch  |



    
