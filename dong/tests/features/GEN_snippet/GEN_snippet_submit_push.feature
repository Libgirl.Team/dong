Feature: Submit a snippet to both the cloud and the local.

  This feature copy a file into the local repo, "git commit", "git pull --rebase" and then "git push".
  
  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists
    And a cloud repo of url "https://github.com/my_id/snippets" is the remote "origin" of the local repo

  Scenario Outline: No file with idential target name exists and submit succesfull

    The user may specify to override by --override, but in this case it make no difference.
    The default branch to push to is "master".

    Given a file of init/cnn.py exists in the working diretory
    And no file of <result_path> exists in the branch <branch> of the cloud repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py <destination> --target=push <submit_branch> <is_override>"
    Then the file is copied as <result_path> into the branch <branch> of the cloud repo

    Examples:
    | destination             | is_override | result_path             | submit_branch   | branch |
    | model/init/.            |             | model/init/cnn.py       |                 | master |
    | model/init/cnn_keras.py | --override  | model/init/cnn_keras.py | --branch=tuned  | tuned  |
    |                         |             | cnn.py                  | --branch=master | master |
    
  Scenario: No file with idential target name exists and submit fail.

    There may be to git or file io problems.

    Given no file of "model/init/cnn.py" exists in the branch <branch> of the cloud repo
    And there are git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py model/init/. --branch=master --target=push"
    Then the user should see the error messages
    
  Scenario: a file with idential target name exists, the user specify to override and submit succesfull.

    Given a file of init/cnn.py exists in the working diretory
    And a file of <result_path> exists in the branch <branch> of the cloud repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py <destination> --target=push <submit_branch> --override"
    Then init/cnn.py is copied as <result_path> into the branch <branch> of the cloud repo

    Examples:
    | destination             | result_path             | submit_branch   | branch |
    | model/init/.            | model/init/cnn.py       |                 | master |
    | model/init/cnn_keras.py | model/init/cnn_keras.py | --branch=tuned  | tuned  |
    |                         | cnn.py                  | --branch=master | master |
    
  Scenario: a file with idential target name exists, the user specify to override and submit fail due to file io problems.

    Given a file of "model/init/cnn.py" exists in the branch <branch> of the cloud repo
    And there are git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py model/init/. --target=push --branch=tuned  --override"
    Then the user should see the error messages
    
  Scenario: a file with idential target name exists, the user choose to override at prompt and submit succesfull.

    Given a file of init/cnn.py exists in the working diretory
    And a file of <result_path> exists in the branch <branch> of the cloud repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py <destination> --target=push <submit_branch>"
    Then the user should see "<result_path> already exists in the branch <branch> of https://github.com/my_id/snippets. Do you want to override? [Y/N]"
    And the user enters "Y"
    And "init/cnn.py" is copied as <result_path> into the branch <branch> of the cloud repo

    Examples:
    | destination             | result_path             | submit_branch   | branch |
    | model/init/.            | model/init/cnn.py       |                 | master |
    | model/init/cnn_keras.py | model/init/cnn_keras.py | --branch=tuned  | tuned  |
    |                         | cnn.py                  | --branch=master | master |

  Scenario: A file with idential target name exists, the user choose to override at prompt and submit fail due to file io problems.

    Given a file of "model/init/cnn.py" exists in the branch <branch> of the cloud repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py model/init/. --target=push --branch=master"
    Then the user should see "model/init/cnn.py already exists in the branch <branch> of https://github.com/my_id/snippets. Do you want to override? [Y/N]"
    And the user enters "Y"
    And the user should see the error messages
    
  Scenario: a file with idential target name exists, the user decide to cancel at prompt.

    Given a file of "model/init/cnn.py" exists in the branch <branch> of the cloud repo
    And there are no git or file io problems
    When a user enters "dong snippet submit repo_name init/cnn.py --target=push"
    Then the user should see "model/init/cnn.py already exists in the branch <branch> of https://github.com/my_id/snippets. Do you want to override? [Y/N]"
    And the user enters "N"
    And Nothing
