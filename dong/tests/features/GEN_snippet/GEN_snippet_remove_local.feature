Feature: Remove a local repo which is not connected to a cloud repo

  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And local repo "repo_name" exists
    And local repo "repo_name" has no remote

  Scenario: stop due to git or file io errors before prompting
    Given there are file io or git problems
    When a user input "dong snippet remove repo_name"
    Then the user should see the error messages

  Scenario: removing local repo fail due to file io problems after prompting
    Given there are file io problems
    And there are no git problems
    When a user input "dong snippet remove repo_name"
    Then the user should see "repo_name only exists at local and has no other copies anywhere else. Do you still want to remove it? [YES]"
    And the user enters "YES"
    And the user should see the error messages

  Scenario: user choose to disgard the local and remove the repo successfully
    Given there are no file io or git problems
    When a user input "dong snippet remove repo_name"
    Then the user should see "repo_name only exists at local and has no other copies anywhere else. Do you still want to remove it? [YES]"
    And the user enters "YES"
    And the local repo is removed
    
  Scenario: user choose not to disgard the local and stop
    Given there are no file io or git problems
    When a user input "dong snippet remove repo_name"
    Then the user should see "repo_name only exists at local and has no other copies anywhere else. Do you still want to remove it? [YES]"
    And the user enters anythong other than "YES"
    And nothing
