Feature: Load a code snippet from local

  A local repo may be connected to cloud repo or not.
  In case that a local repo is connected, there may be changes to either the user's local repo or the cloud repo.
  This feature let a user load the local version.

  Background: 
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists
    
  Scenario Outline: no existing file with identical target name and a user load successful.

    If a user doesn't enter <src_version>, the default is to load snippet from the local version.
    If a user doesn't enter <target_path>, the target snippet is copied to the working directory with identical name.
    The user may specify to override by --override, but in this case it make no difference.
    
    Given the file "model/init/cnn.py" exists in the local repo
    And no file of <result_path> exists
    And there are no git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py <target_path> <src_version> <is_override>"
    Then the file "model/init/cnn.py" in the local repo is copied as <result_path> under the working directory

    Examples:
    | target_path             | src_version | result_path             | is_override |
    | model/init/cnn_keras.py |             | model/init/cnn_keras.py |             |
    | model/init/cnn_keras.py | --src=local | model/init/cnn_keras.py | --override  |
    |                         |             | cnn.py                  |             |
    |                         | --src=local | cnn.py                  | --override  |
    | model/init/.            |             | model/init/cnn.py       |             |
    | model/init/.            | --src=local | model/init/cnn.py       | --override  |

  Scenario: No existing file with identical target name and a user fail to load.

    If a user doesn't enter <src_version>, the default is to load snippet from the local version.
    If a user doens't enter <target_path>, the target snippet is copied to the working directory with identical name.
    There may be git ot file io problems.
    The user may specify to override by --override, but in this case it make no difference.
    
    Given no file of "model/init/cnn_keras.py" exists
    And there are git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py model/init/."
    Then the user should see the error messages

  Scenario Outline: A file with identical target name exists, a user specifies to override and load successful.
    Given the file "model/init/cnn.py" exists in the branch "master" of the local repo
    And a file of <result_path> exists
    And there are no git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py <target_path> --src=local --override"
    Then the file "model/init/cnn.py" in the branch "master" of the local repo is copied as <result_path> under the working directory

    Examples:
    | target_path             | result_path             |
    | model/init/cnn_keras.py | model/init/cnn_keras.py |
    |                         | cnn.py                  |
    | model/init/.            | model/init/cnn.py       |

  Scenario: A file with identical target name exists, a user specifies to override and fail to load.
      
    There may be git ot file io problems.
      
    Given a file of "model/init/cnn.py" exists
    And there are git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py model/init/. --src=local --override"
    Then the user should see the error messages

  Scenario Outline: A file with identical target name exists, a user chooses to override at prompt and load successful.
    Given the file "model/init/cnn.py" exists in the branch "master" of the local repo
    And a file of <result_path> exists
    And there are no git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py <target_path> --src=local"
    And the user should see "<result_path> already exists. Do you want to override? [Y/N]"
    And the user enters "Y"
    Then the file "model/init/cnn.py" in the branch "master" of the local repo is copied as <result_path> under the working directory

    Examples:
    | target_path             | result_path             |
    | model/init/cnn_keras.py | model/init/cnn_keras.py |
    |                         | cnn.py                  |
    | model/init/.            | model/init/cnn.py       |

  Scenario: A file with identical target name exists, a user chooses to override at prompt and fail to load.
    Given a file of "model/init/cnn.py" exists
    And there are no git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py model/init/."
    And the user should see "model/init/cnn.py already exists. Do you want to override? [Y/N]"
    And the user enters "Y"
    Then the user should see the error messages

  Scenario: a file with identical target name exists, a user chooses to cancel at prompt
    Given the file "model/init/cnn.py" exists in the branch "master" of the local repo
    And a file of "model/init/cnn.py" exists
    And there are no git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py model/init/."
    And the user should see "model/init/cnn.py already exists. Do you want to override? [Y/N]"
    And the user enters "N"
    Then Nothing
