Feature: prompt when removing a repo with local & cloud unmerged.

  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists
    And a cloud repo of url "https://github.com/my_id/snippets" is the remote "origin" of the local repo
    And in the local repo, there are changes in branch "master" which are not yet merged to "origin/master"
    
  Scenario: user get prompt message
    Given there are no git or file io problems
    When the user enters "dong snippet remove repo_name"
    Then the user should see
    # detailed details of P C D in messages.
    # Disgard is serious!!!!!
    """
    changes in repo_name not yet merged to remote https://github.com/my_id/snippets.
    [P]ush: push the local changes then remove repo_name.
    [C]ancel: no actions.
    [D]isgard: remove repo_name directly. All the local chnages will be lost and there will be no backup!
    Pls enter [P/C/D] to choose the action.
    """

  Scenario: process stopped due to git or file io problems
    Given there are git or file io problems
    When the user enters "dong snippet remove repo_name"
    Then the user should see the error messages
