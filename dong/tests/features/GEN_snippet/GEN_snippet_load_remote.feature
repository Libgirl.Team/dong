Feature: Load a code snippet from cloud or origin

  There may be changes to either the user's local repo or the cloud repo.
  This feature guarantee to load the newest version from cloud repo or the one pulled previously from the cloud.
  By --src=fetch, the process is "git fetch" and then copy the model/init/cnn.py in the branch "master/origin" of the local repo.
  By --src=origin, just copy the model/init/cnn.py in the branch "master/origin" of the local repo.

  Background: 
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists
    And a cloud repo of url "https://github.com/my_id/snippets" is the remote "origin" of the local repo

  Scenario Outline: No existing file with identical target name and a user loads successful.

    Because no existing file with identical name exists, specifying is_override make no difference.
    
    Given the file "model/init/cnn.py" exists in the <src> repo
    And no file of <result_path> exists
    And there are no git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py <target_path> <load_src> <is_override>"
    Then the file "model/init/cnn.py" in the <src> repo is copied as <result_path> under the working directory
        
    Examples:
    | target_path             | result_path             | is_override | load_src     | src                             |
    | model/init/cnn_keras.py | model/init/cnn_keras.py |             | --src=fetch  | cloud                           |
    |                         | cnn.py                  | --override  | --src=fetch  | cloud                           |
    | model/init/.            | model/init/cnn.py       |             | --src=fetch  | cloud                           |
    | model/init/cnn_keras.py | model/init/cnn_keras.py | --override  | --src=origin | branch "origin/master" of local |
    |                         | cnn.py                  |             | --src=origin | branch "origin/master" of local |
    | model/init/.            | model/init/cnn.py       | --override  | --src=origin | branch "origin/master" of local |

  Scenario: No existing file with identical target name and a user fail to load.

    There may be git ot file io problems.

    Given no file of <result_path> exists
    And there are git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py model/init/cnn_keras.py --src=fetch"
    Then the user should see the error messages

  Scenario Outline: A file with identical target name exists, a user specifies to override and load successful.
    Given the file "model/init/cnn.py" exists in the <src> repo
    And a file of <result_path> exists
    And there are no git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py <target_path> <load_src> --override"
    Then the file "model/init/cnn.py" in the <src> repo is copied as <result_path> under the working directory

    Examples:
    | target_path             | result_path             | load_src     | src                             |
    | model/init/cnn_keras.py | model/init/cnn_keras.py | --src=fetch  | cloud                           |
    |                         | cnn.py                  | --src=fetch  | cloud                           |
    | model/init/.            | model/init/cnn.py       | --src=fetch  | cloud                           |
    | model/init/cnn_keras.py | model/init/cnn_keras.py | --src=origin | branch "origin/master" of local |
    |                         | cnn.py                  | --src=origin | branch "origin/master" of local |
    | model/init/.            | model/init/cnn.py       | --src=origin | branch "origin/master" of local |

  Scenario: A file with identical target name exists, a user specifies to override and fail to load.
      
    There may be git or file io problems.
      
    Given a file of "model/init/cnn.py" exists
    And there are git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py model/init/. --src=origin --override"
    Then the user should see the error messages

  Scenario Outline: A file with identical target name exists, a user chooses to override at prompt and load successful.
    Given the file "model/init/cnn.py" exists in the <src> repo
    And a file of <result_path> exists
    And there are no git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py <target_path> <load_src>"
    And the user should see "<result_path> already exists. Do you want to override? [Y/N]"
    And the user enters "Y"
    Then the file "model/init/cnn.py" in the <src> repo is copied as <result_path> under the working directory

    Examples:
    | target_path             | result_path             | load_src     | src                             |
    | model/init/cnn_keras.py | model/init/cnn_keras.py | --src=fetch  | cloud                           |
    |                         | cnn.py                  | --src=fetch  | cloud                           |
    | model/init/.            | model/init/cnn.py       | --src=fetch  | cloud                           |
    | model/init/cnn_keras.py | model/init/cnn_keras.py | --src=origin | branch "origin/master" of local |
    |                         | cnn.py                  | --src=origin | branch "origin/master" of local |
    | model/init/.            | model/init/cnn.py       | --src=origin | branch "origin/master" of local |

  Scenario: A file with identical target name exists, a user chooses to override at prompt and fail to load..
      
    There may be git or file io problems.
    
    Given a file of "model/init/cnn.py" exists
    And there are git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py model/init/. --src=origin"
    And the user should see "model/init/cnn.py already exists. Do you want to override? [Y/N]"
    And the user enters "Y"
    Then the user should see the error messages
    
  Scenario: A file with identical target name exists, a user chooses to cancel at prompt
    Given the file "model/init/cnn.py" exists in the cloud repo
    And a file of "model/init/cnn_keras.py" exists
    And there are no git or file io problems
    When a user enters "dong snippet load repo_name model/init/cnn.py model/init/cnn_keras.py --src=fetch"
    And the user should see "model/init/cnn_keras.py already exists. Do you want to override? [Y/N]"
    And the user enters "N"
    Then Nothing
