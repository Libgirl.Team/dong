Feature: perform the promted action when removing a repo with local & cloud unmerged.
  
  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" exists
    And a cloud repo of url "https://github.com/my_id/snippets" is the remote "origin" of the local repo
    And in the local repo, there are changes in branch "master" which are not yet merged to "origin/master"
    And a user get the prompt message after entering "dong snippet remove repo_name"
    
  Scenario: the user successfully push changes to the cloud repo and remove the local

    In this scenario, this feature runs "git pull origin --rebase" "git push" and then remove the local repo.
      
    Given there are no git or file io problems
    When the user enters "P"
    Then the changes in the local repo "repo_name" are pushed to remote "https://github.com/my_id/snippets"
    And the user should see "Snippet repo repo_name successfully pushed and removed."
    And the local repo "repo_name" is removed

  Scenario: user fail to push changes to cloud and remove local after getting prompt messages
    Given there are git or file io problems
    When the user enters "P"
    Then the user should see the error messages
    
  Scenario: user successfully remove local without push after getting prompt messages

    In this scenario, this feature just remove the local repo.
      
    Given there are no git or file io problems
    When the user enters "D"
    Then the local repo "repo_name" is removed
    And user should see "Snippet repo repo_name successfully removed without push."

  Scenario: user fail to remove local without push after getting prompt messages
    Given there are git or file io problems
    When the user enters "D"
    Then the user should see the error messages

  Scenario: user cancel actions after getting prompt messages
    When the user enters "C"
    Then nothing
