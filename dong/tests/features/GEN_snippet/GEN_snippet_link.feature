Feature: Set the remote of a built local repo to a cloud repo

  This feature "git remote add cloud_repo_url" for a local repo which has no remotes.
  
  Background:
    Given the env variable "DONG_SNIPPET_DIR" is defined as an existing directory
    And a local repo "repo_name" is already created
    And a cloud repo of <remote_url> exists
    
  Scenario Outline: A user set remote of <local_repo> successfully by <remote_info>
    Given there are no git setting or connection problems
    When a user enters "dong snippet link repo_name <remote_info>"
    Then the cloud repo is added as the remote "origin" of the local repo
    And the user can access the cloud repo

    Examples:
    | remote_info                       | remote_url                        |
    | https://github.com/my_id/snippets | https://github.com/my_id/snippets |
    | my_id/snippets                    | https://github.com/my_id/snippets |
    | my_id/snippets --host=github      | https://github.com/my_id/snippets |
    | https://gitlab.com/my_id/snippets | https://gitlab.com/my_id/snippets |
    | my_id/snippets --host=gitlab      | https://gitlab.com/my_id/snippets |

  Scenario: A user fail to set the remote of <local_repo> due to git or connection problems.
    Given there are git setting or connection problems
    When user enters "dong snippet link repo_name my_id/snippets --host=github"
    Then the user should see the erro messages
    And the user still cannot access the cloud repo
