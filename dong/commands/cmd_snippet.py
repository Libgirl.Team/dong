import os
import shutil
import pathlib
import click
import git
from dong import utils


@click.group(short_help='Submit or load snippets.')
def snippet():
    """
    Before using it, the env variable DONG_SNIPPET_DIR has to be defined as an existing directory.
    It's for storing the local snippet repos.
    """
    pass


@snippet.command()
@click.argument('remote-name', default='')
@click.argument('remote-url', default='')
@click.option('--snippets-path', default=os.environ.get('DONG_SNIPPET_DIR'), help='dir for the local snippet repos.')
def add(remote_name, remote_url, snippets_path):
    """add a remote of snippets."""

    remote_name = utils.prompt_empty_str(remote_name, 'Remote name')
    remote_url = utils.prompt_empty_str(remote_url, 'Remote URL')

    pathlib.Path(snippets_path).mkdir(parents=True, exist_ok=True)
    remote_path = os.path.join(snippets_path, remote_name)
    git.Repo.clone_from(remote_url, remote_path)


def get_remotes_list():
    return next(os.walk(os.environ.get('DONG_SNIPPET_DIR')))[1]


@snippet.command()
@click.argument('remote')
@click.argument('module')
@click.argument('src-snippet')
@click.argument('new-snippet', default='')
@click.option('--location', default=lambda: os.environ.get('DONG_SNIPPET_DIR'), help='dir for the local snippet repos.')
@click.option('--override', default=False, help='to override or not when NEW_SNIPPET already exists.')
def load(remote, module, src_snippet, new_snippet, location, override):
    """load the requested snippets to the current directory."""

    repo = git.Repo(os.path.join(location, remote))
    repo.remote('origin').fetch()
    repo.git.checkout('origin/master')
    src_path = os.path.join(repo.working_dir, module, src_snippet)

    if new_snippet == '':
        new_snippet = src_snippet
    if not override and os.path.isfile(new_snippet):
        utils.eprint(new_snippet +
                     ' already exists, pls choose another file name.')
    else:
        shutil.copyfile(src_path, new_snippet)

    repo.git.checkout('master')


@snippet.command()
@click.argument('remote')
@click.argument('module')
@click.argument('src-snippet')
@click.argument('submit-snippet', default='')
@click.option('--location', default=lambda: os.environ.get('DONG_SNIPPET_DIR'), help='dir for the local snippet repos.')
@click.option('--override', is_flag=True, default=False, help='to override or not when NEW_SNIPPET already exists.')
def submit(remote, module, src_snippet, submit_snippet, location, override):
    """submit the created snippet to the snippets reposotory."""

    commit_msg = ' '.join([
        'dong snippet submit', remote, module, src_snippet,
        submit_snippet if submit_snippet else 'None'
    ])

    repo = git.Repo(os.path.join(location, remote))

    submit_snippet = src_snippet if submit_snippet == '' else submit_snippet
    submit_dir = os.path.join(repo.working_dir, module)
    submit_path = os.path.join(submit_dir, submit_snippet)

    repo.remote('origin').fetch()

    if 'origin/master' in repo.git.branch(r=True).split(' '):
        repo.git.checkout('master')
        repo.remote('origin').pull(rebase=True)
    elif 'master' not in repo.branches:
        repo.git.checkout(b='master')
    else:
        repo.git.checkout('master')

    if not override and os.path.isfile(submit_path):
        utils.eprint(submit_snippet +
                     ' already exists, pls choose another file name.')
    else:
        pathlib.Path(submit_dir).mkdir(parents=True, exist_ok=True)
        shutil.copyfile(src_snippet, submit_path)
        repo.index.add([os.path.join(module, submit_snippet)])
        repo.index.commit(commit_msg)
        repo.remote('origin').push()
