from fabric import task
from fabric import Connection
import paramiko
#paramiko.common.logging.basicConfig(level=paramiko.common.DEBUG)

def update_common(c):
    c.local('cd ../katsu-py; tar czf ../deploy-katsu/deploy.tgz *')
    c.put("deploy.tgz", "/tmp/")
    print(c.run("sudo -i -u ubuntu sh -c 'cd ~/katsu;tar xf /tmp/deploy.tgz'").stdout)
    print(c.run("sudo -i -u ubuntu sh -c 'cd ~/katsu;pipenv install'").stdout)

def launch_common(c):
    print(c.run("sudo -i -u ubuntu sh -c 'tmux send-keys \"cd ~/katsu/src;pipenv run python manage.py migrate --settings=core.settings.local\" C-m'").stdout)
    print(c.run("sudo -i -u ubuntu sh -c 'tmux send-keys \"cd ~/katsu/;pipenv run make runserver\" C-m'").stdout)
    
@task
def install(c):
    print(c.run("sudo apt-get -y update").stdout)
    print(c.run("sudo apt-get -y upgrade").stdout)
    print(c.run("sudo apt-get -y install python3-pip nginx").stdout)
    print(c.run("sudo -i -u ubuntu pip3 install pipenv").stdout)
    print(c.run("sudo cp /tmp/proxy  /etc/nginx/sites-available/").stdout)
    print(c.run("sudo ln -s /etc/nginx/sites-available/proxy /etc/nginx/sites-enabled/proxy|true").stdout)
    print(c.run("sudo rm -f /etc/nginx/sites-enabled/default").stdout)
    print(c.run("sudo service nginx restart").stdout)
    c.put("proxy", "/tmp/")
    print(c.run("sudo -i -u ubuntu sh -c 'mkdir -p ~/katsu'").stdout)
    update_common(c)
    print(c.run("sudo -i -u ubuntu sh -c 'tmux ls || tmux new-session -d;sleep 5'").stdout)
    print(c.run("sudo -i -u ubuntu sh -c 'tmux send-keys \"mv $HOME/katsu/fullstackaiplatform-storage-admin.json /tmp\" C-m'").stdout)
    print(c.run("sudo -i -u ubuntu sh -c 'tmux send-keys \"export FIREBASE_ACCOUNT_KEY_FILE=$HOME/katsu/fullstackaiplatform-firebase-adminsdk-sacfy-5ba1f087d0.json\" C-m'").stdout)
    launch_common(c)

@task
def update(c):
    update_common(c)
    print(c.run("sudo -i -u ubuntu sh -c 'tmux send-keys C-c'").stdout)
    launch_common(c)

@task
def sqlproxy(c):
    c.run("sudo wget -nc https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O /opt/cloud_sql_proxy|true")
    print(c.run("sudo chmod +x /opt/cloud_sql_proxy").stdout)
