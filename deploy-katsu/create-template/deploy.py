import os
import sys
import compute
import storage
import subprocess
import image

zone = 'asia-east1-b'
project = 'fullstackaiplatform'
bucket = 'libgirl-base-packages'


class prepare_disk():
    def __init__(self, name):
        self.name = name

    def _get_startup_script(self):
        content = """#!/bin/bash
apt-get -y update
apt-get -y upgrade
apt-get -y install python3-pip nginx
sudo -i -u ubuntu pip3 install pipenv
gsutil cp -r gs://{}/deploy/{} /tmp
cp /tmp/*/proxy /etc/nginx/sites-available/
cp /tmp/*/boot /home/ubuntu/boot
chmod +x /home/ubuntu/boot
ln -s /etc/nginx/sites-available/proxy /etc/nginx/sites-enabled/proxy
rm -f /etc/nginx/sites-enabled/default
service nginx restart
sudo -i -u ubuntu sh -c "echo '@reboot /home/ubuntu/boot'|crontab"
sudo -i -u ubuntu sh -c 'mkdir -p ~/katsu'
sudo -i -u ubuntu sh -c 'cd ~/katsu;tar xf /tmp/*/t.tgz'
sudo -i -u ubuntu sh -c 'cd ~/katsu;pipenv install'
wget -nc https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O /usr/bin/cloud_sql_proxy
chmod +x /usr/bin/cloud_sql_proxy
gcloud -q compute instances delete {} --zone {}
""".format(bucket, self.name, self.name, zone)
        return content

    def upload(self):
        subprocess.call(
            'cd ../../katsu-py; tar czf ../deploy-katsu/create-template/{}.tgz *'
            .format(self.name),
            shell=True)
        storage.upload_file('{}.tgz'.format(self.name),
                            bucket,
                            blob_name='deploy/{}/t.tgz'.format(self.name))
        storage.upload_file('boot',
                            bucket,
                            blob_name='deploy/{}/boot'.format(self.name))
        storage.upload_file('../proxy',
                            bucket,
                            blob_name='deploy/{}/proxy'.format(self.name))
        storage.upload_file(
            'cloud_sql_proxy@.service',
            bucket,
            blob_name='deploy/{}/cloud_sql_proxy@.service'.format(self.name))
        subprocess.call('rm {}.tgz'.format(self.name), shell=True)

    def instance(self):
        instance_name = self.name
        startup_script = self._get_startup_script()
        compute.create_instance(
            project,
            instance_name,
            startup_script,
            http=True,
            source_disk_image=None,
            credentials=None,
            zone=zone,
            autoDelete=False,
            serviceAccount={
                'serviceAccounts': [{
                    'email':
                    '1076847360700-compute@developer.gserviceaccount.com',
                    'scopes': [
                        'https://www.googleapis.com/auth/devstorage.read_only',
                        'https://www.googleapis.com/auth/logging.write',
                        'https://www.googleapis.com/auth/monitoring.write',
                        'https://www.googleapis.com/auth/servicecontrol',
                        'https://www.googleapis.com/auth/service.management.readonly',
                        'https://www.googleapis.com/auth/trace.append',
                        'https://www.googleapis.com/auth/compute'
                    ]
                }]
            },
            wait=True)

    def convert_image(self):
        image.insert_image(project=project,
                           name=self.name,
                           zone=zone,
                           disk=self.name)

    def instance_template(self):
        compute.instanceTemplate(project=project, name=self.name)

    def create_template(self):
        self.upload()
        self.instance()
        self.convert_image()
        self.instance_template()

    def _get_startup_script2(self):
        content = """#!/bin/bash
apt-get -y update
apt-get -y upgrade
apt-get -y install python3-pip nginx
sudo -i -u ubuntu pip3 install pipenv
gsutil cp -r gs://{}/deploy/{} /tmp
cp /tmp/*/proxy  /etc/nginx/sites-available/
cp /tmp/*/cloud_sql_proxy@.service /etc/systemd/system/cloud_sql_proxy@.service
ln -s /etc/nginx/sites-available/proxy /etc/nginx/sites-enabled/proxy
rm -f /etc/nginx/sites-enabled/default
service nginx restart
sudo -i -u ubuntu sh -c 'mkdir -p ~/katsu'
sudo -i -u ubuntu sh -c 'cd ~/katsu;tar xf /tmp/*/t.tgz'
sudo -i -u ubuntu sh -c 'cd ~/katsu;pipenv install'
wget -nc https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O /usr/bin/cloud_sql_proxy
chmod +x /usr/bin/cloud_sql_proxy
gcloud -q compute instances delete {} --zone {}
""".format(bucket, self.name, self.name, zone)
        return content

    def instance2(self, name):
        instance_name = name
        startup_script = self._get_startup_script2()
        compute.create_instance(
            project,
            instance_name,
            startup_script,
            http=True,
            source_disk_image=None,
            credentials=None,
            zone=zone,
            autoDelete=False,
            serviceAccount={
                'serviceAccounts': [{
                    'email':
                    '1076847360700-compute@developer.gserviceaccount.com',
                    'scopes': [
                        'https://www.googleapis.com/auth/sqlservice.admin',
                        'https://www.googleapis.com/auth/devstorage.read_only',
                        'https://www.googleapis.com/auth/logging.write',
                        'https://www.googleapis.com/auth/monitoring.write',
                        'https://www.googleapis.com/auth/servicecontrol',
                        'https://www.googleapis.com/auth/service.management.readonly',
                        'https://www.googleapis.com/auth/trace.append',
                        'https://www.googleapis.com/auth/compute'
                    ]
                }]
            },
            wait=True)


    def help(self):
        print ("deploy.py template [template-name]")
        print ("deploy.py instance template-name [name]")


    def create_instance(self,name):
        instance_name = name
        startup_script = ''
        compute.create_instance(
            project,
            instance_name,
            startup_script,
            http=True,
            source_disk_image=None,
            credentials=None,
            zone=zone,
            autoDelete=False,
            serviceAccount={
                'serviceAccounts': [{
                    'email':
                    '1076847360700-compute@developer.gserviceaccount.com',
                    'scopes': [
                        'https://www.googleapis.com/auth/sqlservice.admin',
                        'https://www.googleapis.com/auth/devstorage.read_only',
                        'https://www.googleapis.com/auth/logging.write',
                        'https://www.googleapis.com/auth/monitoring.write',
                        'https://www.googleapis.com/auth/servicecontrol',
                        'https://www.googleapis.com/auth/service.management.readonly',
                        'https://www.googleapis.com/auth/trace.append',
                        'https://www.googleapis.com/auth/compute'
                    ]
                }]
            },
            wait=False)


## scriptin from here
def main():
    if (len(sys.argv) != 1):
        cmd = sys.argv[1]
    else:
        cmd = "help"

    if cmd == "help":
        prepare_disk("").help()
    elif cmd == "template":
        if (len(sys.argv) >= 3):
            name = sys.argv[2]
        else:
            name = compute.random_instance_name(prefix='katsu')
        print("name:{}".format(name))
        disk = prepare_disk(name)
        disk.create_template()
    elif cmd == "instance":
        disk = prepare_disk(sys.argv[2])
        if (len(sys.argv) >= 4):
            name = sys.argv[3]
        else:
            name = compute.random_instance_name(prefix='katsu')
        disk.create_instance(name)


if __name__ == "__main__":
    main()
