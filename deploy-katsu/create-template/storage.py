import logging
import os
import string
import hashlib
from google.cloud import storage


logger = logging.getLogger(__name__)



def upload_file(path, bucket_name, *, blob_name=None):
    file_name = os.path.basename(path)

    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)
    blob.upload_from_filename(path)

    return blob.public_url
