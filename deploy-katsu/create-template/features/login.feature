Feature: Login feature
    #==== login
    Scenario: User enters correct password
        Given an user account with the <email> and the <password>
        When some user logins with the <email> and the <password>
        Then output : "Login success!"
        And user can use login function

    Scenario: User enters wrong password
        Given an user account with the <email> and the <wrong password>
        When some user logins with the <email> and the <wrong password>
        Then output : "Login failed"
