@given(u'user has a project directory that can be initialize.')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given user has a project directory that can be initialize.')


@when(u'user is under project folder')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user is under project folder')


@when(u'user enters "dong init"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong init"')


@then(u'dong initialize project folllow the template')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then dong initialize project folllow the template')
