import subprocess

@when(u'user enters "dong data"')
def step_impl(context):
    context.ret = subprocess.call(
        'dong data',
        shell=True)


@then(u'return')
def step_impl(context):
    assert context.ret != 0


@then(u'output <error message>')
def step_impl(context):
    pass

