from behave import given, when, then
import subprocess
import os
from pathlib import Path


@given(u'an user account with the <email> and the <password>')
def prepare_param(context):
    pass

@when(u'some user logins with the <email> and the <password>')
def step_impl(context):
    j= context.json
    subprocess.call(
        'rm -f ~/.netrc;dong login -u {} -p {}'.format(j['login']['good']['email'],j['login']['good']['password']),
        shell=True)


@then(u'output : "Login success!"')
def step_impl(context):
    pass


@then(u'user can use login function')
def step_impl(context):
    assert os.path.isfile("{}/.netrc".format(str(Path.home()))) is True


@given(u'an user account with the <email> and the <wrong password>')
def step_impl(context):
    pass


@when(u'some user logins with the <email> and the <wrong password>')
def step_impl(context):
    j= context.json
    subprocess.call(
        'rm -f ~/.netrc;dong login -u {} -p {}'.format(j['login']['bad']['email'],j['login']['bad']['password']),
        shell=True)


@then(u'output : "Login failed"')
def step_impl(context):
    assert os.path.isfile("{}/.netrc".format(str(Path.home()))) is False

