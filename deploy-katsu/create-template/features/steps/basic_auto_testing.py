import subprocess
import re

@when(u'user enters "dong version"')
def step_impl(context):
    command = "dong version"
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=None, shell=True)
    output = process.communicate()

    context.output =  output[0].decode()


@then(u'output <dong version>')
def step_impl(context):
    assert re.search("^dong [0-9.]*$",context.output) is not None


@when(u'user enters "dong"')
def step_impl(context):
    command = "dong"
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=None, shell=True)
    output = process.communicate()

    context.output =  output[0].decode()


@then(u'output <main command>')
def step_impl(context):
    #print (context.output)
    assert re.search("Universal Command Line Interface for Libgirl AI Platform",
                context.output) is not None
