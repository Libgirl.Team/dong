import subprocess
import os

@given(u'user is under the project folder')
def step_impl(context):
    # prepare directory
    if not os.path.isdir('my_dong_mnist'):
        command = "git clone https://github.com/libgirlenterprise/my_dong_mnist.git"
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=None, shell=True)
        output = process.communicate()
        context.output =  output[0].decode()
    

@given(u'setup.py in project folder')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given setup.py in project folder')


@when(u'user enters dong train exec <command>')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters dong train exec <command>')


@then(u'dong package project')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then dong package project')


@then(u'upload to dong cloud for training')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then upload to dong cloud for training')


@given(u'project folder has no setup.py')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given project folder has no setup.py')


@then(u'output "setup.py not found under current working directory"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then output "setup.py not found under current working directory"')


@given(u'user has train jobs')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given user has train jobs')


@when(u'user enters "dong train ls"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong train ls"')


@then(u'output <jobs list>')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then output <jobs list>')


@given(u'user has a training job')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given user has a training job')


@when(u'user enters "dong train status -j <job name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong train status -j <job name>"')


@then(u'output <job status>')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then output <job status>')


@when(u'user enters "dong train status <job name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong train status <job name>"')


@when(u'When user enters "dong train kill -j <job name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When When user enters "dong train kill -j <job name>"')


@when(u'When user enters "dong train kill <job name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When When user enters "dong train kill <job name>"')
