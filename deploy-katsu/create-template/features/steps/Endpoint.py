@given(u'user has a already trained job')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given user has a already trained job')


@when(u'user enters "dong endpoint up <job name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong endpoint up <job name>"')


@then(u'output "Bring up..."')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then output "Bring up..."')


@then(u'send reguest to <api>')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then send reguest to <api>')


@then(u'output <message>')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then output <message>')


@when(u'user enters "dong endpoint ls"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong endpoint ls"')


@given(u'user has an endpoint')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given user has an endpoint')


@when(u'user enters "dong endpoint status -e <endpoint name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong endpoint status -e <endpoint name>"')


@when(u'user enters "dong endpoint status <endpoint name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong endpoint status <endpoint name>"')


@given(u'user has an endpoint can be kill')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given user has an endpoint can be kill')


@when(u'When user enters "dong endpoint kill -e <endpoint name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When When user enters "dong endpoint kill -e <endpoint name>"')


@then(u'output <killing message>')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then output <killing message>')


@when(u'When user enters "dong train kill -e <endpoint name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When When user enters "dong train kill -e <endpoint name>"')
