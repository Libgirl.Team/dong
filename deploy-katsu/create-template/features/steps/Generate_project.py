@when(u'user enters dong new "<project-name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters dong new "<project-name>"')


@then(u'dong generate a new project folllow the template')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then dong generate a new project folllow the template')


@then(u'output: "Generate ML project:<project-name> successful!"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then output: "Generate ML project:<project-name> successful!"')


@given(u'Generate a new project without name')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given Generate a new project without name')


@when(u'user enters "dong new"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong new"')
