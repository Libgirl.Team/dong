@given(u'user specifies the <module type>')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given user specifies the <module type>')


@given(u'user specifies a <name> for the module')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given user specifies a <name> for the module')


@when(u'user enters "dong template <module type> <name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong template <module type> <name>"')


@then(u'dong generate template file in the filepath')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then dong generate template file in the filepath')


@when(u'user enters "dong template <multiple modules with name>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong template <multiple modules with name>"')


@then(u'dong generate template files in the filepath')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then dong generate template files in the filepath')


@given(u'without <name>')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given without <name>')


@when(u'user enters "dong template <module type>"')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user enters "dong template <module type>"')
