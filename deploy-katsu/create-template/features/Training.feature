Feature: Training
    #==== exec
    Scenario: User trains a trainable project
        Given user is under the project folder
        And setup.py in project folder
        When user enters dong train exec <command>
        Then dong package project
        And upload to dong cloud for training
        And output <message>
        #Examples:
        #    |message|
        #    """
        #    New Job-name:  tr_811ba
        #    [Usage] Status check:
        #    dong train status -j tr_811ba
        #    """

    Scenario: User trains a untrainable project
        Given project folder has no setup.py
        When user enters dong train exec <command>
        Then return
        And output "setup.py not found under current working directory"

    #==== ls
    Scenario: User listing training jobs
        Given user has train jobs
        When user enters "dong train ls"
        Then output <jobs list>

    #==== status
    Scenario: User check training job status with option -j
        Given user has a training job
        When user enters "dong train status -j <job name>"
        Then output <job status>

    Scenario: User check training job status without option -j
        Given user has a training job
        When user enters "dong train status <job name>"
        Then return
        And output <error message>

    #==== kill
    Scenario: User kill a existing training job with option -j
        Given user has a training job
        When  When user enters "dong train kill -j <job name>"
        Then send reguest to <api>
        And output <killing message>

    Scenario: User kill a existing training job without option -j
        Given user has a training job
        When  When user enters "dong train kill <job name>"
        Then return
        And output <error message>
