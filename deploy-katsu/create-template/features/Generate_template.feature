Feature: Generate template in project file
    #==== template
    Scenario: User generators single module with name
        Given user specifies the <module type>
        And user specifies a <name> for the module
        When user enters "dong template <module type> <name>"
        Then dong generate template file in the filepath
        #Examples:
        #    | module type | name     | filepaths                            |
        #    | model       | evelin   | /models/evelin.py                    |
        #    | save_load   | sl       | /save_load/sl.py                     |
        #    | train       | dalian   | /trains/dalian.py,/trains/dalian.ini |
        #    | endpoint    | 101      | /endpoints/101.py                    |
        #    | data        | cat      | /data/cat.py                         |
        #    | build       | 20190417 | /builds/20190417.cfg                 |

    Scenario: Users generator multiple modules with name
        When user enters "dong template <multiple modules with name>"
        Then dong generate template files in the filepath
        #Examples:
        #    | multiple modules with name                                                       |
        #    | --data-module new_data --tune-module new_tune                                    |
        #    | --config-module new_con --tune-module new_tune --model-serializer-module new_ser |

    Scenario: User generators moudle without name
        Given user specifies the <module type>
        And without <name>
        When user enters "dong template <module type>"
        Then return
        And output <error message>

    Scenario: User not inside a dong project
        When user enters "dong template <multiple modules with name>"
        Then return
        And output <error message>
