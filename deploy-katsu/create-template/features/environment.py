import os
import shutil
import subprocess
import json
from pathlib import Path

from googleapiclient import discovery
from behave import fixture, use_fixture

ip = None

def before_feature(context, feature):
    shutil.copyfile(
        "{}/.netrc".format(os.path.dirname(os.path.abspath(__file__))),
        "{}/.netrc".format(str(Path.home())))

def before_all(context):
    home = str(Path.home())
    script = os.path.dirname(os.path.abspath(__file__));
    if os.path.isfile("{}/param.json".format(script)):
        f = open("{}/param.json".format(script), 'r')
        context.json = json.load(f)
        j= context.json
    else:
        raise NameError('Prepare json')
    print(context.json)
    if os.path.isfile("{}/.netrc".format(home)):
        shutil.copyfile(
            "{}/.netrc".format(home),
            "{}/.netrc_backup".format(script))
    if os.path.isfile("{}/.netrc".format(script)):
        os.remove("{}/.netrc".format(script))
    
    subprocess.call(
        'rm -f ~/.netrc;dong login -u {} -p {}'.format(j['login']['good']['email'],j['login']['good']['password']),
        shell=True)
    shutil.copyfile(
        "{}/.netrc".format(home),
        "{}/.netrc".format(script))
    compute = discovery.build('compute', 'v1', credentials=None)
    instance_list=compute.instances().list(zone='asia-east1-b',project='fullstackaiplatform').execute()
    items=instance_list['items']
    ip = items[len(items)-1]['networkInterfaces'][0]['accessConfigs'][0]['natIP']
    os.environ['DONG_DEBUG']="http://{}".format(ip)

def after_all(context):
    home = str(Path.home())
    script = os.path.dirname(os.path.abspath(__file__));
    if os.path.isfile("{}/.netrc_backup".format(script)):
        shutil.move(
            "{}/.netrc_backup".format(script),
            "{}/.netrc".format(home))
    if os.path.isfile("{}/.netrc".format(script)):
        os.remove("{}/.netrc".format(script))
