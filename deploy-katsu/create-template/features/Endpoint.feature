Feature: Endpoint
    #==== up
    Scenario: User deploy a trained job
        Given user has a already trained job
        When user enters "dong endpoint up <job name>"
        Then output "Bring up..."
        And send reguest to <api>
        And output <message>
        #Examples:
        #    | message |
        #    """
        #    New endpoint-name:  ep_6eqgl
        #    [Usage] Status check:
        #    dong endpoint status -e ep_6eqgl
        #    """

    Scenario: User deploy a not exist job
        When user enters "dong endpoint up <job name>"
        Then output "Bring up..."
        And send reguest to <api>
        And output <error message>

    #==== ls
    Scenario: User listing endpoint
        When user enters "dong endpoint ls"
        Then output <message>
        #Examples:
        #    | message |
        #    """
        #    ENDPOINT NAME    EXTERNAL IP     STATUS
        #    ep_trxrw                         NotFound
        #    ep_6eqgl         35.229.164.254  Running
        #    ep_6eqgl         35.229.164.252  Preparing
        #    """

    #==== status
    Scenario: User check a endpoint status with option -e
        Given user has an endpoint
        When user enters "dong endpoint status -e <endpoint name>"
        Then output <message>
        #Examples:
        #    | message |
        #    """
        #    Endpoint name: ep_6eqgl
        #    External ip: 35.229.164.254
        #    Status: Preparing
        #    """

    Scenario: User check a endpoint status without option -e
        Given user has an endpoint
        When user enters "dong endpoint status <endpoint name>"
        Then return
        And output <error message>

    #==== kill
    Scenario: User kills a existing endpint with option -e
        Given user has an endpoint can be kill
        When  When user enters "dong endpoint kill -e <endpoint name>"
        Then send reguest to <api>
        And output <killing message>

    Scenario: User kills a existing endpint without option -e
        Given user has an endpoint can be kill
        When  When user enters "dong train kill -e <endpoint name>"
        Then send reguest to <api>
        And output <killing message>
