Feature: Generate a new project folllow the template
    #==== new
    Scenario: User generate new project with giving a name
        When user enters dong new "<project-name>"
        Then dong generate a new project folllow the template
        And output: "Generate ML project:<project-name> successful!"

    Scenario: User generate new project without giving a name
        Given Generate a new project without name
        When user enters "dong new"
        Then output <error message>
