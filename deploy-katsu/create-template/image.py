from time import sleep 
from googleapiclient import discovery


def insert_image(*,credentials=None,project=None,name=None,zone=None,disk=None):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    config = {
        'kind':
        'compute#image',
        'name':
        name,
        'sourceDisk':
        'zones/{}/disks/{}'.format(zone,disk),
    }
    compute.images().insert(project=project,
                            body=config).execute()
    c = 0
    while compute.images().get(project=project,image=name).execute()['status'] != 'READY':
        print("waiting for image ready {} sec passed".format(c* 30))
        c = c + 1
        sleep(30)
    compute.disks().delete(project=project, zone=zone, disk=disk).execute()
