import os
import random
import string
from googleapiclient import discovery
from datetime import datetime
from time import sleep 


def random_name(num=5):
    return ''.join([
        random.choice(string.ascii_lowercase + string.digits)
        for n in range(num)
    ])

def random_instance_name(prefix='deply',suffix=''):
    return '{}-{}'.format(prefix, datetime.now().strftime("%Y%m%d%H%M%S"))


def create_instance(project,
                    instance_name,
                    startup_script=None,
                    http=False,
                    source_disk_image=None,
                    serviceAccount=True,
                    credentials=None,
                    autoDelete=True,
                    zone=None,
                    wait=None,
                   ):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    if source_disk_image is None:
        image_response = compute.images().getFromFamily(
            project='ubuntu-os-cloud', family='ubuntu-1804-lts').execute()
        source_disk_image = image_response['selfLink']
    print("source_disk_image: {}".format(source_disk_image))
    machine_type = "zones/%s/machineTypes/n1-standard-1" % zone
    config = {
        'name':
        instance_name,
        'machineType':
        machine_type,
        'disks': [{
            'boot': True,
            'autoDelete': autoDelete,
            'initializeParams': {
                'sourceImage': source_disk_image,
            }
        }],
        'networkInterfaces': [{
            'network':
            'global/networks/default',
            'accessConfigs': [{
                'type': 'ONE_TO_ONE_NAT',
                'name': 'External NAT'
            }]
        }],
    }

    if serviceAccount is None:
        None
    else:
        config.update(serviceAccount)

    if startup_script is not None:
        config.update({
            'metadata': {
                'items': [{
                    'key': 'startup-script',
                    'value': startup_script
                }]
            }
        })

    if http is True:
        config.update({'tags': {'items': ['http-server']}})

    print("create_instance config:{}".format(config))
    response = compute.instances().insert(project=project,
                                          zone=zone,
                                          body=config).execute()
    if wait is True:
        name = instance_name
        c = 0
        while True:
            print("waiting for instance finish installation {} sec passed".format(c* 30))
            c = c + 1
            sleep(30)
            try:
                status = compute.instances().get(project=project,zone=zone,instance=name).execute()
            except Exception as e:
                status = 'UNSPECIFIED'
            if(status == 'UNSPECIFIED'):
                break;

    return response


def delete_instance(project, instance_name, credentials=None):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    return compute.instances().delete(project=project,
                                      zone=zone,
                                      instance=instance_name).execute()


def attach_disk(project, instance_name, disk_name, credentials=None):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    attach_disk_body = {'source': disk_name}
    response = compute.instances().attachDisk(project=project,
                                              zone=zone,
                                              instance=instance_name,
                                              body=attach_disk_body).execute()


def status(project, instance_name, credentials=None,zone=None):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    response = compute.instances().get(project=project,
                                       zone=zone,
                                       instance=instance_name).execute()
    return response

def instanceTemplate(project, name, credentials=None):
    service = discovery.build('compute', 'v1', credentials=credentials)
    instance_template_body = {
        'name': name,
        'description': '',
        'properties': {
            'machineType': 'n1-standard-1',
            'displayDevice': {
                'enableDisplay': False
            },
            'metadata': {
                'kind': 'compute#metadata',
                'items': []
            },
            'disks': [
                {
                    'kind': 'compute#attachedDisk',
                    'type': 'PERSISTENT',
                    'boot': True,
                    'mode': 'READ_WRITE',
                    'autoDelete': True,
                    'deviceName': name,
                    'initializeParams': {
                        'sourceImage': 'projects/fullstackaiplatform/global/images/{}'.format(name),
                        'diskType': 'pd-standard',
                        'diskSizeGb': '10'
                    }
                }
            ],
            'canIpForward': False,
            "networkInterfaces": [
                {
                    "kind": "compute#networkInterface",
                    "network": "projects/fullstackaiplatform/global/networks/default",
                    "accessConfigs": [
                        {
                            "kind": "compute#accessConfig",
                            "name": "External NAT",
                            "type": "ONE_TO_ONE_NAT",
                            "networkTier": "PREMIUM"
                        }
                    ],
                    "aliasIpRanges": []
                }
            ],
            "labels": {},
            "scheduling": {
                "preemptible": False,
                "onHostMaintenance": "MIGRATE",
                "automaticRestart": True,
                "nodeAffinities": []
            },
            "reservationAffinity": {
                "consumeReservationType": "ANY_RESERVATION"
            },
            "serviceAccounts": [
                {
                    "email": "1076847360700-compute@developer.gserviceaccount.com",
                    "scopes": [
                        "https://www.googleapis.com/auth/devstorage.read_only",
                        "https://www.googleapis.com/auth/logging.write",
                        "https://www.googleapis.com/auth/monitoring.write",
                        "https://www.googleapis.com/auth/servicecontrol",
                        "https://www.googleapis.com/auth/service.management.readonly",
                        "https://www.googleapis.com/auth/trace.append"
                    ]
                }
            ]
        }
    }
    request = service.instanceTemplates().insert(project=project, body=instance_template_body)
    return request.execute()
