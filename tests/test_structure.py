from os.path import isdir, isfile

from dong import structure

def test_create_structure(tmpfolder):
    expect = {
        "my_file": "Some content",
        "my_folder": {
            "my_dir_file": "Some other content",
        }
    }

    changed = structure.create_structure(expect, {})
    assert changed == expect
    assert isdir("my_folder")
    assert isfile("my_folder/my_dir_file")
    assert isfile("my_file")
    assert open("my_file").read() == "Some content"
    assert open("my_folder/my_dir_file").read() == "Some other content"


def test_define_structure():
    opts = {
        'project': 'my_project',
        'package': 'my_package'
    }
    struct = structure.define_structure(opts)
    assert isinstance(struct, dict)
