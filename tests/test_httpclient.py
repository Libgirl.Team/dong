
from _pytest.monkeypatch import MonkeyPatch
import requests
import dong
import dong.httpclient as client

class Object(object):
    pass


def mock_getpost(url, **kwargs):
    r = Object()
    r.status_code = 200
    return r


def test_get():
    monkeypatch = MonkeyPatch()
    monkeypatch.setattr(requests, 'get', mock_getpost)
    assert client.get('get').status_code == 200


def test_post():
    monkeypatch = MonkeyPatch()
    monkeypatch.setattr(requests, 'post', mock_getpost)
    assert client.post('post', json={'who': 'whoami'}).status_code == 200
