import os
import dong.utils as utils

def test_create_directory(tmpfolder):
    utils.create_directory('a-test')
    assert tmpfolder.join('a-test').check(dir=1)


def test_create_file(tmpdir):
    filename = tmpdir.join('test-file')
    utils.create_file(filename, 'content')
    assert filename.read() == 'content'
