import os
import dong.templates as templates

def test_get_template():
    template = templates.get_template("setup_py")
    content = template.safe_substitute()
    assert content.split(os.linesep, 1)[0] == 'from setuptools import setup'
