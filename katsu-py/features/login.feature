Feature: Login feature

  As a user
  I want to login with my details
  So that I can start using other CLI commands

  Scenario: User enters correct password
    Given an user with the email team@libgirl.com and the password 0223119816
    When he logins
    Then login should be successful

  Scenario: User enters wrong password
    Given an user with the email team@libgirl.com and the password 12341234
    When he logins
    Then login should be failed
