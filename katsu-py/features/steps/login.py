from behave import given, when, then

@given(u'an user with the email {email} and the password {password}')
def prepare_credentials(context, email, password):
    context.email = email
    context.password = password


@when(u'he logins')
def try_login(context):
    email = context.email
    password = context.password
    context.response = context.test.client.post(
        '/api/v1/login/',
        {'email': email, 'password': password}
    )


@then(u'login should be {status}')
def login_status(context, status):
    if status == 'successful':
        context.test.assertEqual(200, context.response.status_code)
    else:
        context.test.assertNotEqual(200, context.response.status_code)
