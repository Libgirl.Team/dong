# Katsu in Django

## Run

Using pipenv

```
pipenv install
pipenv shell
```

## Local development

Use service account credential

```
# Download credential from https://drive.google.com/drive/folders/1Ubejb0wAazhgbrXVUHIpfEy6pBvUjVK4?usp=sharing

export GOOGLE_APPLICATION_CREDENTIALS="/path/to/fullstackaiplatform-storage-admin.json"
export FIREBASE_ACCOUNT_KEY_FILE="/path/to/fullstackaiplatform-firebase-adminsdk-sacfy-5ba1f087d0.json"
```

```
cd src
python manage.py migrate --settings=core.settings.local
cd ..
make runserver
```

## Testing

Run testing

```
make devserver
```
