import random
import string

zone = 'asia-east1-a'


def random_name(num=5):
    return ''.join([
        random.choice(string.ascii_lowercase + string.digits)
        for n in range(num)
    ])
