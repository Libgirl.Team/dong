import logging
import os
import random
import string
import hashlib
from google.cloud import storage

from . import random_name

logger = logging.getLogger(__name__)


def _random_job_name(num=5):
    cities = [
        'munich', 'bangkok', 'yokohama', 'nhopal', 'astana', 'minsk',
        'johannesburg', 'jaipur', 'hiroshima', 'novosibirsk', 'kyoto',
        'zhongshan', 'taipei', 'taichung', 'kaohsiung'
    ]

    name = random.choice(cities)
    id_name = random_name(num)

    return name + '-' + id_name


def _get_storage_client(credentials=None):
    return storage.Client(credentials=credentials)


def _calc_checksum(path_to_file, algorithm=hashlib.sha256):
    hash = algorithm()
    with open(path_to_file, "rb") as f:
        while True:
            chunk = f.read(4096)
            if not chunk:
                break
            hash.update(chunk)

    return hash.hexdigest()


def upload_file(path, bucket_name, *, blob_name=None, credentials=None):
    if credentials is None:
        credentials = GoogleCredentials.get_application_default()
    file_name = os.path.basename(path)
    job_name = _random_job_name()
    checksum = _calc_checksum(path)
    if blob_name is None:
        blob_name = os.path.join(job_name, checksum, file_name)

    client = _get_storage_client(credentials)
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)
    blob.upload_from_filename(path)

    return blob.public_url


def download_string(bucket_name, source_blob_name, credentials=None):
    client = _get_storage_client(credentials)
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    return blob.download_as_string()


def download_file(path, bucket_name, source_blob_name, credentials=None):
    client = _get_storage_client(credentials)
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    f = open(path, 'wb')
    return blob.download_to_file(f)


def delete_blob(bucket_name, blob_name, credentials=None):
    client = _get_storage_client(credentials)
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)

    return blob.delete()


def list_blobs(bucket_name, *, page_token=None, credentials=None):
    # https://google-cloud-python.readthedocs.io/en/0.8.0/storage-buckets.html
    client = _get_storage_client(credentials)
    bucket = client.get_bucket(bucket_name)

    return bucket.list_blobs()


def list_buckets(*, page_token=None, credentials=None):
    # https://google-cloud-python.readthedocs.io/en/0.8.0/storage-buckets.html
    client = _get_storage_client(credentials)
    return client.list_buckets()
