import os
from googleapiclient import discovery

from . import zone
from . import random_name


def _random_disk_name(volume=1, num=5, _prefix="lg"):
    uniq_name = os.uname().nodename[0:5].lower() + random_name(num)
    return '{}-{}-disk{}'.format(_prefix, uniq_name, volume)


def create_disk(project,
                sizeGb=10,
                sourceSnapshot=None,
                prefix='lg',
                credentials=None):
    if credentials is None:
        credentials = GoogleCredentials.get_application_default()
    disk_name = _random_disk_name(_prefix=prefix)
    phy_blk = '4096'

    disk_body = {
        'name': disk_name,
        'sizeGb': sizeGb,
        'physicalBlockSizeBytes': phy_blk,
    }
    if sourceSnapshot is not None:
        disk_body.update({'sourceSnapshot': sourceSnapshot})

    service = discovery.build('compute', 'v1', credentials=credentials)
    request = service.disks().insert(project=project,
                                     zone=zone,
                                     body=disk_body)
    response = request.execute()

    return (disk_name, response['targetLink'], response)


def delete_disk(project, name, credentials=None):
    if credentials is None:
        credentials = GoogleCredentials.get_application_default()
    service = discovery.build('compute', 'v1', credentials=credentials)
    request = service.disks().delete(project=project,
                                     zone=zone,
                                     body=disk_body)
    response = request.execute()
    return response


def list_disks(project, credentials=None):
    if credentials is None:
        credentials = GoogleCredentials.get_application_default()
    service = discovery.build('compute', 'v1', credentials=credentials)
    request = service.disks().list(project=project, zone=zone)
    while request is not None:
        response = request.execute()
        print(response)
        for disk in response['items']:
            print(disk)
        request = service.disks().list_next(previous_request=request,
                                            previous_response=response)
