import os

from django.conf import settings
from core.hash import sha224
from core.google import storage


def from_service_account_file(filename):
    from google.oauth2 import service_account
    return service_account.Credentials.from_service_account_file(filename)


def Default(request):
    email = request.user.email
    print("request: {}".format(request))
    hash = sha224.hash(email)
    path = "/tmp/{}.json".format(hash)
    print("email:{}".format(email))
    if not os.path.isfile(path):
        storage.download_file(path=path,
                              bucket_name='libgirl-doorkeeper',
                              source_blob_name='{}.json'.format(hash))
    return from_service_account_file(path)
