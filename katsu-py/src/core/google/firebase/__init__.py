import urllib.request
import urllib.error
import urllib.parse
import json

def _make_post_request_using_json(url, data):
    req = urllib.request.Request(url)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    jsondata = json.dumps(data)
    jsondataasbytes = jsondata.encode('utf-8') # needs to be bytes
    req.add_header('Content-Length', len(jsondataasbytes))

    code = 200
    try:
        response = urllib.request.urlopen(req, jsondataasbytes)
        data = json.load(response)
        return data
    except urllib.error.HTTPError as e:
        code = e.code
        return None


def _make_post_request_using_formencode(url, data):
    req = urllib.request.Request(url)
    req.add_header('Content-Type', 'application/x-www-form-urlencoded')
    encode_data = urllib.parse.urlencode(data).encode()

    try:
        response = urllib.request.urlopen(req, encode_data)
        data = json.load(response)
        return data
    except urllib.error.HTTPError as e:
        code = e.code
        return None


def signin_with_email_and_password(api_key, email, password):
    signin_url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key={}'.format(api_key)
    resp = _make_post_request_using_json(signin_url, {'email': email, 'password': password, 'returnSecureToken': True})
    if resp is None:
        return None

    # Use refresh token
    refresh_token = resp['refreshToken']
    return refresh_token
