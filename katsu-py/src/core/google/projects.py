import os
#from pprint import pprint
from googleapiclient import discovery

from oauth2client.client import GoogleCredentials
from core.hash import sha224
from core.google import storage

#service = discovery.build('cloudresourcemanager', 'v1', credentials=credentials)
#project_id = 'test-project-sano'

#request = service.projects().get(projectId=project_id)
#response = request.execute()

#pprint(response)


def list_projects(*, credentials=None):
    if credentials is None:
        credentials = GoogleCredentials.get_application_default()
    service = discovery.build('cloudresourcemanager',
                              'v1',
                              credentials=credentials)
    request = service.projects().list()
    response = request.execute()
    return response.get('projects', [])


def create_project(*, credentials=None):
    if credentials is None:
        credentials = GoogleCredentials.get_application_default()
    service = discovery.build('cloudresourcemanager',
                              'v1',
                              credentials=credentials)

    project_body = {
        'projectId': 'sn-tekito2',
        'parent': {
            'id': '400781712235',
            'type': 'organization'
        },
    }
    request = service.projects().create(body=project_body)
    response = request.execute()
    return response


def delete_project(*, credentials=None):
    if credentials is None:
        credentials = GoogleCredentials.get_application_default()
    service = discovery.build('cloudresourcemanager',
                              'v1',
                              credentials=credentials)
    project_id = 'sn-tekito2'

    request = service.projects().delete(projectId=project_id)
    request.execute()


def switch_project(email):
    hash = sha224.hash(email)
    path = "/tmp/{}.json".format(hash)
    print("email:{}".format(email))
    if not os.path.isfile(path):
        storage.download_file(path=path,
                              bucket_name='libgirl-doorkeeper',
                              source_blob_name='{}.json'.format(hash))


############################################################################
#import os

#pprint(list_projects())
#pprint(create_project())
#pprint(os.environ["GOOGLE_APPLICATION_CREDENTIALS"])
#os.environ["GOOGLE_APPLICATION_CREDENTIALS"]= '/home/snmsts/work/dong/katsu-py/fullstackaiplatform-storage-admin.json'
#pprint(delete_project())

#pprint(list_projects())
#pprint(create_project())
#print('------------------------------------------------------------------')
#pprint(list_projects())
