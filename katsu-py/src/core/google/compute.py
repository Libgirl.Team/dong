import os

from googleapiclient import discovery

from . import zone
from . import random_name


def random_instance_name(prefix='lg'):
    uniq_name = os.uname().nodename[0:5].lower() + random_name()
    return '{}-{}-instance'.format(prefix, uniq_name)


def create_instance(project,
                    instance_name,
                    startup_script=None,
                    http=False,
                    source_disk_image=None,
                    serviceAccount=True,
                    credentials=None):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    if source_disk_image is None:
        image_response = compute.images().getFromFamily(
            project='ubuntu-os-cloud', family='ubuntu-1804-lts').execute()
        source_disk_image = image_response['selfLink']
    print("source_disk_image: {}".format(source_disk_image))
    machine_type = "zones/%s/machineTypes/n1-standard-1" % zone
    config = {
        'name':
        instance_name,
        'machineType':
        machine_type,
        'disks': [{
            'boot': True,
            'autoDelete': True,
            'initializeParams': {
                'sourceImage': source_disk_image,
            }
        }],
        'networkInterfaces': [{
            'network':
            'global/networks/default',
            'accessConfigs': [{
                'type': 'ONE_TO_ONE_NAT',
                'name': 'External NAT'
            }]
        }],
    }

    if serviceAccount is None:
        None
    else:
        config.update(serviceAccount)

    if startup_script is not None:
        config.update({
            'metadata': {
                'items': [{
                    'key': 'startup-script',
                    'value': startup_script
                }]
            }
        })

    if http is True:
        config.update({'tags': {'items': ['http-server']}})

    print("create_instance config:{}".format(config))
    response = compute.instances().insert(project=project,
                                          zone=zone,
                                          body=config).execute()

    return response


def delete_instance(project, instance_name, credentials=None):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    return compute.instances().delete(project=project,
                                      zone=zone,
                                      instance=instance_name).execute()


def get_external_interface(project, instance, credentials=None):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    response = compute.instances().get(project=project,
                                       zone=zone,
                                       instance=instance).execute()

    network_interfaces = response['networkInterfaces']
    for interface in network_interfaces:
        if 'accessConfigs' in interface:
            accessConfigs = interface['accessConfigs']
            if 'natIP' in accessConfigs[0]:
                return interface

    return None


def get_external_ip(project, instance_name, credentials=None):
    network_interface = get_external_interface(project,
                                               instance_name,
                                               credentials=credentials)
    return network_interface['accessConfigs'][0][
        'natIP'] if network_interface else None


def attach_disk(project, instance_name, disk_name, credentials=None):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    attach_disk_body = {'source': disk_name}
    response = compute.instances().attachDisk(project=project,
                                              zone=zone,
                                              instance=instance_name,
                                              body=attach_disk_body).execute()


def status(project, instance_name, credentials=None):
    compute = discovery.build('compute', 'v1', credentials=credentials)
    response = compute.instances().get(project=project,
                                       zone=zone,
                                       instance=instance_name).execute()
    print('respones: {}'.format(response['status']))
    return response['status']
