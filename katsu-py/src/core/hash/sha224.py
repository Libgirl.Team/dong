import hashlib
 
def hash(str):
    hash = hashlib.sha224()
    hash.update(str.encode())
    return hash.hexdigest()


