from django.urls import path

from . import views
from api.views.train import TrainingJobDetail,TrainingJob

urlpatterns = [
    path('Instances/<slug:instance_name>/<slug:method>',views.Instances.as_view()),
    path('train/', TrainingJob.as_view()),
    path('train/<slug:name>/', TrainingJobDetail.as_view()),
    path('train/<slug:name>/<slug:method>', TrainingJobDetail.as_view()),
    path('endpoint/', views.Endpoint.as_view()),
    path('endpoint/<slug:name>/', views.EndpointDetail.as_view()),
    path('endpoint/<slug:name>/<slug:method>', views.EndpointDetail.as_view()),
    path('login/', views.AuthUser.as_view()),
    path('storage/b', views.Storage.as_view()),
    path('storage/b/<slug:bucket>', views.Storage.as_view()),
    path('storage/b/<slug:bucket>/<slug:method>', views.Storage.as_view()),
    path('storage/b/<slug:bucket>/o/<path:object>', views.Storage.as_view()),
]
