import os
import json

from django.conf import settings
from django.shortcuts import get_object_or_404, get_list_or_404
from django.contrib.auth.models import User

from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status

from core.google.firebase.authentication import FirebaseAuthentication
from core.google import Credentials
from core.google import storage

import train
import core
from api import serializers

from api.services import TrainingService


class TrainingJobDetail(APIView):

    authentication_classes = (FirebaseAuthentication, )
    permission_classes = (IsAuthenticated, )

    def post(self, request, name, method=None):
        credentials = Credentials.Default(request)
        job = get_object_or_404(train.models.TrainingJob, name=name)
        data = serializers.TrainingJobSerializer(job).data
        instance_name = data['instance_name']
        message = data['message']

        serv = TrainingService(credentials=credentials)
        serv.kill(instance_name)
        return Response({'name': data['name'], 'status': 'ok'}, status=200)

    def get(self, request, name):
        credentials = Credentials.Default(request)
        job = get_object_or_404(train.models.TrainingJob, name=name)
        data = serializers.TrainingJobSerializer(job).data
        instance_name = data['instance_name']
        message = data['message']

        serv = TrainingService(credentials=credentials)
        status = serv.status(instance_name)
        #print('Intance status: {}'.format(status))
        resp = {'job_name': name, 'status': 'Running', 'message': message}
        if status == 'UNSPECIFIED' or status == 'TERMINATED':
            resp['status'] = 'Succeeded'
            return Response(resp)
        return Response(resp)


class TrainingJob(APIView):
    parser_classes = (MultiPartParser, FormParser)

    authentication_classes = (FirebaseAuthentication, )
    permission_classes = (IsAuthenticated, )

    def upload_and_train(self, user_email, salt_password, job_name, content,
                         module_name, credentials, args):
        # Save file
        package_dir = os.path.join(settings.FILES_ROOT, "packages/")
        package_name = content.name
        if not os.path.isdir(package_dir):
            os.makedirs(package_dir)

        dest_file_path = os.path.join(package_dir, package_name)

        with open(dest_file_path, "wb") as package_file:
            for chunk in content.chunks():
                package_file.write(chunk)

        # Upload to cloud storage
        url = storage.upload_file(dest_file_path,
                                  settings.BUCKET_NAME,
                                  credentials=credentials)
        print("Upload url:{}".format(url))
        # Delete file here?

        # Create GCE instance
        serv = TrainingService(credentials=credentials)
        (instance_name, disk_name) = serv.exec(url, package_name, module_name,
                                               user_email, salt_password,
                                               job_name, args)

        return (instance_name, disk_name, url)

    def get(
            self,
            request,
    ):
        credentials = Credentials.Default(request)
        print('Current user: {}'.format(request.user))

        serv = TrainingService(credentials=credentials)
        # TODO: pagination or filters
        job_list = get_list_or_404(train.models.TrainingJob, user=request.user)
        data_list = []
        for job in job_list:
            data = serializers.TrainingJobSerializer(job).data
            status = serv.status(data['instance_name'])
            data_list.append(
                {
                    'job_name': data['name'],
                    'status': 'Succeeded' if status == 'UNSPECIFIED' or status == 'TERMINATED' else 'Running',
                    'message': data['message']
                }
            )
        return Response(data_list)

    
    def post(
            self,
            request,
    ):
        credentials = Credentials.Default(request)
        # file_obj = request.FILES['file']
        content = request.data['file']
        module_name = request.data['module_name']
        message = request.data['message']
        args = request.data['args']
        print('Current user: {}'.format(request.user))

        user = User.objects.get(username=request.user.username)
        job_name = train.models.random_job_name(prefix='tr_')

        (instance_name, disk_name, storage_name) = self.upload_and_train(
            user.email, request.headers['Authorization'].replace('JWT ', ''),
            job_name, content, module_name, credentials, args)

        data = {
            'user': user.id,
            'name': job_name,
            'status': core.STATUS_PREPARING,
            'message': message,
            'instance_name': instance_name,
            'disk_name': disk_name,
            'storage_name': storage_name
        }
        serializer = serializers.TrainingJobSerializer(data=data)
        if serializer.is_valid():
            job = serializer.save()
            return Response(
                {
                    'instance': instance_name,
                    'disk': disk_name,
                    'name': job_name
                },
                status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
