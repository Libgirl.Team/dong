import logging
import os

from django.conf import settings
from django.shortcuts import get_object_or_404, get_list_or_404
from django.contrib.auth.models import User
from django.http import FileResponse

from rest_framework.generics import ListCreateAPIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status

from core.google.firebase.authentication import FirebaseAuthentication
from core.google import Credentials
from core.google import storage
from googleapiclient.errors import HttpError

import endpoint
import core

import api.serializers as serializers

from api.services import TrainingService
from api.services import EndpointService
from api.services import UserService
from train.models import TrainingJob

logger = logging.getLogger(__name__)

DEFAULT_IP_ADDRESS = "192.168.0.1"


class AuthUser(APIView):
    def post(
            self,
            request,
    ):
        email = request.data['email']
        password = request.data['password']
        service = UserService(settings.FIREBASE_API_KEY)
        token = service.signin_with_email_and_password(email, password)
        if token is None:
            return Response(status=401)

        status = {'status': 'OK', 'token': token}
        return Response(status, status=200)


class Instances(APIView):
    authentication_classes = (FirebaseAuthentication, )
    permission_classes = (IsAuthenticated, )

    def post(self, request, instance_name, method):
        credentials = Credentials.Default(request)
        if method == 'kill':
            serv = EndpointService(credentials=credentials)
            serv.kill(instance_name)
            return Response({
                'name': instance_name,
                'status': 'ok'
            },
                            status=200)


class EndpointDetail(APIView):

    authentication_classes = (FirebaseAuthentication, )
    permission_classes = (IsAuthenticated, )

    def post(self, request, name, method):
        credentials = Credentials.Default(request)
        ed_pt = get_object_or_404(endpoint.models.Endpoint, name=name)
        data = serializers.EndpointSerializer(ed_pt).data
        instance_name = data['instance_name']
        serv = EndpointService(credentials=credentials)
        serv.kill(instance_name)
        return Response({'name': data['name'], 'status': 'ok'}, status=200)

    def get(self, request, name):
        credentials = Credentials.Default(request)
        ed_pt = get_object_or_404(endpoint.models.Endpoint, name=name)
        data = serializers.EndpointSerializer(ed_pt).data
        instance_name = data['instance_name']

        serv = EndpointService(credentials=credentials)
        status = serv.status(instance_name)
        #print(status)

        external_ip = status['external_ip']
        if ed_pt.external_ip == DEFAULT_IP_ADDRESS and external_ip is not None:
            ed_pt.external_ip = external_ip
            ed_pt.save()

        data['external_ip'] = external_ip

        if status['status'] != ed_pt.status:
            ed_pt.status = status['status']
            ed_pt.save()

        data['status'] = core.STATUS_CHOICES[ed_pt.status][1]

        return Response(
            {
                'name': data['name'],
                'external_ip': data['external_ip'],
                'status': data['status']
            },
            status=200)


class Endpoint(APIView):

    authentication_classes = (FirebaseAuthentication, )
    permission_classes = (IsAuthenticated, )

    def get(
        self,
        request,
    ):
        credentials = Credentials.Default(request)
        print('Current user: {}'.format(request.user))

        serv = EndpointService(credentials=credentials)

        # TODO: pagination or filters
        # TODO: provide more information
        endpoint_list = get_list_or_404(endpoint.models.Endpoint, user=request.user)
        response_endpoint_list = []
        
        for ed_pt in endpoint_list:
            data = serializers.EndpointSerializer(ed_pt).data
            try:
                status = serv.status(data['instance_name'])
            except HttpError:
                status = {
                    'external_ip': None,
                    'status' : core.STATUS_NOT_FOUND
                }
                
            if ed_pt.external_ip == DEFAULT_IP_ADDRESS and status['external_ip'] is not None:
                ed_pt.external_ip = status['external_ip']
                ed_pt.save()

            if status['status'] != ed_pt.status:
                ed_pt.status = status['status']
                ed_pt.save()

            data['status'] = core.STATUS_CHOICES[ed_pt.status][1]

            response_endpoint_list.append(
                {
                    'name': data['name'],
                    'external_ip': status['external_ip'],
                    'status': data['status']
                }
            )

        return Response(response_endpoint_list,
                        status=200)


    def post(
            self,
            request,
    ):
        credentials = Credentials.Default(request)
        print(request.data['job'])
        job_name = request.data['job']
        job = get_object_or_404(TrainingJob, name=job_name)
        disk_name = job.disk_name

        serv = EndpointService(job_name=disk_name, credentials=credentials)
        instance_name = serv.exec()

        user = User.objects.get(username=request.user.username)
        data = {
            'user': user.id,
            'name': endpoint.models.random_endpoint_name(prefix="ep_"),
            'instance_name': instance_name,
            'status': core.STATUS_PREPARING,
            'disk_name': disk_name,
            'external_ip': DEFAULT_IP_ADDRESS
        }
        serializer = serializers.EndpointSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {
                    'endpoint_name': data['name'],
                    'status': core.STATUS_CHOICES[data['status']][1]
                },
                status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class Storage(APIView):
    authentication_classes = (FirebaseAuthentication, )
    permission_classes = (IsAuthenticated, )

    def post(self, request, method=None, bucket=None, object=None):
        print(request.data)
        if method == 'o':
            # upload
            content = request.data['file']
            module_name = request.data['module_name']
            message = request.data['message']

            status = {'message': 'upload', 'status': 'OK', 'token': method}
            url = storage.upload_file(dest_file_path, settings.BUCKET_NAME)
            return Response(status, status=200)
        elif method == 'd':
            return Response({
                'message': 'download',
                'status': 'OK',
            },
                            status=200)
        elif method == 'l':
            return Response({
                'message': 'list',
                'status': 'OK',
            }, status=200)
        elif method is None:
            return Response(
                {
                    'message': 'list/buckets',
                    'bucket': bucket,
                    'status': 'OK',
                },
                status=200)
        else:
            print(method)
            return Response({
                'message': 'error',
            }, status=400)

    def get(self, request, method=None, bucket=None, object=None):
        if bucket is None:
            result = []
            for bucket in storage.list_buckets():
                result.insert(0, bucket.name)
            return Response({
                'bucket': result,
                'status': 'OK',
            }, status=200)
        elif object is not None:
            print(object)
            path = "/tmp/file"
            storage.download_string(path, bucket, object)
            response = FileResponse(path, as_attachment=True)
            return response
        elif method is None:
            result = []
            for blob in storage.list_blobs(bucket):
                result.insert(0, blob.name)
            return Response(
                {
                    'bucket': bucket,
                    'lists': result,
                    'status': 'OK',
                },
                status=200)
        else:
            print(method)
            return Response({
                'message': 'error',
            }, status=400)
