import urllib

import core
from core.google import Credentials
from core.google import disk
from core.google import compute
from core.google import zone
from core.google import firebase

class UserService(object):

    def __init__(self, api_key):
        self._api_key = api_key


    def signin_with_email_and_password(self, email, password):
        return firebase.signin_with_email_and_password(self._api_key, email, password)


class EndpointService(object):

    def __init__(self, job_name=None, credentials=None):
        self._credentials = credentials
        self._project_name = credentials.project_id
        self._job_name = job_name


    def _get_startup_script(self):
        content = """#!/bin/bash
mkdir -p /mnt/input
mount -o discard,defaults,ro /dev/sdb /mnt/input

cp  /mnt/input/endpoint/proxy /etc/nginx/sites-available/proxy
rm /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/proxy /etc/nginx/sites-enabled/proxy
service nginx reload
useradd -ms /bin/bash user
sleep 10

sudo -u user -i pip3 install dong
sudo -u user -i pip3 install /mnt/input/endpoint/misosoup-0.2.3.tar.gz
sudo -u user -i pip3 install /mnt/input/endpoint/*.tar.gz
sudo -u user -i sh -c "cd /tmp/;tar xf /mnt/input/endpoint/misosoup-0.2.3.tar.gz"
sudo -u user MODEL_SAVE_DIR=/mnt/input -i sh -c "cd /tmp/misosoup-0.2.3/misosoup;/home/user/.local/bin/gunicorn \\"misosoup:app(project_name='`cat /mnt/input/endpoint/package_name`')\\""
"""
        return content


    def exec(self):
        instance_name = compute.random_instance_name()
        startup_script = self._get_startup_script()
        compute.create_instance(
            self._project_name,
            instance_name,
            startup_script,
            http=True,
            source_disk_image='projects/fullstackaiplatform/global/images/image-nginx-2',
            credentials=self._credentials,
            serviceAccount=None
        )
        targetLink = 'https://www.googleapis.com/compute/v1/projects/'+self._project_name+'/zones/asia-east1-a/disks/' + self._job_name
        print('targetLink: {}'.format(targetLink))
        compute.attach_disk(self._project_name, instance_name, targetLink, credentials=self._credentials)

        return instance_name

    def kill(self,instance_name):
        compute.delete_instance(
            self._project_name,
            instance_name,
            credentials=self._credentials,
        )
        return instance_name

    def _status(self, ip):
        url = "http://{}/api/v1".format(ip)
        req = urllib.request.Request(url)

        status = core.STATUS_PREPARING
        try:
            response = urllib.request.urlopen(req)
            status = core.STATUS_RUNNING
        except urllib.error.HTTPError as e:
            if e.code >= 200 and e.code < 300:
                status = core.STATUS_RUNNING
            elif e.code == 404:
                status = core.STATUS_FAILED
        except urllib.error.URLError as e:
            pass

        return status


    def status(self, instance_name):
        external_ip = compute.get_external_ip(self._project_name, instance_name, credentials=self._credentials)
        status = {
            'external_ip': external_ip,
            'status': self._status(external_ip)
        }
        return status


class TrainingService(object):

    def __init__(self, credentials):
        self._credentials = credentials
        self._project_name = credentials.project_id

    def _get_startup_script(self, storage_url, package_name, module_name, user_email, salt_password, args):
        package_url = storage_url.replace('https://storage.googleapis.com/', 'gs://')
        #
        # gsutil could have access rate limit
        #
        content = """#!/bin/bash
mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/sdb
mkdir -p /mnt/output
mount -o discard,defaults /dev/sdb /mnt/output
chmod a+w /mnt/output
mkdir -p /mnt/output/endpoint

MAX_RETRY=5
INTERVAL=5

for i in $(seq 1 $MAX_RETRY); do
   gsutil cp {} /mnt/output/endpoint && break
   sleep $INTERVAL && /bin/false
done
for i in $(seq 1 $MAX_RETRY); do
   gsutil cp gs://libgirl-base-packages/20190618/misosoup-0.2.3.tar.gz /mnt/output/endpoint && break
   sleep $INTERVAL && /bin/false
done
for i in $(seq 1 $MAX_RETRY); do
   gsutil cp gs://libgirl-base-packages/20190502/proxy /mnt/output/endpoint && break
   sleep $INTERVAL && /bin/false
done

useradd -ms /bin/bash user

sudo -u user -i pip3 install dong
echo "machine api.libgirl.com" > /home/user/.netrc
echo "	login {}" >> /home/user/.netrc
echo "	password {}" >> /home/user/.netrc
chown user:user /home/user/.netrc
sudo -u user -i pip3 install /mnt/output/endpoint/{}
sudo -u user MODEL_SAVE_DIR=/mnt/output -i {} {}
echo {} > /mnt/output/endpoint/package_name
umount /mnt/output
sleep 100
""".format(package_url, user_email, salt_password, package_name, module_name, ' '.join(eval(args)), module_name)
        return content


    def _destroy_command(self, project_name, zone_name, instance_name, job_name):
        cmd = "gcloud -q compute instances delete {} --project={} --zone={}|sudo -u user -i dong train kill -j {}".format(
            instance_name,
            project_name,
            zone_name,
            job_name
        )
        return cmd


    def kill(self,instance_name):
        compute.delete_instance(
            self._project_name,
            instance_name,
            credentials=self._credentials,
        )
        return instance_name


    def status(self, instance_name):
        try:
            status = compute.status(self._project_name, instance_name, credentials=self._credentials)
        except Exception as e:
            status = 'UNSPECIFIED'

        return status


    def exec(self, storage_url, package_name, module_name, user_email, salt_password, job_name, args):
        (disk_name, targetLink, _) = disk.create_disk(self._project_name, credentials=self._credentials)
        print("train.exec: disk_name :{},targetlink: {}".format(disk_name, targetLink))
        instance_name = compute.random_instance_name()
        destroy_command = self._destroy_command(self._project_name, zone, instance_name, job_name)
        startup_script = self._get_startup_script(storage_url, package_name, module_name, user_email, salt_password, args) + destroy_command
        compute.create_instance(
            self._project_name,
            instance_name,
            startup_script,
            source_disk_image='projects/fullstackaiplatform/global/images/image-1',
            credentials=self._credentials,
            serviceAccount={
                'serviceAccounts': [{
                    'email': self._credentials.service_account_email,
                    'scopes': [
                        'https://www.googleapis.com/auth/devstorage.read_only',
                        'https://www.googleapis.com/auth/logging.write',
                        'https://www.googleapis.com/auth/monitoring.write',
                        'https://www.googleapis.com/auth/servicecontrol',
                        'https://www.googleapis.com/auth/service.management.readonly',
                        'https://www.googleapis.com/auth/trace.append',
                        #'https://www.googleapis.com/auth/compute'
                    ]}]}
        )
        print("instance: {}, disk: {}, targetLink: {}".format(instance_name, disk_name, targetLink))
        compute.attach_disk(self._project_name, instance_name, targetLink, credentials=self._credentials)

        return (instance_name, disk_name)
