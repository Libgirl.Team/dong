from rest_framework import serializers
import train
import endpoint

class TrainingJobSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = train.models.TrainingJob


class EndpointSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = endpoint.models.Endpoint
