from django.conf import settings
from django.db import models
from django.utils import timezone

import core
from core.google import random_name

User = settings.AUTH_USER_MODEL

class TrainingJob(models.Model):
    user = models.ForeignKey(User, default=1, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=30)
    instance_name = models.CharField(max_length=30, null=True)
    disk_name = models.CharField(max_length=30, null=True)
    storage_name = models.CharField(max_length=200, null=True)

    # Reserved for tabemasu and misosoup (?)
    # ...

    status = models.IntegerField(choices=core.STATUS_CHOICES, default=core.STATUS_UNSPECIFIED)
    message = models.CharField(max_length=100, default='a message')
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


def random_job_name(prefix=''):
    uniq_name = random_name()
    return '{}{}'.format(prefix,uniq_name)
