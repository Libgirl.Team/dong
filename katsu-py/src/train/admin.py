from django.contrib import admin

from .models import TrainingJob

admin.site.register(TrainingJob)
