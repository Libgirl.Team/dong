# dong

Dong is a universal command line tools to libgirls AI platform

## Installation

```bash
cd dong
pip install .
```

## Running tests

```bash
cd dong
python setup.py test
```


## for debugging
```bash
export DONG_DEBUG=t
```

## train with dong_mnist_example
```bash
git clone git@bitbucket.org:libgirl_hai/dong_mnist_example.git
cd dong_mnist_example
dong train exec -- --config-module default
```

## launch endpoint
```bash
dong endpoint up libgirl-cghp8-disk1
```
