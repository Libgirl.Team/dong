# Dong project framework
## Introduction
A **dong** project is a [Python project](https://packaging.python.org/tutorials/packaging-projects/) with its conventional folder structure designed for machine learning. 

## Overview

## Framework Features

**Simple**: dong CLI provides all the commands needed for a ML project, from training, tunning to deploying. Just fill the contents (model, data, parameters…) into the framwork, then everything works.

**Modular**: dong framework help users with building ML projects under the module system of python, which facilitates composability and reusability.

**Customizable**: dong framework help users with building ML projects in the form of source codes, so users can program all the needed functionalities for any part of the ML projects. Other ML services like AutoML that provide fixed usages can be a contrast.

### Why
#### Why a machine learning project shall follow a conventional project framework
A conventional project framework provides **regularity** across projects following the same framework. With that regularity,

1. Machine learning coders have the skeleton to quickly start or continue their projects.
2. Behaviors of project's components are restricted, which reduces the chance of random bugs or errors.
3. Developmental practices which harm reproducibility can be forbidden.
4. Boundaries of components are in consistent with files or folders, which makes it easier to modify specific component by a human or by program automation. 
5. It's much easier to understand a machine learning project following the same framework, which boosts team collaboration and project reuses.

#### Why dong follows [Python project](https://packaging.python.org/tutorials/packaging-projects/) structure
1. The whole project is under your control. Visit the project code anytime and do customization as long as you follow [Python project](https://packaging.python.org/tutorials/packaging-projects/) structure. There is no magic in the framework.
2. [Some tools](https://write.as/chobeat/python-project-tooling-explained) handling Python projects can immediately apply on your machine learning projects. Some of the tools have important features like dependency management.
3. You can install your machine learning project by ```pip install .``` or other ```pip``` compatible ways.
4. You can easily reuse part of your machine learning project by python ```import``` modules after installation. 
5. You can package your machine learning project and share it on [PyPi](https://pypi.org) for scientist or developer community.

### Folder structure layout
#### Folder root layer
- [setup.py](https://packaging.python.org/tutorials/packaging-projects/#creating-setup-py) as the Python project build script.
- [MANIFEST.in](https://docs.python.org/3/distutils/sourcedist.html#specifying-the-files-to-distribute) to claim what files to be included in the [source distribution](https://packaging.python.org/glossary/#term-source-distribution-or-sdist)
- A folder whose name is the project name.
#### Under the project name folder
- ```__init__.py``` contains the project entry function.
- ```data``` folder contains module files to do data fetching and preprocessing.
- ```models``` folder contains module files to define different types of model collections.
- ```trains``` folder contains module files as training scirpts
- ```tunes``` folder contains module files for hyper parameter tuning.
- ```save_load``` folder contains module files handling model serialization and deserialization. Serialization happends after training or tuning, and deserialization happends before deploying to the endpoints and serving the model.
- ```endpoints``` folder contains module files to define how to respond to an API request given the deployed model.
#### Layout illustration 
```
.
├── MANIFEST.in
├── setup.py
└── some_dong_project
    ├── __init__.py
    ├── data
    │   ├── __init__.py
    │   ├── default.py
    │   ├── [ANOTHER_DATA_PROCESSING_MODULE].py
    │   ├── ...
    │   ├── ...   
    │   └── ...
    ├── endpoints
    │   ├── __init__.py
    │   ├── default.py
    │   ├── [ANOTHER_ENDPOINTS_MODULE].py
    │   ├── ...
    │   ├── ...   
    │   └── ...
    ├── models
    │   ├── __init__.py
    │   ├── default.py
    │   ├── [ANOTHER_MODEL_COLLECTION_MODULE].py
    │   ├── ...
    │   ├── ...   
    │   └── ...
    ├── save_load
    │   ├── __init__.py
    │   ├── default.py
    │   ├── [ANOTHER_SAVE_LOAD_MODULE].py
    │   ├── ...
    │   ├── ...   
    │   └── ...
    ├── trains
    │   ├── __init__.py
    │   ├── default.json
    │   ├── default.py
    │   ├── [ANOTHER_TRAIN_MODULE].json
    │   ├── [ANOTHER_TRAIN_MODULE].py
    │   ├── ...
    │   ├── ...   
    │   └── ...    
    └── tunes
        ├── __init__.py
        ├── default.py
        ├── [ANOTHER_TUNE_MODULE].py
        ├── ...
        ├── ...   
        └── ...
```
### Workflow by entry point source code

Path is ```./[PROJECT_NAME]/__init__.py```

```python
def main():

    # argument parsing
    # ...
    # 

    # if NOT applying hyper parameter tuning
    # Invoke train_module.train for training. Then we have the return value as the result model and accuracy score.
        model_score_pair = train_module.train(model=model_collection_module.new(data_fetch_module.get_data_params()),
                                              train_data=data_fetch_module.get_train_data(),
                                              eval_data=data_fetch_module.get_eval_data(),
                                              config=json.loads( # code for loading training parameter config 
                                              ))
        
    
    # if applying hyper parameter tuning

        # get a list of hyper_parameter config from tune module
        hyper_parameter_config_list = tune_module.get_hyper_parameter_config_list()

        model_score_pair_list = []

        # for each hyper parameter config, invoke train_module.train and then append it into model_score_pair_list
        for hyper_parameter_config in hyper_parameter_config_list:
            model_score_pair_list.append(train_module.train(model=model_collection_module.new(data_fetch_module.get_data_params()),
                                                            train_data=data_fetch_module.get_train_data(),
                                                            eval_data=data_fetch_module.get_eval_data(),
                                                            config=hyper_parameter_config.hyper_parameter_dict))

        # select the best model from mode_score_pair_list
        model_score_pair = tune_module.get_best_model_score_pair(model_score_pair_list)
        

    # save the model
    save_load_module.save(model_score_pair.model,
                          # omitted ...
                         )
```

#### Training
For each training execution, the following has to be specified,
- Which train module to use
- Which model collection module to use
- Which data fetching module to use
- Which save/load module to use

If the **dong cli** user doesn't specify one of them, ```dong train exec``` will use the corresponding default module.


##### So, when the train starts, the composition and relationships is clear as the following figure.

![](https://i.imgur.com/tkJzZ4L.png)

##### Or, here is the code
```python
model_score_pair = train_module.train(model=model_collection_module.new(data_fetch_module.get_data_params()),
                                      train_data=data_fetch_module.get_train_data(),
                                      eval_data=data_fetch_module.get_eval_data(),
                                      config=json.loads( # code for loading training parameter config 
                                     ))
```
#### Hyper parameter tuning
For each tuning execution, the following has to be specified,
- Which tune module to use
- Which train module to use
- Which model collection module to use
- Which data fetching module to use
- Which save/load module to use

If the dong cli user doesn’t specify one of them, dong train exec will use the corresponding default module.

##### After understanding the training part, hyper parameter tuning is just about getting the list of hyper_parameter_config object, and then apply them to the training.
```python
# get a list of hyper_parameter config from tune
hyper_parameter_config_list = tune_module.get_hyper_parameter_config_list()

model_score_pair_list = []

# for each hyper parameter config, invoke train_module.train and then append it into model_score_pair_list
for hyper_parameter_config in hyper_parameter_config_list:
    model_score_pair_list.append(train_module.train(model=model_collection_module.new(data_fetch_module.get_data_params()),
                                                    train_data=data_fetch_module.get_train_data(),
                                                    eval_data=data_fetch_module.get_eval_data(),
                                                    config=hyper_parameter_config.hyper_parameter_dict))

# select the best model from mode_score_pair_list
model_score_pair = tune_module.get_best_model_score_pair(model_score_pair_list)

```
### Package Dependency
Please specify your package depency in ```setup.py```  as
```python
setup(name='PROJECT_NAME',
      # ...
      install_requires=[
          'PACKAGE_1',
          'PACKAGE_2',
      ],
      #...
      )
```
## Data
### Data preparation
### Data fetching
#### Local data
#### Cloud data
#### Production input data
We suggest developers write production input data fetching functionality in ```/[PROJECT_NAME]/endpoints/[YOUR_ENDPOINT_MODULE].py```. 
It's the module file for writing the logic how to respond to a service request given a request body. It doesn't bind to any underlying protocols: what is provided is the body of a request and the developer just has to give also the body of the response.
### Data preprocessing
Developers don't want date processing logic inconsistency among training, testing, and predictions. 
To avoid such issue, we suggest developers unify data preprocessing code for all of the training, testing, model serving. Then, put it inside ```/[PROJECT_NAME]/data/[YOUR_DATA_PROCESSING_MODULE].py```
### Parameters which based on data
In a machine learning project, modules besides training can have parameters relying on data. For example, it's common for a model collection definition whose input layer structure is coupled with data shape.
We suggest developers to wrap those data information and provide it via ```get_data_params()``` inside ```/[PROJECT_NAME]/data/[YOUR_DATA_PROCESSING_MODULE].py```
## Model
### Model collection V.S. model
A model is a concrete instance of information extracted or learned from a dataset. In comparison, a description of models could actually indicate a group of models. For example, multi-layer perceptrons (MLPs) is a group of models (or a hypothesis set), not one model. 
To distinguish a group of models and a single model, we use the word **model collection** for a group and **model** for a single instance. The word, class, may fit this purpose, but we don't use it to avoid confusion with Class of Objective Oriented Programming.
### new(data_params)
This function should return an initial model for training module to use. Feel free to use any library like Keras, PyTorch, or others in your model collection module.
#### Naming
We name the function as **new** which provide an initial model for training. During training, the algorithm optimizes the model to become another one.
#### What if I don't want to create a concrete model here
Of course, many machine learning algorithms don't work this ways. The parameters of the model collection could be totally not decided yet. It's ok to return ```None``` and let your training module do the model selection.
#### Denpendency on data shape or other data information
See Parameters which based on data

#### Post prediction processing/Online learning
In ```/[PROJECT_NAME]/endpoints/[YOUR_ENDPOINT_MODULE].py```, your model is given once there is any request. You can update your model and commit it by ```[SAVE_LOAD_MODULE].save()```.

#### Chain multiple models
Since a dong project is a [Python project](https://packaging.python.org/tutorials/packaging-projects/) under your full control, feel free to get others of your models by ```import [PROJECT_NAME].models.[ANOTHER_MODEL_COLLECTION_MODULE]```. Then, you can chain different model together.
#### Train on pretrained models
## Train
Please read worflow by entry point source code first.

On the training phase, the initial model, training data, and evaluation data should be well prepared. You can concentrate on writing the training logic on this train module. By some popular machine learning library, the training code can be as simple as calling ```model.fit``` with configurations.

### Training code/configuration separation
Please try as possible to put the code part and the configuration into ```[YOUR_TRAIN_MODule].py``` and ```[YOUR_TRAIN_MODULE].json``` correspondingly. It's much easier to trace variations across different training. And it makes hyper parameter tuning less friction.

We **don't** force such separation, developers can freely write configurations in the source code file. Ideally we suggest to do separation, but practically there should be cases impossible to achieve so. 
### Model evaluation
## Hyper parameter tuning
### HyperParameterConfig
#### Location
```HyperParameterConfig``` is named tuple and defined in ```/[PROJECT_NAME]/tunes/__init__.py```.
#### Usage
Please prepare your each hyper parameter configuration in one instance of ```HyperParameterConfig```.
- ```hyper_parameter_config.hyper_parameter_dictionary``` is the field for each hyper parameter configuration as a python dictionary. This dictionary will be passed into ```[train_module].train``` as the config instead of using ```[train_module].json```.
- ```hyper_parameter_config.get_next_hyper_parameter_config_list``` is the field for a function. After this hyper_parameter_config is used to train, the project will invoke this function to get the next list which can depend on the result of current training result. This value can be ```None```.

## Save load, restore, or serialization/desrialization
Giving developers full control and freedom is one of the central concept of **dong platform**. Thus, developers can implement ```save(model, save_dir)``` and ```load(save_dir)```.

```save()``` of the specified save_load module will be called after training or hyper parameter tunning. ```load()``` will be invoked before serving the endpoint to get the model to serve. 

Other than these two ocassions, feel free to import your save_load module anytime according to various situation.

## Model deployment
### respond_to_request()
Please implement respond_to_request() in ```/[PROJECT_NAME]/endpoints/[ENDPOINT_MODULE].py``` to decide how to use your model to serve a request.

### Usage
#### On dong cloud
You can use **dong cloud** to serve your model quickly by
```sh
$ dong endpoint up MODEL_NAME
```
#### Other ways
But of course, you can prepare your own model serving code, independent to any protocol. Just use ```import PROJECT_NAME.endpoints.ENDPOINT_MODULE``` so you can use ```respond_to_request()```
