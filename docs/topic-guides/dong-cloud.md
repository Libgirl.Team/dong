# dong cloud
## Introduction
**Dong cloud** is an official cloud service of **dong platform** for a **dong** machine learning project to train and deploy its model.

Developers can use **dong cli** to access **dong cloud**.
## Data storage
## Computing resources
## Training
### Common commands
- Execute a train : ```$ dong train exec```
- List train jobs : ```$ dong train ps```
- Training status : ```$ dong train status```
- Stop a train    : ```$ dong train stop```
- Resume a train  : ```$ dong train bg```
- Kill a train    : ```$ dong train kill```
- Train monitor   : ```$ dong train top```
### About forcing to write training message
#### Git's convention
We think, to force writing training message for every training execution can help machine learning practictioners improve experiment reproducibility and knowledge management. It's analog to commit message in git that it forces developers to do write commit message. 
#### Pier pressure works for there is a tool
Of course practitioners can still write lazy training messages. However, **the tool is now there, no excuse anymore**. one teammate can blame the other about the poor training messages just like developers can do so given the existence of git version control software.
## Hyper parameter tuning
```$ dong train exec --do-hyper-parameter-tune```
Please see the reference manual for more
## Parallel computing
## Model deployment hosting
### Common commands
- Deploy from a model  : ```$ dong endpoint up```
- Stop an endpoint     : ```$ dong endpoint down```
- Restart an endpoint  : ```$ dong endpoint up```
- Delete an endpoint   : ```$ dong endpoint delete```
- List deployment jobs : ```$ dong endpoint ps```
- Endpoint status      : ```$ dong endpoint status```
- Endpoint monitor     : ```$ dong endpoint top```
## Security
