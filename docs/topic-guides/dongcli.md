# Dong CLI
## Introduction
**dong cli** is the command line interface tool for **dong platform**.
It means,
- **dong** project framework
- **dong cloud** for cloud training/deployment

## Local execution

## Templated file generation
### Generating a dong project
Please see LINK for dong project structure
You can generate a templated project by

- ```dong init```

- ```dong new [PROJECT_NAME]```

### Generating modules in dong project
The followings are all the module types supported by **dong cli**'s template generation

- data

  Generate data module from the template:

  ```
  dong template --data-module-name mydata
  ```

- models

  Generate model module from the template:

  ```
  dong template --model-module-name mymodel
  ```

- trains

  Generate train module from the template:

  ```
  dong template --train-module-name mytrain
  ```

- save_loads

  Generate save_load module from the template:

  ```
  dong template --save-load-module-name mysaveload
  ```

- tunes

  Generate tune module from the template:

  ```
  dong template --tune-module-name mytune
  ```

- endpoints

  Generate endpoint module from the template:

  ```
  dong template --endpoint-module-name myendpoint
  ```

#### //for each modules above, explain directly by templates under dong/dong/templates
