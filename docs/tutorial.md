# dong tutorial
## Overview
In this tutorial, we'll step-by-step make a simple mnist **dong** project using **dong cli**. We'll use tensorflow as the machine learning library.

## Project creation
Use ```dong init``` or ```dong new``` to create a new dong project. For example,
```bash
$ dong new my_dong_mnist
```
Let's go into the directory ```my_dong_mnist```.
```bash
$ cd my_dong_mnist
```
Here is the project structure
```
.
├── MANIFEST.in
├── README.md
├── setup.py
└── my_dong_mnist
    ├── __init__.py
    ├── data
    │   ├── __init__.py
    │   └── default.py
    ├── endpoints
    │   ├── __init__.py
    │   └── default.py
    ├── models
    │   ├── __init__.py
    │   └── default.py
    ├── save_load
    │   ├── __init__.py
    │   └── default.py
    └── trains
        ├── __init__.py
        ├── default.json
        └── default.py
```
## Data preparation module
### Module file location
Edit ```my_dong_mnist/data/default.py```

We have a templated file here.
```python
def get_train_data():
    # write your code here to generate program training data
    return 1

def get_eval_data():
    # write your code here to generate program evaluation data
    return 1

def get_data_params():
    # write your code here to pass parameters based on data properties.
    # dong will call this function and pass the result to your model definition
    return 1
```
### get_train_data(), get_eval_data()
**dong cli** will invoke 

- ```get_train_data()``` to get the training data and 
- ```get_eval_data()``` to get the training evaluation data 
when **dong cli** executes a training. 

For a basic tensorflow mnist project, here is the code for ```get_train_data()``` and ```get_eval_data()```

```python                                
mnist = tensorflow.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train, x_test = x_train / 255.0, x_test / 255.0
```                        

### get_data_params()
Sometimes we want to know data properties to define our learning model. For example, what the input data dimension is.```get_data_params()``` serves this purpose.

For a basic tensorflow mnist project, it means

```python
num_labels = len(numpy.unique(y_train))
image_size = x_train.shape[1]
```
### collections.namedtuple
To make the data more semantical accessible, in this tutorial we use [collections.nametuple](https://docs.python.org/3/library/collections.html#collections.namedtuple).
```python
DataPair = collections.namedtuple('DataPair', ['x', 'y'])
DataParams = collections.namedtuple('Params', ['image_size', 'num_labels'])
```
So we can use 
```python
train_data.x
train_data.y
data_params.image_size
data_params.num_labels
``` 
to access the data

### Done module file editing
Here is the data preparation content for ```my_dong_mnist/data/default.py```

```python
from __future__ import division

import collections
import numpy
import tensorflow

DataPair = collections.namedtuple('DataPair', ['x', 'y'])
DataParams = collections.namedtuple('Params', ['image_size', 'num_labels'])
                                
mnist = tensorflow.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train, x_test = x_train / 255.0, x_test / 255.0

def get_train_data():
    return DataPair(x_train, y_train)

def get_eval_data():
    return DataPair(x_test, y_test)

def get_data_params():
    
    image_size = x_train.shape[1]
    num_labels = len(numpy.unique(y_train))
    return DataParams(image_size, num_labels)
```

## Model collection definition module
### Module file location 
Edit ```my_dong_mnist/models/default.py```
```python
def new(data_params=None):
    # write your model initial definition here
    return None
```
### data_params
We'll get the return value of ```get_data_params()``` defined in ```my_dong_mnist/data/default.py``` as the parameter ```data_params```. 

By ```data_params.image_size``` we can access the image size, which is 28, of the mnist data.
By ```data_params.num_labels``` we can access the number of label, which is 10, of the mnist data.

### Done module file editing
Here is the basic **tensorflow** multilevel perceptron model for ```my_dong_mnist/models/default.py```
```python
import tensorflow


hidden_units=512
dropout=0.2

def new(data_params=None):
    return tensorflow.keras.models.Sequential([
        tensorflow.keras.layers.Flatten(input_shape=(data_params.image_size, data_params.image_size)),
        tensorflow.keras.layers.Dense(hidden_units, activation=tensorflow.nn.relu),
        tensorflow.keras.layers.Dropout(dropout),
        tensorflow.keras.layers.Dense(data_params.num_labels, activation=tensorflow.nn.softmax)
    ])

```
## Training definition module
### Module file location
Edit ```my_dong_mnist/trains/default.py```
```python
def train(model, train_data, eval_data=None, config={}):

    # write your training script as this function.

    evalution_score = 0.0
    return model, evaluation_score
```
### Done editing the training module file

```python
def train(model, train_data, eval_data=None, config={}):

    model.compile(**config['compile'])    
    model.fit(train_data.x, train_data.y, epochs=config['fit']['epochs'])
    
    return model, model.evaluate(eval_data.x, eval_data.y)[1]
```
### Parameters
#### model
The parameter ```model``` we get it from 
```python
# file my_dong_mnist/models/default.py
def new(data_params=None): 
```
#### train_data
The parameter ```train_data``` is the return value of 
```python
# my_dong_mnist/data/default.py

def get_train_data():
    return DataPair(x_train, y_train)
```

According to our definition of ```get_train_data()```,

- by ```train_data.x``` we can get the training data x;
- by ```train_data.y``` we can get the training data y.

#### eval_data
The parameter ```train_data``` is the return value of 
```python
# my_dong_mnist/data/default.py

def get_eval_data():
    return DataPair(x_test, y_test)
```

According to our definition of ```eval_data()```,

- by ```eval_data.x``` we can get the training data x;
- by ```eval_data.y``` we can get the training data y.
###### No forcing using ```get_eval_data()```
We don't force the developers to use ```get_eval_data()``` to pass the evaluation data. 
You can use your own sampling method in the training/tuning script to have evaluation data.

#### config
**dong cli** will parse```/my_dong_mnist/trains/default.json``` and make the training configuration available as the ```config``` parameter. 
So we can access the training parameter as the following
```python
config['compile']
config['fit']['epochs']
```

```/my_dong_mnist/trains/default.py```
## Training configuration
### File location
Edit ```my_dong_mnist/trains/default.json```
The initial content is an empty object
```json
{}
```
### Editing as the following
```json
{
  "compile": {
    "optimizer": "adam",
    "loss": "sparse_categorical_crossentropy",
    "metrics": [ "accuracy" ]
  },
  "fit": {
    "epochs": 5
  }
}
```
### Usage
**dong cli** will parse ```/my_dong_mnist/trains/default.json``` and make the it available as the ```config``` parameter. So, 
- ```config['compile']```  We can access the tensorflow compile argument.
- ```config['fit']``` We can access the tensorflow fit argument.
###### No forcing using ```config```
We don't force the developers to use ```[TRAIN_SCRIPT_NAME].json``` to define training configuration.

You can directly define training configuration in ```[TRAIN_SCRIPT_NAME].py```. 
(```/my_dong_mnist/trains/default.py``` in this tutorial) 

## Serialize/deserialize module
### Module file location
Edit ```/my_dong_mnist/save_load/default.py```
```python
def save(model, save_dir):
    # write your model serialization code here given the model instance and output directory save_dir
    # dong will call this function after training to save your model
    pass

def load(save_dir):
    # given the model saved directory save_dir
    # please write your code here to deserialize your model
    
    model = None
    return model
```

### save(model, save_dir)
To save the training result for model deployment, the trained **model** is passed after the training execution.

**save_dir** is a directory path for you to save the trained model output.

We'll use [model.save](https://www.tensorflow.org/api_docs/python/tf/keras/models/Sequential#save) to output the h5 file.
```python
def save(model, save_dir):
    model.save(save_dir + 'my_model.h5')
    pass
```
### load(save_dir)
To restore a model for model deployment, we have a **save_dir** which is the previous output location.

We'll use [tensorflow.keras.models.load_model](https://www.tensorflow.org/api_docs/python/tf/keras/models/load_model) to restore the saved model after training.

```python
def load(save_dir):
    return tensorflow.keras.models.load_model( save_dir + 'my_model.h5')
```
### Done module file editing
This is our ```/my_dong_mnist/save_load/default.py```
```python
import tensorflow

def save(model, save_dir):
    model.save(save_dir + 'my_model.h5')
    pass

def load(save_dir):
    return tensorflow.keras.models.load_model( save_dir + 'my_model.h5')
```    
    
## Endpoint module file
### Module file location
Edit ```my_dong_mnist/endpoints/default.py```  

```python
import json

def respond_to_request(model, request_body_json): 
    # with request body as a json
    # please write your program to return a response to a request given your model instance
    
    response = None
    return response
```
### respond_to_request(model, request_body_json)
Whenever there is an API request, ```respond_te_request``` will be called with the deployed model as **model** and request body as  **request_body_json**.

Let's write code to return a json string as a reply to the request.

#### Load the json
```python
    data = json.loads(request_body_json)
```
#### Scale the input
We expect the request as an array of 28*28 of 0 to 255 input. let's scale it between 0.0 and 1.0
```python
    x = numpy.array(data) / 255.0
```
#### Predict and return
We can directly use [model.predict](https://www.tensorflow.org/api_docs/python/tf/keras/models/Model) to predict the mnist data output. We also dump it into a json string.
```python
    return json.dumps(model.predict(x).tolist())
```
### Done endpoint module file
```python

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import json, numpy

def respond_to_request(model, request_body_json): # should support more formats
    data = json.loads(request_body_json)
    x = numpy.array(data) / 255.0
    return json.dumps(model.predict(x).tolist())
```

## Add package dependencies
### File location
Edit ```setup.py```
```python
from setuptools import setup, find_packages

setup(name='my_dong_mnist',
      version='0.1',
      description='none',
      license='MIT',
      packages=find_packages(),
      include_package_data=True,  
      zip_safe=False,
      entry_points = {
        'console_scripts': ['my_dong_mnist=my_dong_mnist:main'],
      },
    )
```
### Add dependencies
```python
      install_requires=[
          'tensorflow',
      ],
```
### Done dependencies declaration
```python
from setuptools import setup, find_packages

setup(name='tabemasu',
      version='0.2',
      description='none',
      license='apache2',
      install_requires=[
          'tensorflow',
      ],
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      entry_points = {
        'console_scripts': ['my_dong_mnist=my_dong_mnist:main'],
      },
    )

```
## Execute my_dong_mnist
### Train
```bash
$ ls # make sure we're under the write directory
MANIFEST.in	my_dong_mnist	setup.py
```
```bash
$ dong train exec

Building package...
my_dong_mnist/my_dong_mnist

/var/folders/sy/42xk2x7s1pdf7_hmv1swkb140000gn/T/tmpnsmpiz39
Uploading package...
_upload_package: {'instance': 'libgirl-SOME_INSTANCE-instance', 'disk': 'libgirl-SAVE_MODEL-disk1'}
_execute: libgirl-SOME_INSTANCE-instance libgirl-[SAVE_MODEL]-disk1
Execute a training job: ...................................................................................................................👍
Saved model: libgirl-[SAVE_MODEL]-disk1
```
### Deploy
### Deploy the model to an API endpoint
```bash
$ dong endpoint up YOUR_SAVE_MODEL
bring up: libgirl-[SAVE_MODEL]-disk1
Deployment IP: SOME_IP
```
### Test the endpoint
```bash
curl -X POST \
  http://SOME_IP/api/v1 \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '[[[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 224, 0, 0, 0, 0, 0, 0, 0, 70, 29, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 121, 231, 0, 0, 0, 0, 0, 0, 0, 148, 168, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 195, 231, 0, 0, 0, 0, 0, 0, 0, 96, 210, 11, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 252, 134, 0, 0, 0, 0, 0, 0, 0, 114, 252, 21, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 45, 236, 217, 12, 0, 0, 0, 0, 0, 0, 0, 192, 252, 21, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 168, 247, 53, 0, 0, 0, 0, 0, 0, 0, 18, 255, 253, 21, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 84, 242, 211, 0, 0, 0, 0, 0, 0, 0, 0, 141, 253, 189, 5, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 169, 252, 106, 0, 0, 0, 0, 0, 0, 0, 32, 232, 250, 66, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 15, 225, 252, 0, 0, 0, 0, 0, 0, 0, 0, 134, 252, 211, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 22, 252, 164, 0, 0, 0, 0, 0, 0, 0, 0, 169, 252, 167, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 9, 204, 209, 18, 0, 0, 0, 0, 0, 0, 22, 253, 253, 107, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 169, 252, 199, 85, 85, 85, 85, 129, 164, 195, 252, 252, 106, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 41, 170, 245, 252, 252, 252, 252, 232, 231, 251, 252, 252, 9, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 84, 84, 84, 84, 0, 0, 161, 252, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127, 252, 252, 45, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 128, 253, 253, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127, 252, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 135, 252, 244, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 232, 236, 111, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 179, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]], [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 77, 254, 107, 3, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 227, 254, 254, 9, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 81, 254, 254, 165, 1, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 203, 254, 254, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 53, 254, 254, 250, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 134, 254, 254, 180, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 196, 254, 248, 48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 254, 254, 237, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 111, 254, 254, 132, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 163, 254, 238, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 252, 254, 223, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 79, 254, 254, 154, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 163, 254, 238, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 28, 252, 254, 210, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 86, 254, 254, 131, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 254, 234, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 175, 254, 204, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 211, 254, 196, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 158, 254, 160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 26, 157, 107, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]'
```
The reply should look something like this
```
[[2.879309568548649e-10, 2.6625946239478004e-11, 2.580107016925126e-09, 5.453760706930488e-12, 0.9999690055847168, 3.3259575649147166e-10, 3.2778924019538636e-10, 4.35676184906697e-07, 2.190379821964683e-10, 3.0488341508316807e-05], [1.3478038130010361e-10, 0.9997259974479675, 6.728584622806011e-08, 5.9139901864568856e-09, 9.023875122693426e-07, 5.708465922182882e-10, 1.2523435088951373e-07, 0.0002721738419495523, 6.667413003924594e-07, 9.076808638042166e-09]]
```

