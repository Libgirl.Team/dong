use katsu;

fn main() {
    println!("Start server...");
    let server = katsu::create_server().unwrap();
    server.launch();
}
