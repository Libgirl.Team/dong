use rocket::http::Status;
use rocket::post;
use rocket::State;
use rocket_contrib::json;
use rocket_contrib::json::{Json, JsonValue};

use jsonwebtoken::{decode, encode, Algorithm, Header};
use serde_derive::{Deserialize, Serialize};

use crate::services::UserService;

#[derive(Serialize, Deserialize)]
pub struct UserData {
    pub email: String,
    pub password: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct UserToken {
    iat: i64,
    exp: i64,
    username: String,
}

fn jwt_generate(username: String) -> String {
    let now = time::get_time().sec;
    let payload = UserToken {
        iat: now,
        exp: now,
        username: username,
    };

    encode(&Header::default(), &payload, "secret".as_ref()).unwrap()
}

#[post("/login", format = "application/json", data = "<user>")]
pub fn login(
    user: Json<UserData>,
    user_service: State<Box<UserService>>,
) -> Result<JsonValue, Status> {
    match user_service.validate(&user.email, &user.password) {
        Ok(status) => {
            let token = jwt_generate(user.email.clone());
            Ok(json!({ "status": status, "token": token }))
        }
        Err(_) => Err(Status::Unauthorized),
    }
}

#[post("/logout", format = "application/json")]
pub fn logout() -> JsonValue {
    json!({ "status": "ok" })
}
