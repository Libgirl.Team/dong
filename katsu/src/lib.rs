#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

mod api;
mod services;

use crate::services::UserService;

pub fn create_server() -> Result<rocket::Rocket, String> {
    let server = rocket::ignite()
        .manage(Box::new(UserService{}))
        .mount("/", routes![api::hello::index, api::auth::login, api::auth::logout]);
    Ok(server)
}
