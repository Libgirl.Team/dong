extern crate cucumber_rust;

use crate::World;
use rocket::http::{ContentType, Status};
use rocket_contrib::json;

steps!(World => {
    given regex r"an user with the email (.*) and the password (.+)" |world, matches, _| {
        let email = &matches[1];
        let password = &matches[2];
        world.email = email.to_string();
        world.password = password.to_string();
    };

    when "he logins" |world, _| {
        let data = json!({
            "email": world.email,
            "password": world.password,
        });
        let res = world.client.post("/login")
            .header(ContentType::JSON)
            .body(data.to_string())
            .dispatch();
        world.res = res.status();
    };

    then "login should be successful" |world, _|{
        assert_eq!(world.res, Status::Ok);
    };

    then "login should be failed" |world, _|{
        assert_ne!(world.res, Status::Ok);
    };
});
