Feature: Login feature

  As a user
  I want to login with my details
  So that I can start using other CLI commands

  Scenario: User enters correct password
    Given an user account with the email johndoe@libgirl.com and the password 42703538
    When some user logins with the email johndoe@libgirl.com and the password 42703538
    Then login should be successful

  Scenario: User enters wrong password
    Given an user account with the email johndoe@libgirl.com and the password 42703538
    When some user logins with the email johndoe@libgirl.com and the password 12341234
    Then login should be failed
