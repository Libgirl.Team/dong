#[macro_use]
extern crate cucumber_rust;

use katsu;
use rocket::http::Status;
use rocket::local::Client;

pub mod steps;

pub struct World {
    pub email: String,
    pub password: String,
    pub client: rocket::local::Client,
    pub res: rocket::http::Status,
}

impl cucumber_rust::World for World {}
impl std::default::Default for World {
    fn default() -> World {
        let rocket = katsu::create_server().unwrap();
        World {
            email: "johndoe@libgirl.com".to_string(),
            password: "42703538".to_string(),
            client: Client::new(rocket).unwrap(),
            res: Status::NotFound,
        }
    }
}

// Declares a before handler function named `a_before_fn`
before!(a_before_fn => |_scenario| {
});

// Declares an after handler function named `an_after_fn`
after!(an_after_fn => |_scenario| {

});

// A setup function to be called before everything else
fn setup() {}

cucumber! {
    features: "./tests/features", // Path to our feature files
    world: crate::World, // The world needs to be the same for steps and the main cucumber call
    steps: &[
        steps::login::steps // the `steps!` macro creates a `steps` function in a module
    ],
    setup: setup, // Optional; called once before everything
    before: &[
        a_before_fn // Optional; called before each scenario
    ],
    after: &[
        an_after_fn // Optional; called after each scenario
    ]
}
